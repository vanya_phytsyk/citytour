package one.citytour.presenter.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import one.citytour.CityTourApplication;

/**
 * Created by vanya on 7/20/2016.
 */
public class ConnectivityHelper {

    private static boolean nextTimeNetworkUnAvailable = false;

    public static void setNextTimeNetworkUnAvailable() {
        ConnectivityHelper.nextTimeNetworkUnAvailable = true;
    }

    public static boolean isNetworkAvailable() {
        if (nextTimeNetworkUnAvailable) {
            nextTimeNetworkUnAvailable = false;
            return false;
        }
        return true;
    }
}

