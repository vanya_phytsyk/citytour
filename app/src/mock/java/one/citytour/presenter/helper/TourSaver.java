package one.citytour.presenter.helper;

import android.graphics.Bitmap;

import one.citytour.CityTourApplication;
import one.citytour.domain.model.Photo;
import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;
import one.citytour.domain.repository.impl.Injector;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import one.citytour.presenter.helper.StorageHelper;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by vanya on 6/7/2016.
 */
public class TourSaver {
    public Observable<Photo> saveTour(Tour tourForSave) {
        return Observable.just(tourForSave.getCoverPhoto());
    }

    private Observable<TourPoint> saveAudio(TourPoint tourPoint) {
        return Injector.provideRemoteFileRepository().getFile(tourPoint.getAudio().getUrl())
                .flatMap(responseBody -> Observable.create(new Observable.OnSubscribe<TourPoint>() {
                    @Override
                    public void call(Subscriber<? super TourPoint> subscriber) {
                        try {
                            byte[] bytes = responseBody.bytes();
                            StorageHelper.getInstance().cacheFile(tourPoint.getAudio().getUrl(), bytes, uri -> {
                                        Timber.d("cached file id: %d", tourPoint.getAudio().getId());
                                        subscriber.onNext(tourPoint);
                                        subscriber.onCompleted();
                                    }
                                    , throwable -> {
                                        Timber.d("cached file id: %d failure", tourPoint.getAudio().getId());
                                        subscriber.onError(new Exception("Something happen"));
                                    }
                            );
                        } catch (IOException e) {
                            subscriber.onError(e);
                        }
                    }
                })).subscribeOn(Schedulers.newThread());
    }

    public Observable<Tour> deleteTour(Tour tour) {
        return Observable.just(tour);
    }
}
