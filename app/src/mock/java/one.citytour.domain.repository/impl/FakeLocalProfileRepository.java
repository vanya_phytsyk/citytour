package one.citytour.domain.repository.impl;

import one.citytour.domain.model.Photo;
import one.citytour.domain.model.Profile;
import one.citytour.domain.repository.LocalProfileRepository;
import rx.Observable;

/**
 * Created by vanya on 7/22/2016.
 */
public class FakeLocalProfileRepository implements LocalProfileRepository {
    @Override
    public Observable<Profile> getProfile() {
        return Observable.just(new Profile(123, "Ivan", "Cool", new Photo(2342, "some/photo/url"), false));
    }

    @Override
    public Observable<Profile> storeProfile(Profile profile) {
        return null;
    }

    @Override
    public Observable<Object> clearCurrentProfile() {
        return null;
    }
}
