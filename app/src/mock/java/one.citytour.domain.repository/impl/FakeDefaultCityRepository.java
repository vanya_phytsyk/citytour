package one.citytour.domain.repository.impl;

import com.google.android.gms.maps.model.LatLng;

import one.citytour.domain.repository.DefaultCityRepository;

/**
 * Created by vanya on 7/21/2016.
 */
public class FakeDefaultCityRepository implements DefaultCityRepository {

    private final static LatLng LVIV_LATLNG = new LatLng(49.833258, 24.038394);
    private final static String LVIV_NAME = "Lviv";

    @Override
    public String getDefaultCityName() {
        return LVIV_NAME;
    }

    @Override
    public LatLng getDefaultCityLatLng() {
        return LVIV_LATLNG;
    }
}
