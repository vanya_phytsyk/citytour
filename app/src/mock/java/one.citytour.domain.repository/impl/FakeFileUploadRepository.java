package one.citytour.domain.repository.impl;

import one.citytour.domain.model.Audio;
import one.citytour.domain.model.Photo;
import one.citytour.domain.repository.FileUploadRepository;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by vanya on 7/19/2016.
 */
public class FakeFileUploadRepository implements FileUploadRepository {

    public static final String CORRECT_URL = "http://some.url";
    public static final String FAILURE_FILE_PATH = "fail/file/path";

    @Override
    public Observable<Photo> uploadPhoto(String filePath) {
        if (filePath.equals(FAILURE_FILE_PATH)) {
            return Observable.create(new Observable.OnSubscribe<Photo>() {
                @Override
                public void call(Subscriber<? super Photo> subscriber) {
                    subscriber.onError(new Throwable("Wrong file path!"));
                }
            });
        }
        return Observable.just(new Photo(0, CORRECT_URL));
    }

    @Override
    public Observable<Audio> uploadAudio(String audioPath) {
        return null;
    }
}
