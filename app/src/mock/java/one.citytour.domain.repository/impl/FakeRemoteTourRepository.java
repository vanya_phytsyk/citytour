package one.citytour.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;

import one.citytour.domain.model.Photo;
import one.citytour.domain.model.Tour;
import one.citytour.domain.repository.RemoteTourRepository;
import rx.Observable;

/**
 * Created by vanya on 7/20/2016.
 */
public class FakeRemoteTourRepository implements RemoteTourRepository {
    @Override
    public Observable<List<Tour>> getTours(double latitude, double longitude) {
        return Observable.just(getFakeTourList());
    }

    @Override
    public Observable<Tour> getTour(long id) {
        return null;
    }

    @Override
    public Observable<Tour> createTour(Tour tour) {
        return Observable.just(new Tour());
    }


    private List<Tour> getFakeTourList() {
        List<Tour> tours = new ArrayList<>();

        Tour architectureTour = new Tour(56456);
        architectureTour.setTitle("Lviv Architecture tour");
        architectureTour.setDescription("Most famous Lviv buildings");
        architectureTour.setCoverPhoto(new Photo(235235, "https://www.dropbox.com/s/113u9g45w20pu5m/Lviv.Theatre.of.Opera.and.Ballet.jpg?dl=1"));
        tours.add(architectureTour);

        Tour alcoTour = new Tour(234234);
        alcoTour.setTitle("Lviv alcohol tour");
        alcoTour.setDescription("Interesting Lviv pubs, cafes, restaurants...");
        alcoTour.setCoverPhoto(new Photo(43235462, "https://www.dropbox.com/s/z54syp1ac7ckjgh/62235836.jpg?dl=1"));
        tours.add(alcoTour);

        Tour candyTour = new Tour(243389234);
        candyTour.setTitle("Lviv sweets");
        candyTour.setDescription("Chocolate, candies, lollipops...");
        candyTour.setCoverPhoto(new Photo(35623122, "https://www.dropbox.com/s/mqx80zsam2ny09v/lvivska-maysternya-shokoladu.jpg?dl=1"));
        tours.add(candyTour);

        Tour historyTour = new Tour(242);
        historyTour.setTitle("History is near us");
        historyTour.setDescription("Places where history was made");
        historyTour.setCoverPhoto(new Photo(92345432, "https://www.dropbox.com/s/g3wol8z2x3mu86h/31_foto3.jpg?dl=1"));
        tours.add(historyTour);

        Tour nightClubTour = new Tour(243423234);
        nightClubTour.setTitle("Best Lviv nightClubs");
        nightClubTour.setDescription("Best music, hottest girls, drinks and zakuska");
        nightClubTour.setCoverPhoto(new Photo(121235235, "https://www.dropbox.com/s/izv02phrd4cn1g6/efbdba87sesaa.jpg?dl=1"));
        tours.add(nightClubTour);

        return tours;
    }
}
