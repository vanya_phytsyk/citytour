package one.citytour.domain.repository.impl;

import one.citytour.domain.repository.AuthLocalRepository;
import one.citytour.domain.repository.Authorizer;
import one.citytour.domain.repository.DefaultCityRepository;
import one.citytour.domain.repository.FileUploadRepository;
import one.citytour.domain.repository.LocalPhotoRepository;
import one.citytour.domain.repository.LocalProfileRepository;
import one.citytour.domain.repository.LocalRouteRepository;
import one.citytour.domain.repository.LocalTourRepository;
import one.citytour.domain.repository.LocationRepository;
import one.citytour.domain.repository.RemoteFileRepository;
import one.citytour.domain.repository.RemoteProfileRepository;
import one.citytour.domain.repository.RemoteRouteRepository;
import one.citytour.domain.repository.RemoteTourRepository;

/**
 * Created by vanya on 7/19/2016.
 */
public class Injector {
    public static AuthLocalRepository provideAuthLocalRepository() {
        return new FakeAuthLocalRepository();
    }

    public static Authorizer provideAuthorizer(Authorizer.AuthType authType) {
        return null;
    }

    public static FileUploadRepository provideFileUploadRepository() {
        return new FakeFileUploadRepository();
    }

    public static LocalPhotoRepository provideLocalPhotoRepository() {
        return null;
    }

    public static LocalProfileRepository provideLocalProfileRepository() {
        return new FakeLocalProfileRepository();
    }

    public static LocalRouteRepository provideLocalRouteRepository() {
        return null;
    }

    public static LocalTourRepository provideLocalTourRepository() {
        return new FakeLocalTourRepository();
    }

    public static LocationRepository provideLocationRepository() {
        return null;
    }

    public static RemoteFileRepository provideRemoteFileRepository() {
        return null;
    }

    public static RemoteProfileRepository provideRemoteProfileRepository() {
        return null;
    }

    public static RemoteRouteRepository provideRemoteRouteRepository() {
        return null;
    }

    public static RemoteTourRepository provideRemoteTourRepository() {
        return new FakeRemoteTourRepository();
    }

    public static DefaultCityRepository provideDefaultCityRepository() {
        return new FakeDefaultCityRepository();
    }
}
