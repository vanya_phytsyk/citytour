package one.citytour.domain.repository.impl;

import one.citytour.domain.model.AuthResponse;
import one.citytour.domain.repository.AuthLocalRepository;

/**
 * Created by vanya on 7/20/2016.
 */
public class FakeAuthLocalRepository implements AuthLocalRepository {

    private boolean isAuthorized;

    @Override
    public String getAccessToken() {
        return null;
    }

    @Override
    public String getRefreshToken() {
        return null;
    }

    @Override
    public void clearSession() {
        isAuthorized = false;
    }

    @Override
    public void storeAuthCredentials(String accessToken, String refreshToken, long expiredTime) {

    }

    @Override
    public void storeAuthCredentials(AuthResponse authResponse) {

    }

    @Override
    public boolean hasAccessToken() {
        return isAuthorized;
    }

    @Override
    public boolean hasRefreshToken() {
        return false;
    }
}
