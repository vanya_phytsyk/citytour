package one.citytour.sharing;

import one.citytour.domain.model.Tour;

/**
 * Created by vanya on 8/14/2016.
 */

public interface Sharer {
    void shareToWall(Tour tour);
}
