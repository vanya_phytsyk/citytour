package one.citytour.sharing;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKPhotoArray;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;
import com.vk.sdk.dialogs.VKShareDialog;
import com.vk.sdk.dialogs.VKShareDialogBuilder;

import one.citytour.R;
import one.citytour.domain.model.Tour;
import rx.subjects.PublishSubject;

/**
 * Created by vanya on 8/14/2016.
 */

public class VKSharer implements Sharer {

    private final Activity activity;
    private PublishSubject<Object> shareResultSubject;
    private Bitmap logoBitmap;

    public VKSharer(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void shareToWall(Tour tour) {

        loadImageAndShowDialog(tour);
    }

    private VKShareDialog.VKShareDialogListener listener = new VKShareDialog.VKShareDialogListener() {
        @Override
        public void onVkShareComplete(int postId) {
            logoBitmap.recycle();
            shareResultSubject.onNext(null);
        }

        @Override
        public void onVkShareCancel() {
            logoBitmap.recycle();
            shareResultSubject.onNext(null);
        }

        @Override
        public void onVkShareError(VKError error) {
            logoBitmap.recycle();
            shareResultSubject.onError(error.httpError);

        }
    };

    private void loadImageAndShowDialog(Tour tour) {
        Picasso.with(activity).load(tour.getCoverPhoto().getScreenDependentUrl(activity)).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                VKShareDialogBuilder builder = new VKShareDialogBuilder();
                builder.setText(tour.getTitle() +
                        ".\n" + tour.getDescription() + "\n#citytour");
                logoBitmap = BitmapFactory.decodeResource(activity.getResources(),
                        R.mipmap.ic_launcher);

                builder.setAttachmentLink(activity.getString(R.string.app_link_text),
                        "https://play.google.com/store/apps/details?id=one.citytour");

                builder.setShareDialogListener(listener);
                builder.setAttachmentImages(new VKUploadImage[]{
                        new VKUploadImage(logoBitmap, VKImageParameters.pngImage()),
                        new VKUploadImage(bitmap, VKImageParameters.jpgImage(0.9f))});
                builder.show(activity.getFragmentManager(), "VK_SHARE_DIALOG");
                shareResultSubject.onNext(null);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                shareResultSubject.onError(new Throwable("Cannot load image " + tour.getCoverPhoto().getUrl()));
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    public void setShareResultSubject(PublishSubject shareResultSubject) {
        this.shareResultSubject = shareResultSubject;
    }
}
