package one.citytour;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;
import com.vk.sdk.VKSdk;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * Created by vanya on 5/6/2016.
 */
public class CityTourApplication extends Application {
    private static Context applicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        FacebookSdk.sdkInitialize(getApplicationContext());
        VKSdk.initialize(this);
        AppEventsLogger.activateApp(this);
        applicationContext = this;

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            Picasso.with(this)
                    .setLoggingEnabled(true);
        } else {
            Timber.plant(new CrashReportingTree());
        }
    }

    public static Context getContext() {
        return applicationContext;
    }

    /**
     * A tree which logs important information for crash reporting.
     */
    private static class CrashReportingTree extends Timber.Tree {
        @Override
        protected void log(int priority, String tag, String message, Throwable t) {
            if (priority == Log.ERROR) {
                Crashlytics.log(priority, tag, message);
                if (t != null) {
                    Crashlytics.logException(t);
                }
            }
        }
    }

    private Tracker mTracker;

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }
}
