package one.citytour.domain.filestorage;

import android.net.Uri;

/**
 * Created by vanya on 5/24/2016.
 */
public interface Storage {
    void storeFile(final String url, final byte[] bytes, final SuccessCallback successCallback, final FailureCallback failureCallback);
    boolean isStored(String url);
    public Uri getStoredFileUri(String url);
    void removeFromStorage(String url);
    void remoteFromStorageByUri(String uri);
}
