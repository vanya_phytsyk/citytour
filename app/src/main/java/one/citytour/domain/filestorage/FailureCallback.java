package one.citytour.domain.filestorage;

/**
 * Created by vanya on 5/24/2016.
 */
public interface FailureCallback {
    public void onFailure(Throwable throwable);
}
