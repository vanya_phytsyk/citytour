package one.citytour.domain.filestorage;

import android.net.Uri;

/**
 * Created by vanya on 5/24/2016.
 */
public interface SuccessCallback {
    public void onSuccess(Uri fileUri);
}
