package one.citytour.domain.filestorage;

import android.net.Uri;

/**
 * Created by vanya on 5/24/2016.
 */
public interface Cache {
    void cacheFile(final String url, final byte[] bytes, final SuccessCallback successCallback, final FailureCallback failureCallback);
    boolean isCached(String url);
    public Uri getCachedFileUri(String url);
}
