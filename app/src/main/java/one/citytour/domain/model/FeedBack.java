package one.citytour.domain.model;

/**
 * Created by vanya on 8/1/2016.
 */
public class FeedBack {
    private int rating;
    private String comment;

    public FeedBack(int rating, String comment) {
        this.rating = rating;
        this.comment = comment;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
