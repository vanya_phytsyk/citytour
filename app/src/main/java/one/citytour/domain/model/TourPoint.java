package one.citytour.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanya on 17.04.16.
 */
public class TourPoint implements Parcelable {
    private long id;

    private double longitude;
    private double latitude;
    private Photo previewPhoto;
    private List<Photo> photos = new ArrayList<>();
    private String title;
    private String subTitle;
    private String description;
    private Audio audio;
    private float rating;

    protected TourPoint(Parcel in) {
        id = in.readLong();
        longitude = in.readDouble();
        latitude = in.readDouble();
        previewPhoto = in.readParcelable(Photo.class.getClassLoader());
        photos = in.createTypedArrayList(Photo.CREATOR);
        title = in.readString();
        subTitle = in.readString();
        description = in.readString();
        audio = in.readParcelable(Audio.class.getClassLoader());
    }

    public TourPoint(){}

    public static final Creator<TourPoint> CREATOR = new Creator<TourPoint>() {
        @Override
        public TourPoint createFromParcel(Parcel in) {
            return new TourPoint(in);
        }

        @Override
        public TourPoint[] newArray(int size) {
            return new TourPoint[size];
        }
    };


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Photo getPreviewPhoto() {
        return previewPhoto;
    }

    public void setPreviewPhoto(Photo previewPhoto) {
        this.previewPhoto = previewPhoto;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getStringCoordinates() {
        return latitude + "," + longitude;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void addPhoto(Photo photo) {
        this.photos.add(photo);
    }

    public void removePhoto(Photo photo) {
        this.photos.remove(photo);
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeDouble(longitude);
        parcel.writeDouble(latitude);
        parcel.writeParcelable(previewPhoto, 0);
        parcel.writeTypedList(photos);
        parcel.writeString(title);
        parcel.writeString(subTitle);
        parcel.writeString(description);
        parcel.writeParcelable(audio, 0);
    }
}
