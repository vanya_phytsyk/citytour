package one.citytour.domain.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by vanya on 6/7/2016.
 */
public class Photo implements Parcelable{
    private long id;
    private String url;

    protected Photo(Parcel in) {
        id = in.readLong();
        url = in.readString();
    }

    public Photo(){}

    public Photo(long id, String url) {
        this.id = id;
        this.url = url;
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public String getScreenDependentUrl(Context context) {
        int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
        int screenHeight = context.getResources().getDisplayMetrics().heightPixels;
        return String.format(context.getResources().getConfiguration().locale, "%s?w=%d", url, Math.min(screenWidth, screenHeight));
    }

    public String getResizedtUrl(int size) {
        return String.format("%s?w=%d", url, size);
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(url);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Photo photo = (Photo) o;

        if (id != photo.id) return false;
        return url.equals(photo.url);

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + url.hashCode();
        return result;
    }
}
