package one.citytour.domain.model;

import java.util.Collections;
import java.util.List;

/**
 * Created by vanya on 7/31/2016.
 */
public class TourListResponse {
    private List<Tour> data = Collections.emptyList();
    private int total;

    public List<Tour> getData() {
        return data;
    }

    public boolean hasTours() {
        return data != null && !data.isEmpty();
    }

    public void setData(List<Tour> data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
