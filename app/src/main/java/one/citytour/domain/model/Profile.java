package one.citytour.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by vanya on 7/3/2016.
 */
public class Profile implements Parcelable {
    private long id;
    private String firstName;
    private String lastName;
    private Photo photo;

    private boolean isSelf;

    public Profile() {
    }

    public Profile(long id, String firstName, String lastName, Photo photo, boolean isSelf) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photo = photo;
        this.isSelf = isSelf;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setSelf(boolean self) {
        isSelf = self;
    }

    @Override
    public boolean equals(Object secondProfile) {
        return secondProfile != null
                && secondProfile instanceof Profile
                && ((Profile) secondProfile).getId() == this.id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeParcelable(this.photo, flags);
        dest.writeByte(this.isSelf ? (byte) 1 : (byte) 0);
    }

    protected Profile(Parcel in) {
        this.id = in.readLong();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.photo = in.readParcelable(Photo.class.getClassLoader());
        this.isSelf = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel source) {
            return new Profile(source);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
