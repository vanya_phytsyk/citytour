package one.citytour.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanya on 17.04.16.
 */
public class Tour implements Parcelable {

    private long id;

    private String title;
    private Photo coverPhoto;
    private String description;
    private List<TourPoint> points = new ArrayList<>();
    private float rating;
    private Profile creator;
    private boolean isSaved;

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    private boolean isPrivate;

    public Tour() {
    }

    public Tour(long id) {
        this.id = id;
    }

    public List<TourPoint> getPoints() {
        return points;
    }

    public void addPoint(TourPoint point) {
        points.add(point);
    }

    public void setPoints(List<TourPoint> points) {
        this.points = points;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Photo getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(Photo coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    public boolean hasPoints() {
        return !points.isEmpty();
    }

    public double getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public Profile getCreator() {
        return creator;
    }

    public boolean hasCreator() {
        return creator != null;
    }

    public void setCreator(Profile creator) {
        this.creator = creator;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Tour && ((Tour) o).getId() == id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.title);
        dest.writeParcelable(this.coverPhoto, flags);
        dest.writeString(this.description);
        dest.writeTypedList(this.points);
        dest.writeFloat(this.rating);
        dest.writeParcelable(this.creator, flags);
        dest.writeByte(this.isSaved ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isPrivate ? (byte) 1 : (byte) 0);
    }

    protected Tour(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
        this.coverPhoto = in.readParcelable(Photo.class.getClassLoader());
        this.description = in.readString();
        this.points = in.createTypedArrayList(TourPoint.CREATOR);
        this.rating = in.readFloat();
        this.creator = in.readParcelable(Profile.class.getClassLoader());
        this.isSaved = in.readByte() != 0;
        this.isPrivate = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Tour> CREATOR = new Parcelable.Creator<Tour>() {
        @Override
        public Tour createFromParcel(Parcel source) {
            return new Tour(source);
        }

        @Override
        public Tour[] newArray(int size) {
            return new Tour[size];
        }
    };
}
