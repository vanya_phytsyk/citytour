package one.citytour.domain.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vanya on 7/3/2016.
 */
public class AuthResponse {
    @SerializedName("token")
    private String accessToken;

    @SerializedName("refresh_token")
    private String refreshToken;

    @SerializedName("ttl")
    private long expiredPeriod;

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public long getExpiredPeriod() {
        return expiredPeriod;
    }
}