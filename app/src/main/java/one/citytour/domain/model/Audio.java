package one.citytour.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by vanya on 6/7/2016.
 */
public class Audio implements Parcelable{
    private long id;
    private String url;

    protected Audio(Parcel in) {
        id = in.readLong();
        url = in.readString();
    }

    public Audio() {}

    public Audio(long id, String url) {
        this.id = id;
        this.url = url;
    }

    public static final Creator<Audio> CREATOR = new Creator<Audio>() {
        @Override
        public Audio createFromParcel(Parcel in) {
            return new Audio(in);
        }

        @Override
        public Audio[] newArray(int size) {
            return new Audio[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(url);
    }
}
