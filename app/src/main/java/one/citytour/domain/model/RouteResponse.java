package one.citytour.domain.model;

import java.util.List;

/**
 * Created by vanya on 18.04.16.
 */
public class RouteResponse {

    public List<Route> routes;

    public String getPoints() {
        return this.routes.get(0).overview_polyline.points;
    }

    public boolean hasPoints() {
        return !routes.isEmpty();
    }

    class Route {
        OverviewPolyline overview_polyline;
    }

    class OverviewPolyline {
        String points;
    }
}