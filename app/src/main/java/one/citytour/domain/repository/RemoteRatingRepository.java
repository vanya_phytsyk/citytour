package one.citytour.domain.repository;

import one.citytour.domain.model.FeedBack;
import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;
import rx.Observable;

/**
 * Created by vanya on 8/1/2016.
 */
public interface RemoteRatingRepository {
    public Observable<FeedBack> updateRating(Tour tour, TourPoint point, FeedBack newRating);
}
