package one.citytour.domain.repository;

import one.citytour.domain.model.Tour;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import rx.Observable;

/**
 * Created by vanya on 6/13/2016.
 */
public interface LocalRouteRepository {
    public Observable<List<LatLng>> getRouteForTour(long tourId);

    public Observable<Tour> saveRoute(Tour tour, List<LatLng> latLngs);

    public void removeRoute(long tourId);
}
