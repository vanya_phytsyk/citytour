package one.citytour.domain.repository;

import one.citytour.domain.model.AuthResponse;

/**
 * Created by vanya on 7/3/2016.
 */
public interface AuthLocalRepository {
    String getAccessToken();
    String getRefreshToken();
    void clearSession();
    void storeAuthCredentials(String accessToken, String refreshToken, long expiredTime);
    void storeAuthCredentials(AuthResponse authResponse);
    boolean hasAccessToken();
    boolean hasRefreshToken();

    boolean isAuthByVK();
}
