package one.citytour.domain.repository;

import one.citytour.domain.model.Profile;
import rx.Observable;

/**
 * Created by vanya on 7/3/2016.
 */
public interface LocalProfileRepository {
    Observable<Profile> getProfile();
    Observable<Profile> storeProfile(Profile profile);
    Observable<Object> clearCurrentProfile();
}
