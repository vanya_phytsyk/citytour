package one.citytour.domain.repository;

import one.citytour.domain.model.Profile;
import rx.Observable;

/**
 * Created by vanya on 7/3/2016.
 */
public interface Authorizer {
    public enum AuthType {
        FB("facebook"), VK("vk");
        private String type;
        AuthType(String type) {
            this.type = type;
        }
        public String getType() {
            return type;
        }
    }

    Observable<Profile> auth(String token);
}
