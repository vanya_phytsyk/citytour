package one.citytour.domain.repository;

import one.citytour.domain.model.Tour;

import java.util.List;

import one.citytour.domain.model.TourPoint;
import rx.Observable;

/**
 * Created by vanya on 5/21/2016.
 */
public interface LocalTourRepository {
    Observable<List<Tour>> getSavedTours();

    Observable<Tour> save(Tour tour);

    void cache(List<Tour> tours);

    Observable<Tour> cache(Tour tour);

    Observable<List<Tour>> getCached();

    Observable<Tour> getSingleCached(long tourId);

    Observable<Tour> getSingleSaved(long tourId);

    void clearCache();

    void deleteFromSaved(Tour tour);

    Observable<TourPoint> getCachedPoint(long pointId);
}
