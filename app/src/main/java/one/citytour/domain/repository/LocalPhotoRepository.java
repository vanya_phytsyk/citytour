package one.citytour.domain.repository;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import one.citytour.domain.model.Photo;
import one.citytour.domain.model.Tour;
import rx.Observable;

/**
 * Created by vanya on 6/13/2016.
 */
public interface LocalPhotoRepository {
    public Observable<Photo> updatePhoto(Photo photo);
}
