package one.citytour.domain.repository;

import java.util.List;

import one.citytour.domain.model.Profile;
import one.citytour.domain.model.Tour;
import rx.Observable;

/**
 * Created by vanya on 5/21/2016.
 */
public interface RemoteProfileRepository {
    Observable<Profile> getProfile();
}
