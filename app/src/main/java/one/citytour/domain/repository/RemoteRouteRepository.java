package one.citytour.domain.repository;

import one.citytour.domain.model.Tour;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import rx.Observable;

/**
 * Created by vanya on 6/12/2016.
 */
public interface RemoteRouteRepository {
    Observable<List<LatLng>> getRoute(Tour tour);
}
