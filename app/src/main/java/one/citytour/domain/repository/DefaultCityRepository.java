package one.citytour.domain.repository;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by vanya on 7/21/2016.
 */
public interface DefaultCityRepository {
    String getDefaultCityName();
    LatLng getDefaultCityLatLng();
}
