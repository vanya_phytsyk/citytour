package one.citytour.domain.repository;

import okhttp3.ResponseBody;
import rx.Observable;

/**
 * Created by vanya on 6/9/2016.
 */
public interface RemoteFileRepository {
    public Observable<ResponseBody> getFile(String url);
}
