package one.citytour.domain.repository;

import one.citytour.domain.model.Tour;

import java.util.List;

import one.citytour.domain.model.TourListResponse;
import retrofit2.Response;
import rx.Observable;

/**
 * Created by vanya on 5/21/2016.
 */
public interface RemoteTourRepository {
    Observable<TourListResponse> getTours(double latitude, double longitude);

    Observable<Tour> getTour(long id);

    Observable<Tour> createTour(Tour tour);

    Observable<TourListResponse> getOwnTours();

    Observable<Tour> updateTour(Tour tour);

    Observable<Response<Void>> deleteTour(long id);
}
