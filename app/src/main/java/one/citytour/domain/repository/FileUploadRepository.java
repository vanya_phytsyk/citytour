package one.citytour.domain.repository;

import android.net.Uri;

import one.citytour.domain.model.Audio;
import one.citytour.domain.model.Photo;
import rx.Observable;

/**
 * Created by vanya on 7/8/2016.
 */
public interface FileUploadRepository {
    Observable<Photo> uploadPhoto(String filePath);

    Observable<Audio> uploadAudio(String audioPath);
}
