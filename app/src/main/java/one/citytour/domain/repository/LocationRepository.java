package one.citytour.domain.repository;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import rx.Observable;

/**
 * Created by vanya on 7/7/2016.
 */
public interface LocationRepository {
    public Observable<Location> getCurrentLocation();
}
