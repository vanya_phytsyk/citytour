package one.citytour.audio;

import android.media.MediaRecorder;
import android.net.Uri;
import android.util.Pair;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import one.citytour.domain.model.AuthResponse;
import one.citytour.presenter.helper.StorageHelper;
import rx.Observable;
import rx.functions.Func1;
import timber.log.Timber;

/**111111
 */
public class AudioRecorder {
    private MediaRecorder mediaRecorder;
    private String fileName;
    private Uri fileUri;
    public AudioRecorder(String fileName) {
        this.fileName = fileName;
    }

    public void startRecording() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        fileUri = StorageHelper.getInstance().getCachedFileUri(fileName);
        mediaRecorder.setOutputFile(fileUri.getPath());
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            Timber.e(e, "");
        }

        mediaRecorder.start();
    }

    public Uri getFileUri() {
        return fileUri;
    }

    public void stopRecording() {
        if (mediaRecorder != null) {
            mediaRecorder.stop();
            mediaRecorder.release();
            mediaRecorder = null;
        }
    }

    public Observable<Pair<Long, Integer>> getAmplitudeValue() {
        return Observable.interval(0, 100, TimeUnit.MILLISECONDS).flatMap(aLong -> {
            int amplitude = mediaRecorder.getMaxAmplitude();
            return Observable.just(new Pair<>(aLong, amplitude));
        });
    }

}


