package one.citytour.audio;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Pair;

import one.citytour.presenter.helper.StorageHelper;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import rx.observables.AsyncOnSubscribe;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by vanya on 5/6/2016.
 */
public class AudioService extends Service implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    public static final int START_ID = 562016; //some unique id
    private MediaPlayer player = new MediaPlayer();
    private final IBinder audioBinder = new AudioBinder();

    private PublishSubject<Object> playCompleteSubject;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return audioBinder;
    }

    @Override
    public void onCreate() {
        initMediaPlayer();
    }

    private void initMediaPlayer() {
        player.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (startId != START_ID) {
            return START_NOT_STICKY;
        }
        return START_NOT_STICKY;
    }

    public void playAudio(String audioUrl) {
        player.reset();
        Uri audioFileUri;
        if (isFileInCache(audioUrl)) {
            audioFileUri = StorageHelper.getInstance().getCachedFileUri(audioUrl);
        } else if (isAudioInStorage(audioUrl)) {
            audioFileUri = Uri.fromFile(getExternalFilesDir(null));
        } else {
            stopSelf();
            return;
        }
        playAudioFromSource(audioFileUri);
    }

    public void pause() {
        player.pause();
    }

    public void resume() {
        player.start();
    }

    public void stop() {
        player.stop();
        stopSelf();
    }

    @Override
    public void onDestroy() {
        player.stop();
        player.release();
    }

    private boolean isAudioInStorage(String url) {
        return StorageHelper.getInstance().isStored(url);
    }

    private boolean isFileInCache(String url) {
        return StorageHelper.getInstance().isCached(url);
    }

    public int getDuration() {
        return player.getDuration();
    }

    public Observable<Pair<Integer, Integer>> getCurrentPosition() {
        return Observable.interval(0, 1, TimeUnit.SECONDS).flatMap(new Func1<Long, Observable<Pair<Integer, Integer>>>() {
            @Override
            public Observable<Pair<Integer, Integer>> call(Long position) {
                return Observable.just(new Pair<>(player.getCurrentPosition() / 1000, player.getDuration() / 1000));
            }
        });
    }

    private void playAudioFromSource(Uri uri) {
        try {
            player.setDataSource(getApplicationContext(), uri);
        } catch (IOException e) {
            Log.e(AudioService.class.getSimpleName(), "Cannot set audio source", e);
            stopSelf();
            return;
        }
        player.prepareAsync();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        player.stop();
        player.release();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (playCompleteSubject != null) {
            playCompleteSubject.onNext(null);
        }
        Log.d(AudioService.class.getSimpleName(), "Player complete");
    }

    public void setPlayCompleteSubject(PublishSubject<Object> playCompleteSubject) {
        this.playCompleteSubject = playCompleteSubject;
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.e(AudioService.class.getSimpleName(), "Player error code " + what);

        return true;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        player.start();
    }

    public void moveToPosition(int newPosition) {
        player.seekTo(newPosition * 1000); //seconds to milisec
    }

    public class AudioBinder extends Binder {
        AudioService getService() {
            return AudioService.this;
        }
    }
}
