package one.citytour.audio;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.util.Pair;

import one.citytour.CityTourApplication;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by vanya on 5/6/2016.
 */
public class AudioPlayer {

    private PublishSubject<Object> playCompleteSubject;

    private enum AudioState {STOPPED, PAUSED, PLAYING}

    private boolean isAudioBound;
    private AudioService audioService;
    private Context context;
    private AudioState audioState = AudioState.STOPPED;
    private String currentAudioUrl;

    private Runnable pendingTask;
    private Handler handler = new Handler();

    private AudioPlayer() {
        context = CityTourApplication.getContext();
        initService();
    }

    private static class LazyHolder {
        private static final AudioPlayer INSTANCE = new AudioPlayer();
    }

    public static AudioPlayer getInstance() {
        return LazyHolder.INSTANCE;
    }

    private void initService() {
        Intent playIntent = new Intent(context, AudioService.class);
        context.bindService(playIntent, audioConnection, Context.BIND_AUTO_CREATE);
        context.startService(playIntent);
    }

    public boolean isPlaying() {
        return audioState == AudioState.PLAYING;
    }

    public boolean isPaused() {
        return audioState == AudioState.PAUSED;
    }

    public String getCurrentAudioUrl() {
        return currentAudioUrl;
    }

    public void resumeAudio() {
        audioState = AudioState.PLAYING;
        audioService.resume();
    }

    public void playAudio(final String url) {
        if (isAudioBound) {
            playAudioFromUrl(url);
        } else {
            pendingTask = () -> {
                playAudioFromUrl(url);
                pendingTask = null;
            };
        }
    }

    public int getDuration() {
        return audioService.getDuration();
    }

    public Observable<Pair<Integer, Integer>> getCurrentPosition() {
        return audioService.getCurrentPosition();
    }

    private void playAudioFromUrl(String url) {
        audioService.playAudio(url);
        audioState = AudioState.PLAYING;
        currentAudioUrl = url;
    }

    public void setPlayCompleteSubject(PublishSubject<Object> playCompleteSubject) {
        this.playCompleteSubject = playCompleteSubject;
        playCompleteSubject.subscribe(o -> {
            audioState = AudioState.STOPPED;
        });
        if (isAudioBound) {
            audioService.setPlayCompleteSubject(playCompleteSubject);
        }
    }

    private ServiceConnection audioConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            AudioService.AudioBinder binder = (AudioService.AudioBinder) service;
            audioService = binder.getService();
            if (playCompleteSubject != null) {
                audioService.setPlayCompleteSubject(playCompleteSubject);
            }
            isAudioBound = true;
            if (pendingTask != null) {
                handler.post(pendingTask);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isAudioBound = false;
        }
    };

    public void moveToPosition(int newPosition) {
        audioService.moveToPosition(newPosition);
    }

    public void pauseAudio() {
        audioService.pause();
        audioState = AudioState.PAUSED;
    }

    public void stopAudio() {
        if (audioService != null) {
            audioService.stop();
        }
        audioState = AudioState.STOPPED;
    }

}
