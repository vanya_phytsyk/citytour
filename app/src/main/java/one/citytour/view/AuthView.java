package one.citytour.view;

/**
 * Created by vanya on 7/3/2016.
 */
public interface AuthView extends BaseView {

    void navigateToTourList();
    void authError();


}
