package one.citytour.view;

import android.location.Location;

import java.util.List;

import one.citytour.domain.model.Photo;
import one.citytour.domain.model.TourPoint;

/**
 * Created by vanya on 7/5/2016.
 */
public interface NewTourView extends BaseView {
    void showCoverPhotoDownloading(Photo photo);

    void showCoverPhotoDownloaded(Photo photo);

    void removeCoverPhoto();

    void addPoint(TourPoint tourPoint);

    void removePoint(int pointIndex);

    String getTourTitle();

    String getTourSubtitle();

    boolean isPrivate();

    void showTitle(String title);

    void showDescription(String description);

    void showPoints(List<TourPoint> points);

    void showNoPhotoAlert();

    void showNoTitleAlert();

    void showNoDescriptionAlert();

    void showNoPointsAlert();

    void exit();

    void navigateToPointUpdate(TourPoint point);

    void navigateToPointCreate();

    void updatePoint(TourPoint point, int pointWaitingForUpdatinIndex);
}
