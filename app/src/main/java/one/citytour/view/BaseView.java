package one.citytour.view;

/**
 * Created by vanya on 5/9/2016.
 */
public interface BaseView {
    public void showProgress();

    public void hideProgress();

    public void showError(String message);

    void showNoConnectionView();

    void hideNoConnectionView();

    void showEmptyView();

    void hideEmptyView();
}
