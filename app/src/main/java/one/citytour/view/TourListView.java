package one.citytour.view;

import one.citytour.R;
import one.citytour.domain.model.Tour;
import rx.subjects.PublishSubject;

import java.util.List;

/**
 * Created by vanya on 5/8/2016.
 */
public interface TourListView extends BaseView {

    enum MenuItem {CACHED, SAVED, OWN}

    enum TourMenuItem {
        SAVE(R.string.save), UPDATE(R.string.update), DELETE_FROM_SAVED(R.string.delete_from_saved), REMOVE(R.string.delete), SHARE(R.string.share);

        TourMenuItem(int stringResId) {
            this.stringResId = stringResId;
        }

        private int stringResId;

        public int getStringResId() {
            return stringResId;
        }
    }

    void navigateToTourUpdate(long tourId);

    void selectMenuItem(MenuItem item);

    void showShareDialog(Tour tour);

    void showVKShareDialog(Tour tour, PublishSubject<Object> resultSubject);

    void showPlaceSelectView();

    void updatePlaceName(String name);

    void showPlaceName();

    void hidePlaceName();

    void showTourList(List<Tour> tours);

    void navigateToTourView(Tour tour);

    void showTourMenu(Tour tour, List<TourMenuItem> tourMenuItems);

    void navigateToSettings();

    void showTourSavedSuccess();

    void hideTourItem(Tour tour);

    void navigateToAuthorize();

    void navigateToVKAuthorize();

    void updateProfileAvatar(String avatarUrl);

    void updateProfileName(String userName);

    void showNewTourButton();

    void hideNewTourButton();

    void clearProfileInfo();

    void navigateToNewTour();

    void updateTour(Tour tour);

    void exit();
}
