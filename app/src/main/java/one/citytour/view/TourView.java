package one.citytour.view;

import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by vanya on 17.04.16.
 */
public interface TourView {
    public void showTour(Tour tour);
    public void showTourRoute(List<LatLng> tour);
    public void showTourPointDetails(Tour tour, TourPoint tourPoint);
}
