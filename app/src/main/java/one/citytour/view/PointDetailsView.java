package one.citytour.view;

import one.citytour.domain.model.TourPoint;
import rx.subjects.PublishSubject;

/**
 * Created by ivan on 23.04.16.
 */
public interface PointDetailsView {
    void showPointDetails(TourPoint tourPoint);
    void showPlaying(boolean showPlayIcon);
    void navigateToPhotos(long pointId);
    void showPlayProgress();
    void hidePlayProgress();

    void showUpdateRatingDialog(float rating);

    void showProgress();

    void updateRatingView(int rating);

    void hideProgress();

    void updateCurrentPlayingPosition(int position, int max);

    void setAudioChangeProgressSubject(PublishSubject<Integer> audioProgressChangeSubject);
}
