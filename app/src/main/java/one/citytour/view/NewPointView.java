package one.citytour.view;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import one.citytour.domain.model.Photo;
import one.citytour.domain.model.TourPoint;

/**
 * Created by vanya on 7/5/2016.
 */
public interface NewPointView extends BaseView{
    void updateLocation(Location location);
    void updateLocation(LatLng latLng);
    void updateLocation();

    void showPhotoDownloading(Photo photo);
    void showPhotoDownloaded(Photo photo);
    void showPhotosDownloaded(List<Photo> photos);
    void showAudioProgress();
    void hideAudioProgress();
    void showAudioUploadSuccess();
    void showCoverPhotoDownloading(Photo photo);
    void showCoverPhotoDownloaded(Photo photo);

    String getPointTitle();
    String getPointDescription();
    String getPointText();
    LatLng getLocation();

    void showUploadAudioAlert();
    void showUploadCoverPhotoAlert();
    void showUploadPhotoAlert();
    void showWaitUntilUpload();

    void removePhoto(Photo photo);
    void removeCoverPhoto();

    void returnToNewTour(TourPoint point);

    void showTitle(String title);
    void showDescription(String description);
    void showText(String text);
}
