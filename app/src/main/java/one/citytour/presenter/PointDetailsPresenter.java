package one.citytour.presenter;

import android.util.Log;

import one.citytour.audio.AudioPlayer;
import one.citytour.domain.model.FeedBack;
import one.citytour.domain.model.Tour;
import one.citytour.domain.repository.impl.Injector;
import one.citytour.domain.model.Audio;
import one.citytour.domain.model.TourPoint;
import one.citytour.presenter.helper.StorageHelper;
import one.citytour.view.PointDetailsView;

import java.io.IOException;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import timber.log.Timber;

/**
 * Created by ivan on 23.04.16.
 */
public class PointDetailsPresenter implements BasePresenter {

    private PointDetailsView pointDetailsView;
    private TourPoint tourPoint;
    private Tour tour;

    private Subscription audioProgressSubscription;
    private Subscription audioChangePositionSubscription;

    public PointDetailsPresenter() {
    }

    public void setView(PointDetailsView pointDetailsView) {
        this.pointDetailsView = pointDetailsView;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public void update(TourPoint tourPoint) {
        this.tourPoint = tourPoint;
        pointDetailsView.showPointDetails(tourPoint);
        pointDetailsView.showPlaying(isCurrentAudioInPlayer());
        if (!isCurrentAudioInPlayer()) {
            pointDetailsView.updateCurrentPlayingPosition(0, 0);
        }
        PublishSubject<Object> playCompletionSubject = PublishSubject.create();
        AudioPlayer.getInstance().setPlayCompleteSubject(playCompletionSubject);
        playCompletionSubject.subscribe(o -> {
            pointDetailsView.showPlaying(false);
        });
    }

    private boolean isCurrentAudioInPlayer() {
        return tourPoint.getAudio().getUrl().equals(AudioPlayer.getInstance().getCurrentAudioUrl());
    }

    public void playAudioClick() {
        AudioPlayer audioPlayer = AudioPlayer.getInstance();
        boolean isAudioPlaying = audioPlayer.isPlaying();
        boolean isCurrentPointAudioInPlayer = isCurrentAudioInPlayer();
        if (isCurrentPointAudioInPlayer) {
            if (AudioPlayer.getInstance().isPlaying()) {
                AudioPlayer.getInstance().pauseAudio();
                pointDetailsView.showPlaying(false);
                return;
            } else if (AudioPlayer.getInstance().isPaused()) {
                AudioPlayer.getInstance().resumeAudio();
                pointDetailsView.showPlaying(true);
                return;
            }
        } else {
            if (isAudioPlaying) {
                audioPlayer.stopAudio();
            }
        }

        final Audio audio = tourPoint.getAudio();
        if (StorageHelper.getInstance().isCached(audio.getUrl()) || StorageHelper.getInstance().isStored(audio.getUrl())) {
            playAudio();
        } else {
            pointDetailsView.showPlayProgress();
            Injector.provideRemoteFileRepository().getFile(audio.getUrl()).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(responseBody -> {
                        try {
                            byte[] bytes = responseBody.bytes();
                            StorageHelper.getInstance().cacheFile(audio.getUrl(), bytes, uri -> {
                                        playAudio();
                                        pointDetailsView.hidePlayProgress();
                                    }
                                    , throwable -> {
                                        pointDetailsView.hidePlayProgress();
                                        Log.e(PointDetailsPresenter.class.getSimpleName(), "Cannot save audio in cache");
                                    }
                            );
                        } catch (IOException e) {
                            pointDetailsView.hidePlayProgress();
                            Log.e(PointDetailsPresenter.class.getSimpleName(), "Cannot get bytes[] from response");
                        }

                    }, throwable -> {
                        pointDetailsView.showPlaying(false);
                        pointDetailsView.showPlaying(true);
                        Log.e(PointDetailsPresenter.class.getSimpleName(), "Response failure");
                    });
        }
    }

    private void playAudio() {
        AudioPlayer.getInstance().playAudio(tourPoint.getAudio().getUrl());
        pointDetailsView.showPlaying(true);
        subscribeAudioProgress();
    }

    private void subscribeAudioProgress() {
        if (audioProgressSubscription == null) {
            audioProgressSubscription = AudioPlayer.getInstance(
            ).getCurrentPosition()
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter(pair -> isCurrentAudioInPlayer())
                    .subscribe(pair -> {
                        pointDetailsView.updateCurrentPlayingPosition(pair.first, pair.second);
                    });
        }
    }

    public void photoClick() {
        pointDetailsView.navigateToPhotos(tourPoint.getId());
    }

    @Override
    public void resume() {
        if (tourPoint != null) {
            update(tourPoint);
        }
        PublishSubject<Integer> audioProgressChangeSubject = PublishSubject.create();

        audioChangePositionSubscription = audioProgressChangeSubject
                .observeOn(AndroidSchedulers.mainThread())
                .filter(integer -> isCurrentAudioInPlayer())
                .subscribe(integer -> {
                    AudioPlayer.getInstance().moveToPosition(integer);
                });
        pointDetailsView.setAudioChangeProgressSubject(audioProgressChangeSubject);
    }

    @Override
    public void pause() {
        audioChangePositionSubscription.unsubscribe();
        audioChangePositionSubscription = null;

    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {
        audioProgressSubscription.unsubscribe();
        audioProgressSubscription = null;
    }

    public void ratingClick() {
        if (Injector.provideAuthLocalRepository().hasAccessToken()) {
            pointDetailsView.showUpdateRatingDialog(tourPoint.getRating());
        }
    }

    public void ratingChosen(int rating) {
        pointDetailsView.showProgress();
        Injector.provideRemoteRatingRepository().updateRating(tour, tourPoint, new FeedBack(rating, ""))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rating1 -> {
                    tourPoint.setRating(rating);
                    pointDetailsView.updateRatingView(rating);
                    pointDetailsView.hideProgress();
                }, throwable -> {
                    Timber.e(throwable, "");
                });
    }
}
