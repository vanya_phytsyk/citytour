package one.citytour.presenter;

import android.util.Log;

import one.citytour.domain.model.TourListResponse;
import one.citytour.domain.repository.DefaultCityRepository;
import one.citytour.domain.repository.LocalTourRepository;
import one.citytour.domain.repository.RemoteTourRepository;
import one.citytour.domain.repository.impl.Injector;
import one.citytour.presenter.helper.ConnectivityHelper;
import one.citytour.presenter.helper.TourSaver;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.api.VKApi;

import one.citytour.domain.model.Tour;
import one.citytour.view.TourListView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import timber.log.Timber;

/**
 * Created by vanya on 5/9/2016.
 */
public class TourListPresenter implements BasePresenter {
    private TourListView tourListView;
    private Place place;
    RemoteTourRepository remoteTourRepository;
    LocalTourRepository localTourRepository;

    private enum TourType {CACHED, SAVED, OWN}

    private TourType tourType = TourType.CACHED;

    public TourListPresenter() {
        remoteTourRepository = Injector.provideRemoteTourRepository();
        localTourRepository = Injector.provideLocalTourRepository();
    }

    public void placeSelectionClick() {
        tourListView.showPlaceSelectView();
    }

    private void showSelectedPlaceName(CharSequence name) {
        tourListView.updatePlaceName(String.valueOf(name));
    }

    public void placeSelected(Place place) {
        this.place = place;
        requestToursForPlace(place.getLatLng());
        showSelectedPlaceName(place.getName());
    }

    public void refreshTourListClicked() {
        refreshTours();
    }

    private void refreshTours() {
        switch (tourType) {
            case CACHED:
                if (place != null) {
                    requestToursForPlace(place.getLatLng());
                } else {
                    loadDefaultCity();
                }
                break;
            case SAVED:
                tourListView.hideProgress();
                loadSavedTours();
                break;
            case OWN:
                loadOwnTours();
                break;
        }
    }

    public void userCredentialsClick() {
        if (!Injector.provideAuthLocalRepository().hasAccessToken()) {
            tourListView.navigateToAuthorize();
        }
    }

    private boolean isDefaultCityLoaded;

    private void requestToursForPlace(LatLng latLng) {
        if (ConnectivityHelper.isNetworkAvailable()) {
            tourListView.showProgress();
            Observable<TourListResponse> listObservable = remoteTourRepository.getTours(latLng.latitude, latLng.longitude);
            listObservable
                    .subscribe(tours -> {
                                tourListView.hideProgress();
                                if (tours.hasTours()) {
                                    localTourRepository.clearCache();
                                    localTourRepository.cache(tours.getData());
                                    tourListView.showTourList(tours.getData());
                                    isDefaultCityLoaded = true;
                                } else {
                                    tourListView.showEmptyView();
                                    Log.e(TourPresenter.class.getSimpleName(), "Response not successful");
                                }
                            }
                            , throwable -> {
                                tourListView.hideProgress();
                                tourListView.showEmptyView();
                            });
        }
    }

    public void tourClick(Tour tour) {
        if (ConnectivityHelper.isNetworkAvailable() || tour.isSaved()) {
            tourListView.navigateToTourView(tour);
        } else {
            tourListView.showNoConnectionView();
        }
    }


    public void removeTourButtonClick(Tour tour) {
        Injector.provideRemoteTourRepository().deleteTour(tour.getId())
                .subscribe(voidResponse -> {
                    refreshTours();
                }, throwable -> Timber.e("", throwable));
    }


    public void tourShareButtonClick(Tour tour) {
        tourListView.showShareDialog(tour);
    }


    public void updateTourButtonClick(Tour tour) {
        tourListView.navigateToTourUpdate(tour.getId());
    }

    public void tourSaveButtonClick(Tour tour) {
        new TourSaver().saveTour(tour)
                .subscribe(o -> {
                    Timber.d("saved tour: %s", tour.getTitle());
                }, throwable -> {
                    Crashlytics.logException(throwable);
                    tourListView.showError(throwable.getMessage());
                }, () -> {
                    tourListView.showTourSavedSuccess();
                    tourListView.updateTour(tour);
                });
    }

    public void removeTourFromSavedButtonClick(Tour tour) {
        new TourSaver().deleteTour(tour)
                .subscribe(tour1 -> {
                            if (tourType == TourType.SAVED) {
                                tourListView.hideTourItem(tour1);
                                loadSavedTours();
                            }
                        }, throwable -> Timber.e(throwable, "", ""),
                        () -> {
                            tourListView.updateTour(tour);
                            Timber.d("Tour id: %d successfully deleted", tour.getId());
                        });
    }

    public void backPressed() {
        if (tourType == TourType.CACHED) {
            tourListView.exit();
        } else {
            tourType = TourType.CACHED;
            loadCachedTours();
            tourListView.selectMenuItem(TourListView.MenuItem.CACHED);
        }
    }

    public void searchMenuItemClick() {
        tourType = TourType.CACHED;
        loadCachedTours();
        updatePlaceView();
    }

    public void savedToursMenuItemClick() {
        tourType = TourType.SAVED;
        loadSavedTours();
        updatePlaceView();
    }

    public void ownToursMenuItemClick() {
        if (Injector.provideAuthLocalRepository().hasAccessToken()) {
            tourType = TourType.OWN;
            loadOwnTours();
            updatePlaceView();
        } else {
            tourListView.navigateToAuthorize();
        }
    }

    private void loadOwnTours() {
        tourListView.showProgress();
        Injector.provideRemoteTourRepository()
                .getOwnTours()
                .subscribe(tours -> {
                    if (tours.hasTours()) {
                        tourListView.showTourList(tours.getData());
                    } else {
                        tourListView.showEmptyView();
                    }
                    tourListView.hideProgress();
                }, throwable -> {
                    tourListView.hideProgress();
                    Timber.e(throwable, "");
                });
    }

    public void tourMenuButtonClicked(Tour tour) {
        Injector.provideLocalProfileRepository().getProfile()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(profile -> {
                    List<TourListView.TourMenuItem> menuItems = new ArrayList<>();
                    menuItems.add(TourListView.TourMenuItem.SHARE);
                    if (tour.hasCreator() && profile != null) {
                        if (profile.equals(tour.getCreator())) {
                            menuItems.add(TourListView.TourMenuItem.REMOVE);
                            menuItems.add(TourListView.TourMenuItem.UPDATE);
                        }
                    }
                    if (tour.isSaved()) {
                        menuItems.add(TourListView.TourMenuItem.DELETE_FROM_SAVED);
                    } else {
                        menuItems.add(TourListView.TourMenuItem.SAVE);
                    }
                    tourListView.showTourMenu(tour, menuItems);
                });
    }

    private void loadCachedTours() {
        localTourRepository.getCached()
                .subscribe(tours -> {
                    tourListView.showTourList(tours);
                });
    }

    private void loadSavedTours() {
        Injector.provideLocalTourRepository().getSavedTours().subscribe(savedTours -> {
            if (!savedTours.isEmpty()) {
                tourListView.showTourList(savedTours);
            } else {
                tourListView.showEmptyView();
            }
        });
    }

    public void settingsMenuItemClick() {
        tourListView.navigateToSettings();
    }

    private void loadDefaultCity() {
        DefaultCityRepository defaultCityRepository = Injector.provideDefaultCityRepository();
        requestToursForPlace(defaultCityRepository.getDefaultCityLatLng());
        showSelectedPlaceName(defaultCityRepository.getDefaultCityName());
    }

    @Override
    public void resume() {
        if (!isDefaultCityLoaded) {
            loadDefaultCity();
        }
        updatePlaceView();
        updateProfileInfo();
        switch (tourType) {
            case CACHED:
                loadCachedTours();
                break;
            case SAVED:
                loadSavedTours();
                break;
            case OWN:
                loadOwnTours();
                break;
        }
    }

    public void newTourButtonClicked() {
        if (Injector.provideAuthLocalRepository().hasAccessToken()) {
            tourListView.navigateToNewTour();
        } else {
            tourListView.navigateToAuthorize();
        }
    }

    private void updateProfileInfo() {
        Injector.provideLocalProfileRepository().getProfile()
                .subscribe(profile -> {
                    if (profile == null) {
                        tourListView.clearProfileInfo();
                    } else {
                        tourListView.updateProfileAvatar(profile.getPhoto().getUrl());
                        tourListView.updateProfileName(String.format("%s %s", profile.getFirstName(), profile.getLastName()));
                    }
                }, throwable -> {
                    Timber.e(throwable, "");
                });
    }

    private void updatePlaceView() {
        if (tourType == TourType.CACHED) {
            tourListView.showPlaceName();
            if (place != null) {
                tourListView.updatePlaceName(place.getName().toString());
            }
        } else {
            tourListView.hidePlaceName();
        }
    }

    public void fbShareClicked(Tour tour) {
        tourListView.showError("Not implemented yet");
    }

    public void vkShareClicked(Tour tour) {
        if (Injector.provideAuthLocalRepository().isAuthByVK()) {
            tourListView.showProgress();
            PublishSubject<Object> shareResultSubject = PublishSubject.create();
            shareResultSubject
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(o -> {
                        tourListView.hideProgress();
                    }, throwable -> {
                        Timber.e(throwable, "");
                        tourListView.hideProgress();
                    });
            tourListView.showVKShareDialog(tour, shareResultSubject);
        } else {
            tourListView.navigateToAuthorize();
        }
    }

    public void setView(TourListView view) {
        this.tourListView = view;
    }

    @Override
    public void pause() {

    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }
}
