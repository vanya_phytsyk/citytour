package one.citytour.presenter.helper;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;

import one.citytour.CityTourApplication;
import one.citytour.domain.filestorage.Cache;
import one.citytour.domain.filestorage.FailureCallback;
import one.citytour.domain.filestorage.Storage;
import one.citytour.domain.filestorage.SuccessCallback;
import timber.log.Timber;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by vanya on 5/7/2016.
 */
public class StorageHelper implements Cache, Storage {
    private Context context;
    File cacheDir;
    File filesDir;

    private StorageHelper() {
        context = CityTourApplication.getContext();
        cacheDir = isExternalStorageReadable() && isExternalStorageWritable()
                ? context.getExternalCacheDir() : context.getCacheDir();
        filesDir = isExternalStorageReadable() && isExternalStorageWritable()
                ? context.getExternalFilesDir(null) : context.getFilesDir();
    }

    @Override
    public void storeFile(String url, byte[] bytes, SuccessCallback successCallback, FailureCallback failureCallback) {
        File file = getFileFromStorage(url);
        writeToFile(bytes, file, successCallback, failureCallback);
    }

    @Override
    public boolean isStored(String url) {
        File file = getFileFromStorage(url);
        return file.exists();
    }

    @Override
    public Uri getStoredFileUri(String url) {
        File file = getFileFromStorage(url);
        return Uri.fromFile(file);
    }

    private static class LazyHolder {
        private static final StorageHelper INSTANCE = new StorageHelper();
    }

    public static StorageHelper getInstance() {
        return LazyHolder.INSTANCE;
    }

    public boolean isCached(String url) {
        File file = getFileFromCache(url);
        return file.exists();
    }

    public Uri getCachedFileUri(String url) {
        File file = getFileFromCache(url);
        return Uri.fromFile(file);
    }

    private File getFileFromCache(String url) {
        return new File(cacheDir.getPath(), getFileName(url));
    }

    private File getFileFromStorage(String url) {
        return new File(filesDir.getPath(), getFileName(url));
    }

    public void cacheFile(final String url, final byte[] bytes, final SuccessCallback successCallback, FailureCallback failureCallback) {
        File file = getFileFromCache(url);
        writeToFile(bytes, file, successCallback, failureCallback);
    }

    private void writeToFile(final byte[] bytes, File file, final SuccessCallback successCallback, FailureCallback failureCallback) {
        final Handler handler = new Handler(Looper.getMainLooper());
        new Thread(() -> {
            try {
                FileOutputStream outputStream;
                outputStream = new FileOutputStream(file);
                outputStream.write(bytes);
                outputStream.close();
                handler.post(() -> successCallback.onSuccess(Uri.fromFile(file)));
            } catch (Exception e) {
                Timber.d("writeTofile exception %s", e.getMessage());
                handler.post(() -> failureCallback.onFailure(e));
            }
        }).start();
    }

    @Override
    public void removeFromStorage(String url) {
        if (isStored(url)) {
            getFileFromStorage(url).delete();
        }
    }

    @Override
    public void remoteFromStorageByUri(String uri) {
        new File(Uri.parse(uri).getPath()).delete();
    }

    /* Checks if external storage is available for read and write */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    private boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    private String getFileName(String url) {
        return md5(url);
    }

    private String md5(String s) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes(Charset.forName("US-ASCII")), 0, s.length());
            byte[] magnitude = digest.digest();
            BigInteger bi = new BigInteger(1, magnitude);
            String hash = String.format("%0" + (magnitude.length << 1) + "x", bi);
            return hash;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}
