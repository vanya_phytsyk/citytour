package one.citytour.presenter;

/**
 * Created by vanya on 17.04.16.
 */
public interface BasePresenter {
    public void resume();
    public void pause();
    public void start();
    public void stop();
}
