package one.citytour.presenter;

import com.facebook.FacebookException;
import com.vk.sdk.api.VKError;

import one.citytour.domain.repository.Authorizer;
import one.citytour.domain.repository.impl.Injector;
import one.citytour.view.AuthView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by vanya on 7/3/2016.
 */
public class AuthPresenter implements BasePresenter {

    private AuthView authView;

    public void setAuthView(AuthView authView) {
        this.authView = authView;
    }

    public void authSuccessVK(String authToken) {
        auth(Authorizer.AuthType.VK, authToken);
    }

    public void authSuccessFB(String authToken) {
        auth(Authorizer.AuthType.FB, authToken);
    }

    private void auth(Authorizer.AuthType authType, String authToken) {
        authView.showProgress();
        Injector.provideAuthorizer(authType).auth(authToken).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(o -> {
            authView.hideProgress();
            authView.navigateToTourList();
        }, throwable -> {
            authView.hideProgress();
            Timber.e(throwable, "");
        });
    }

    public void authError(FacebookException error) {
        authView.showError(error.getMessage());
    }

    public void authError(VKError error) {
        authView.showError(error.errorMessage);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }
}
