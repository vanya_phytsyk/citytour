package one.citytour.presenter;

import com.crashlytics.android.Crashlytics;

import one.citytour.domain.model.Audio;
import one.citytour.domain.model.Photo;
import one.citytour.domain.repository.RemoteTourRepository;
import one.citytour.domain.repository.impl.Injector;

import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;
import one.citytour.presenter.helper.ConnectivityHelper;
import one.citytour.view.TourView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by vanya on 17.04.16.
 */
public class TourPresenter implements BasePresenter {

    private TourView tourView;
    private long tourId;
    private RemoteTourRepository remoteTourRepository;
    private Tour tour;

    public TourPresenter() {
        remoteTourRepository = Injector.provideRemoteTourRepository();
    }

    public void setTourId(long tourId) {
        this.tourId = tourId;
    }

    public void setTourView(TourView tourView) {
        this.tourView = tourView;
    }

    @Override
    public void resume() {

    }

    public void mapReady() {
        Injector.provideLocalTourRepository().getSingleSaved(tourId)
                .filter(tour1 -> tour1 != null)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .zipWith(Injector.provideLocalRouteRepository().getRouteForTour(tourId), (tour1, latLngs) -> {
                    tourView.showTourRoute(latLngs);
                    return tour1;
                })
                .subscribe(tour1 -> {
                    this.tour = tour1;
                    tourView.showTour(tour1);
                    tourView.showTourPointDetails(tour, tour.getPoints().get(0));
                }, throwable -> {
                    Timber.e(throwable, "", "");
                });
        if (ConnectivityHelper.isNetworkAvailable()) {
            requestTour();
        }
    }

    private void requestTour() {
        remoteTourRepository.getTour(tourId).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tour -> {
                    this.tour = tour;
                    Injector.provideLocalTourRepository().cache(Collections.singletonList(tour));
                    tourView.showTour(tour);
                    requestRoute();
                }, throwable -> {
                    Timber.e(throwable, "");
                });
    }

    private void requestRoute() {
        Injector.provideRemoteRouteRepository().getRoute(tour)
                .doOnNext(latLngs -> {
                    Injector.provideLocalRouteRepository().saveRoute(tour, latLngs);
                })
                .subscribeOn(Schedulers.newThread()).
                observeOn(AndroidSchedulers.mainThread()).subscribe(latLngs -> {
            tourView.showTourRoute(latLngs);
        }, throwable -> {
            Crashlytics.log(throwable.getMessage());
        });
    }

    private Tour getFakeTour() {
        Tour tour = new Tour();

        tour.setId(56456);
        tour.setTitle("Lviv Architecture tour");
        tour.setDescription("Most famous Lviv buildings");
        tour.setCoverPhoto(new Photo(123123557456L, "https://upload.wikimedia.org/wikipedia" +
                "/commons/8/84/%D0%9B%D1%8C%D0%B2%D0%BE%D0%B2%D1%81%D0%BA%D0%B0%D1%8F_%D1%80%D0%B0%D1%82%D1%83%D1%88%D0%B0.jpg"));

        List<Photo> photos = new ArrayList<>();
        photos.add(new Photo(849348792, "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Lw%C3%B3w_-_Ratusz.jpg/800px-Lw%C3%B3w_-_Ratusz.jpg"));
        photos.add(new Photo(8122222, "https://upload.wikimedia.org/wikipedia/commons/c/cb/%D0%9B%D1%8C%D0%B2%D1%96%D0%B2_%D1%80%D0%B0%D1%82%D1%83%D1%88%D0%B0_%D1%80%D0%B5%D0%BA%D0%BE%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D1%96%D1%8F.jpg"));
        photos.add(new Photo(82202020, "https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/%D0%A6%D0%B5%D0%BD%D1%82%D1%80_%D0%9B%D1%8C%D0%B2%D0%BE%D0%B2%D0%B0_001.jpg/1280px-%D0%A6%D0%B5%D0%BD%D1%82%D1%80_%D0%9B%D1%8C%D0%B2%D0%BE%D0%B2%D0%B0_001.jpg"));
        photos.add(new Photo(3833838, "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Lviv-ratusha-1.jpg/1280px-Lviv-ratusha-1.jpg"));
        photos.add(new Photo(4738383, "https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Lviv-ratusha-2.jpg/1280px-Lviv-ratusha-2.jpg"));

        String loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci\n" +
                "        nisl, vehicula eu placerat sed, mattis sed orci. Vivamus non blandit urna. Aliquam molestie\n" +
                "        felis et risus pellentesque, vel accumsan orci tempor. Aliquam nulla enim, finibus nec\n" +
                "        feugiat eget, gravida sit amet velit. Aliquam finibus, velit ac imperdiet volutpat, tellus\n" +
                "        nulla ultrices nunc, nec accumsan enim turpis et sapien. Vestibulum ante ipsum primis in\n" +
                "        faucibus orci luctus et ultrices posuere cubilia Curae; Aenean quis augue turpis. Ut\n" +
                "        interdum, augue ac aliquam vulputate, mi mauris mollis augue, vitae molestie ligula velit in\n" +
                "        augue. Proin pharetra lobortis ultrices. Curabitur libero mauris, feugiat in viverra at,\n" +
                "        vehicula a nulla. Proin blandit id urna vitae dapibus. Phasellus interdum et tortor in\n" +
                "        fermentum. Integer sed tristique magna. Praesent nisi tortor, finibus nec rutrum ac, commodo\n" +
                "        in velit. Phasellus lacus risus, pretium id ullamcorper ac, tincidunt ut neque. Aliquam\n" +
                "        accumsan, libero eu tincidunt dignissim, orci ex pretium elit, id aliquam diam mauris nec\n" +
                "        ipsum.";

        TourPoint ratusha = new TourPoint();
        ratusha.setLatitude(49.841860);
        ratusha.setLongitude(24.031335);
        ratusha.setPreviewPhoto(new Photo(933848, "https://upload.wikimedia.org/wikipedia/commons/8/84/%D0%9B%D1%8C%D0%B2%D0%BE%D0%B2%D1%81%D0%BA%D0%B0%D1%8F_%D1%80%D0%B0%D1%82%D1%83%D1%88%D0%B0.jpg"));
        ratusha.setTitle("Ratusha");
        ratusha.setSubTitle("Lvivska Ratusha");
        ratusha.setAudio(new Audio(329234239, "https://www.dropbox.com/s/ieuae7q36hg1ggc/Ploshcha-rynok.wav?dl=1"));
        ratusha.setDescription(loremIpsum);
        ratusha.setPhotos(photos);

        TourPoint opera = new TourPoint();
        opera.setLatitude(49.844167);
        opera.setLongitude(24.026389);
        opera.setPreviewPhoto(new Photo(848384, "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/%D0%9B%D1%8C%D0%B2%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B8%D0%B9_%D0%BE%D0%BF%D0%B5%D1%80%D0%BD%D0%B8%D0%B9_%D1%82%D0%B5%D0%B0%D1%82%D1%80.jpg/1024px-%D0%9B%D1%8C%D0%B2%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B8%D0%B9_%D0%BE%D0%BF%D0%B5%D1%80%D0%BD%D0%B8%D0%B9_%D1%82%D0%B5%D0%B0%D1%82%D1%80.jpg"));
        opera.setTitle("Opera");
        opera.setSubTitle("Lvivska Opera");
        opera.setAudio(new Audio(83843838, "https://www.dropbox.com/s/l4oviitbfuwpleu/Opernyi-teatr.wav?dl=1"));
        opera.setDescription(loremIpsum);
        opera.setPhotos(photos);


        TourPoint virmenska = new TourPoint();
        virmenska.setLatitude(49.842758);
        virmenska.setLongitude(24.027508);
        virmenska.setPreviewPhoto(new Photo(2034894, "https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/%D0%92%D1%83%D0%BB%D0%B8%D1%86%D1%8F_%D0%92%D1%96%D1%80%D0%BC%D0%B5%D0%BD%D1%81%D1%8C%D0%BA%D0%B0_%D0%9B%D1%8C%D0%B2%D1%96%D0%B2.jpg/800px-%D0%92%D1%83%D0%BB%D0%B8%D1%86%D1%8F_%D0%92%D1%96%D1%80%D0%BC%D0%B5%D0%BD%D1%81%D1%8C%D0%BA%D0%B0_%D0%9B%D1%8C%D0%B2%D1%96%D0%B2.jpg"));
        virmenska.setTitle("Virmenska vulytsya");
        virmenska.setSubTitle("Street in the center of Lviv");
        virmenska.setAudio(new Audio(8383838, "https://www.dropbox.com/s/ovtowpco3vurfqa/Virmenska-str.wav?dl=1"));
        virmenska.setDescription(loremIpsum);
        virmenska.setPhotos(photos);


        TourPoint pototski = new TourPoint();
        pototski.setLatitude(49.837778);
        pototski.setLongitude(24.026944);
        pototski.setSubTitle("Great palace");
        pototski.setPreviewPhoto(new Photo(7479849, "https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/%D0%94%D0%B2%D0%BE%D1%80%D0%B5%D1%86_%D0%9F%D0%BE%D1%82%D0%BE%D1%86%D0%BA%D0%B8%D1%85.jpg/1024px-%D0%94%D0%B2%D0%BE%D1%80%D0%B5%D1%86_%D0%9F%D0%BE%D1%82%D0%BE%D1%86%D0%BA%D0%B8%D1%85.jpg"));
        pototski.setTitle("Palats Pototskyh");
        pototski.setAudio(new Audio(838383830, "https://www.dropbox.com/s/8cqnak6mhk6gpdh/Pototskyi-palace.wav?dl=1"));
        pototski.setDescription(loremIpsum);
        pototski.setPhotos(photos);


        tour.setPoints(Arrays.asList(ratusha, opera, virmenska, pototski));
        return tour;
    }


    @Override
    public void pause() {

    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }


    public void tourPointClick(TourPoint tourPoint) {
        tourView.showTourPointDetails(tour, tourPoint);
    }
}