package one.citytour.presenter;

import one.citytour.view.AuthView;
import one.citytour.view.NewPointView;
import one.citytour.view.NewTourView;
import one.citytour.view.PointDetailsView;
import one.citytour.view.TourListView;
import one.citytour.view.TourView;

/**
 * Created by vanya on 5/11/2016.
 */
public class PresenterHolder {
    private static TourListPresenter tourListPresenter = new TourListPresenter();
    private static PointDetailsPresenter pointDetailsPresenter = new PointDetailsPresenter();
    private static TourPresenter tourPresenter = new TourPresenter();
    private static AuthPresenter authPresenter = new AuthPresenter();
    private static NewPointPresenter newPointPresenter = new NewPointPresenter();
    private static NewTourPresenter newTourPresenter = new NewTourPresenter();

    public static TourListPresenter getTourListPresenter(TourListView view) {
        tourListPresenter.setView(view);
        return tourListPresenter;
    }

    public static PointDetailsPresenter getPointDetailsPresenter(PointDetailsView view) {
        pointDetailsPresenter.setView(view);
        return pointDetailsPresenter;
    }

    public static TourPresenter getTourPresenter(long tourId, TourView view) {
        tourPresenter.setTourId(tourId);
        tourPresenter.setTourView(view);
        return tourPresenter;
    }

    public static AuthPresenter getAuthPresenter(AuthView view) {
        authPresenter.setAuthView(view);
        return authPresenter;
    }

    public static NewPointPresenter getNewPointPresenter(NewPointView newPointView) {
        newPointPresenter.setNewPointView(newPointView);
        return newPointPresenter;
    }

    public static NewTourPresenter getNewTourPresenter(NewTourView newTourView) {
        newTourPresenter.setNewTourView(newTourView);
        return newTourPresenter;
    }
}
