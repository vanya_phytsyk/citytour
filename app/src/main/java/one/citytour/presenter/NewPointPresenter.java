package one.citytour.presenter;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import one.citytour.domain.model.Photo;
import one.citytour.domain.model.TourPoint;
import one.citytour.domain.repository.impl.Injector;
import one.citytour.view.NewPointView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by vanya on 7/5/2016.
 */
public class NewPointPresenter implements BasePresenter {

    private NewPointView newPointView;

    public void setNewPointView(NewPointView newtourView) {
        this.newPointView = newtourView;
    }

    private Subscription locationSubscription;

    private TourPoint tourPoint = new TourPoint();

    private volatile int pendingUploadCount;

    public void mapReady() {
        if (!pendingUpdateLocation) {
            newPointView.updateLocation();
        } else {
            pendingUpdateLocation = false;
            newPointView.updateLocation(new LatLng(tourPoint.getLatitude(), tourPoint.getLongitude()));
        }
    }

    private void updateCurrentLocation() {
        if (locationSubscription != null) {
            locationSubscription.unsubscribe();
        }
        locationSubscription = Injector.provideLocationRepository().getCurrentLocation()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(location -> {
                    newPointView.updateLocation(location);
                    locationSubscription.unsubscribe();
                });
    }

    public void currentLocationButtonClick() {
        updateCurrentLocation();
    }

    public void audioChosen(String filePath) {
        newPointView.showAudioProgress();
        uploadAudio(filePath);
    }

    private void uploadAudio(String audioPath) {
        pendingUploadCount++;
        Injector.provideFileUploadRepository().uploadAudio(audioPath)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(audio -> {
                    tourPoint.setAudio(audio);
                    newPointView.showAudioUploadSuccess();
                    pendingUploadCount--;
                }, throwable -> {
                    newPointView.hideAudioProgress();
                    newPointView.showError(throwable.getMessage());
                    pendingUploadCount--;
                    Timber.e(throwable, "");
                });
    }

    public void photoChosen(String photoPath) {
        Photo photo = new Photo(0, photoPath);
        tourPoint.addPhoto(photo);
        newPointView.showPhotoDownloading(photo);
        uploadPhoto(photo);
    }

    private void uploadPhoto(Photo photo) {
        pendingUploadCount++;
        Injector.provideFileUploadRepository().uploadPhoto(photo.getUrl())
                .subscribe(photoFromServer -> {
                    photo.setUrl(photoFromServer.getUrl());
                    photo.setId(photoFromServer.getId());
                    newPointView.showPhotoDownloaded(photo);
                    pendingUploadCount--;
                    Timber.d("Photo uploaded");
                }, throwable -> {
                    newPointView.showError(throwable.getMessage());
                    tourPoint.removePhoto(photo);
                    newPointView.removePhoto(photo);
                    pendingUploadCount--;
                    Timber.e(throwable, "");
                });
    }

    public void deletePhotoClicked(Photo photo) {
        tourPoint.removePhoto(photo);
        newPointView.removePhoto(photo);
    }

    private boolean pendingUpdateLocation;

    public void pointIsSet(TourPoint point) {
        this.tourPoint = point;
        newPointView.showCoverPhotoDownloaded(point.getPreviewPhoto());
        newPointView.showTitle(point.getTitle());
        newPointView.showDescription(point.getSubTitle());
        newPointView.showText(point.getDescription());

        newPointView.showPhotosDownloaded(point.getPhotos());
        pendingUpdateLocation = true;
    }

    public void coverPhotoChosen(String photoPath) {
        Photo photo = new Photo(0, photoPath);
        tourPoint.setPreviewPhoto(photo);
        newPointView.showCoverPhotoDownloading(photo);
        uploadCoverPhoto(photo);
    }

    private void uploadCoverPhoto(Photo photo) {
        pendingUploadCount++;
        Injector.provideFileUploadRepository().uploadPhoto(photo.getUrl())
                .subscribe(photoFromServer -> {
                    photo.setUrl(photoFromServer.getUrl());
                    photo.setId(photoFromServer.getId());
                    newPointView.showCoverPhotoDownloaded(photo);
                    pendingUploadCount--;
                    Timber.d("Photo uploaded");
                }, throwable -> {
                    newPointView.showError(throwable.getMessage());
                    tourPoint.setPreviewPhoto(null);
                    newPointView.removeCoverPhoto();
                    pendingUploadCount--;
                    Timber.e(throwable, "");
                });
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {
        if (locationSubscription != null) {
            locationSubscription.unsubscribe();
        }
    }

    public void createButtonClicked() {
        fillPointData();
        if (validatePoint()) {
            if (isNoPendingUpload()) {
                processCreatedPoint();
            } else {
                newPointView.showWaitUntilUpload();
            }
        }
    }

    private void processCreatedPoint() {
        newPointView.returnToNewTour(tourPoint);
        tourPoint = new TourPoint();
    }


    private boolean isNoPendingUpload() {
        return pendingUploadCount == 0;
    }

    private boolean validatePoint() {
        if (tourPoint.getAudio() == null) {
            newPointView.showUploadAudioAlert();
            return false;
        }
        if (tourPoint.getPreviewPhoto() == null) {
            newPointView.showUploadCoverPhotoAlert();
            return false;
        }
        if (tourPoint.getPhotos().isEmpty()) {
            newPointView.showUploadPhotoAlert();
            return false;
        }
        return true;
    }

    private void fillPointData() {
        String pointTitle = newPointView.getPointTitle();
        String pointDescription = newPointView.getPointDescription();
        String pointText = newPointView.getPointText();
        LatLng latLng = newPointView.getLocation();

        tourPoint.setTitle(pointTitle);
        tourPoint.setSubTitle(pointDescription);
        tourPoint.setDescription(pointText);
        tourPoint.setLatitude(latLng.latitude);
        tourPoint.setLongitude(latLng.longitude);
    }
}
