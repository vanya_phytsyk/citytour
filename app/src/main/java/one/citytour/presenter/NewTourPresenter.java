package one.citytour.presenter;

import one.citytour.domain.model.Photo;
import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;
import one.citytour.domain.repository.impl.Injector;
import one.citytour.view.NewTourView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by vanya on 7/16/2016.
 */
public class NewTourPresenter implements BasePresenter {
    private NewTourView newTourView;

    private Tour tour = new Tour();
    private boolean photoIsUploading;
    private int pointWaitingForUpdatingIndex = -1;
    private long tourId;


    public void setNewTourView(NewTourView newTourView) {
        this.newTourView = newTourView;
    }

    public NewTourPresenter() {

    }

    public void coverPhotoChosen(String photoPath) {
        Photo photo = new Photo(0, photoPath);
        tour.setCoverPhoto(photo);
        newTourView.showCoverPhotoDownloading(photo);
        uploadPhoto(photo);
    }

    public void pointCreated(TourPoint point) {
        newTourView.addPoint(point);
        tour.addPoint(point);
    }

    private void uploadPhoto(Photo photo) {
        photoIsUploading = true;
        Injector.provideFileUploadRepository()
                .uploadPhoto(photo.getUrl())
                .subscribe(photoFromServer -> {
                    photo.setUrl(photoFromServer.getUrl());
                    photo.setId(photoFromServer.getId());
                    newTourView.showCoverPhotoDownloaded(photo);
                    photoIsUploading = false;
                    Timber.d("Tour cover photo uploaded");
                }, throwable -> {
                    newTourView.showError(throwable.getMessage());
                    tour.setCoverPhoto(null);
                    newTourView.removeCoverPhoto();
                    photoIsUploading = false;
                    Timber.e(throwable, "");
                });
    }

    public void pointUpdated(TourPoint point) {
        if (pointWaitingForUpdatingIndex != -1) {
            tour.getPoints().set(pointWaitingForUpdatingIndex, point);
            newTourView.updatePoint(point, pointWaitingForUpdatingIndex);
            pointWaitingForUpdatingIndex = -1;
        }
    }

    public void removePointClicked(int tourIndex) {
        tour.getPoints().remove(tourIndex);
        newTourView.removePoint(tourIndex);
    }

    public void newPointButtonClicked() {
        newTourView.navigateToPointCreate();
    }

    public void pointClicked(TourPoint point, int pointIndex) {
        newTourView.navigateToPointUpdate(point);
        pointWaitingForUpdatingIndex = pointIndex;
    }

    public void createButtonClicked() {
        fillTour();
        if (validateTour()) {
            newTourView.showProgress();
            if (tour.getId() == 0) {
                createNewTour();
            } else {
                updateTour();
            }
        }
    }

    private void updateTour() {
        Injector.provideRemoteTourRepository().updateTour(tour)
                .flatMap(tour1 -> Injector.provideLocalTourRepository().cache(tour1))
                .subscribe(tour1 -> {
                    newTourView.exit();
                    tour = new Tour();
                    Timber.d("Tour update: %s", tour1.getTitle());
                }, throwable -> {
                    Timber.d("Tour update: %s fail. reason: %s", tour.getTitle(), throwable.getMessage());
                    newTourView.hideProgress();
                });
    }

    private void createNewTour() {
        Injector.provideRemoteTourRepository().createTour(tour)
                .flatMap(tour1 -> Injector.provideLocalTourRepository().cache(tour1))
                .subscribe(tour1 -> {
                    newTourView.exit();
                    tour = new Tour();
                    Timber.d("Tour created: %s", tour1.getTitle());
                }, throwable -> {
                    Timber.d("Tour creation: %s fail. reason: %s", tour.getTitle(), throwable.getMessage());
                    newTourView.hideProgress();
                });
    }

    private boolean validateTour() {
        if (tour.getCoverPhoto() == null) {
            newTourView.showNoPhotoAlert();
            return false;
        }

        if (isEmpty(tour.getTitle())) {
            newTourView.showNoTitleAlert();
            return false;
        }

        if (isEmpty(tour.getDescription())) {
            newTourView.showNoDescriptionAlert();
            return false;
        }

        if (!tour.hasPoints()) {
            newTourView.showNoPointsAlert();
            return false;
        }

        return true;
    }

    private boolean isEmpty(String string) {
        return string == null || string.isEmpty();
    }

    private void fillTour() {
        String tourTitle = newTourView.getTourTitle();
        String tourSubTitle = newTourView.getTourSubtitle();
        tour.setTitle(tourTitle);
        tour.setDescription(tourSubTitle);
        tour.setPrivate(newTourView.isPrivate());
    }

    @Override
    public void resume() {
        updateView();
    }

    private void updateView() {
        if (tour.getCoverPhoto() != null) {
            newTourView.showCoverPhotoDownloaded(tour.getCoverPhoto());
        }
        if (tour.getTitle() != null) {
            newTourView.showTitle(tour.getTitle());
        }
        if (tour.getDescription() != null) {
            newTourView.showDescription(tour.getDescription());
        }
        if (tour.hasPoints()) {
            newTourView.showPoints(tour.getPoints());
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public void initTourId(long tourId) {
        Injector.provideRemoteTourRepository()
                .getTour(tourId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tour -> {
                    this.tour = tour;
                    updateView();
                }, throwable -> {
                    Timber.e(throwable, "");
                });
    }
}
