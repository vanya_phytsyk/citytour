package one.citytour.ui.picasso;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by vanya on 17.04.16.
 */
public class PicassoMarkerIconDownloader implements Target {
    private Marker marker;
    private boolean isDisabled;

    public PicassoMarkerIconDownloader(Marker marker) {
        this.marker = marker;
    }

    @Override
    public int hashCode() {
        return marker.hashCode();
    }

    public void setDisabled(boolean disabled) {
        isDisabled = disabled;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof PicassoMarkerIconDownloader) {
            Marker marker = ((PicassoMarkerIconDownloader) o).marker;
            return this.marker.equals(marker);
        } else {
            return false;
        }
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        if (!isDisabled) {
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap));
        }
    }

    @Override
    public void onBitmapFailed(Drawable errorDrawable) {
    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {

    }
}
