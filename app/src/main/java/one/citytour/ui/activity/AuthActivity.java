package one.citytour.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKError;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import one.citytour.R;
import one.citytour.presenter.AuthPresenter;
import one.citytour.presenter.PresenterHolder;
import one.citytour.view.AuthView;
import timber.log.Timber;

/**
 * Created by vanya on 7/3/2016.
 */
public class AuthActivity extends AppCompatActivity implements AuthView, FacebookCallback<LoginResult>, VKCallback<VKAccessToken> {

    public static final String EXTRA_VK_AUTH = "vk_auth";
    public static final String EXTRA_FB_AUTH = "fb_auth";

    private AuthPresenter authPresenter;

    @BindView(R.id.login_button_fb)
    LoginButton loginButtonFb;

    @BindView(R.id.login_button_vk)
    Button loginButtonVk;

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authPresenter = PresenterHolder.getAuthPresenter(this);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);

        callbackManager = CallbackManager.Factory.create();
        loginButtonFb.setReadPermissions("email");
        loginButtonFb.registerCallback(callbackManager, this);

        if (getIntent() != null
                && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(EXTRA_VK_AUTH)) {
            loginVK();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode,
                resultCode, data);
        VKSdk.onActivityResult(requestCode, resultCode, data, this);

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onError(VKError error) {
        authPresenter.authError(error);
        Timber.d("Vk login error: %s", error.errorMessage);
    }

    @Override
    public void onResult(VKAccessToken res) {
        Timber.d("Vk login %s", res.email);
        res.save();
        authPresenter.authSuccessVK(res.accessToken);
    }

    @Override
    public void onError(FacebookException error) {
        authPresenter.authError(error);
        Timber.d("Facebook login error: %s", error.getMessage());
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        authPresenter.authSuccessFB(loginResult.getAccessToken().getToken());
        Timber.d("Facebook login %s", loginResult.toString());
    }

    @OnClick(R.id.login_button_vk)
    public void vkButtonClick() {
        loginVK();
    }

    private void loginVK() {
        VKSdk.login(this, VKScope.EMAIL, VKScope.PHOTOS, VKScope.WALL);
    }

    @Override
    public void onCancel() {
        Timber.d("Facebook login cancelled");
    }

    @Override
    public void navigateToTourList() {
        Intent intent = new Intent(this, TourListActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void authError() {

    }

    @BindView(R.id.progress)
    View progress;

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
        loginButtonFb.setEnabled(false);
        loginButtonVk.setEnabled(false);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
        loginButtonFb.setEnabled(true);
        loginButtonVk.setEnabled(true);
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void showNoConnectionView() {

    }

    @Override
    public void hideNoConnectionView() {

    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }
}
