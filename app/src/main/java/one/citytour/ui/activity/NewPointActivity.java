package one.citytour.ui.activity;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ZoomButton;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import one.citytour.R;
import one.citytour.domain.model.Photo;
import one.citytour.domain.model.TourPoint;
import one.citytour.presenter.NewPointPresenter;
import one.citytour.presenter.PresenterHolder;
import one.citytour.ui.dialog.RecordAudioDialog;
import one.citytour.ui.util.FileUtil;
import one.citytour.util.PermissionUtils;
import one.citytour.view.NewPointView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by vanya on 7/7/2016.
 */
public class NewPointActivity extends AppCompatActivity implements NewPointView, OnMapReadyCallback {

    private final static int REQUEST_CAMERA = 11;
    private static final int REQUEST_GALLERY = 22;
    private static final int REQUEST_AUDIO = 33;
    private static final int AUDIO_RECORD_PERMISSION_REQUEST_CODE = 34;
    private static final int PHOTO_STORAGE_PERMISSION_REQUEST_CODE = 35;
    private static final int AUDIO_STORAGE_PERMISSION_REQUEST_CODE = 36;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 37;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 38;

    private static final int PHOTO_CHOOSE_FOR_COVER = 40;
    private static final int PHOTO_CHOOSE_FOR_PHOTOS = 41;

    public static final String EXTRA_TOUR_POINT = "extra_tour_point";
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 44;

    private int pendingChoosePhotoDestination;

    @BindView(R.id.map)
    MapView mapView;

    @BindView(R.id.photos)
    RecyclerView photos;

    @BindView(R.id.add_audio_button)
    Button addAudioButton;

    @BindView(R.id.chooseLocationButton)
    ImageView changeLocationButton;

    private GoogleMap googleMap;
    private NewPointPresenter newPointPresenter;
    private PhotoAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newPointPresenter = PresenterHolder.getNewPointPresenter(this);
        setContentView(R.layout.activity_new_point);
        ButterKnife.bind(this);

        photos.setLayoutManager(new GridLayoutManager(this, 4));
        adapter = new PhotoAdapter();
        photos.setAdapter(adapter);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey(EXTRA_TOUR_POINT)) {
            TourPoint point = intent.getExtras().getParcelable(EXTRA_TOUR_POINT);
            newPointPresenter.pointIsSet(point);
        }
        changeLocationButton.setColorFilter(getResources().getColor(R.color.textColorSecondary), PorterDuff.Mode.MULTIPLY);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        newPointPresenter.mapReady();
    }

    @Override
    public void updateLocation(Location location) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 16));
    }

    @Override
    public void updateLocation(LatLng latLng) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
    }

    @OnClick(R.id.currentLocationButton)
    public void updateLocation() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            newPointPresenter.currentLocationButtonClick();
        } else {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, false);
        }
    }

    @OnClick(R.id.chooseLocationButton)
    public void showLocationChooser() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            Timber.e(e, "", "");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        newPointPresenter.resume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @OnClick(R.id.add_audio_button)
    void addAudioClicked() {
        showAudioChooseDialog();
    }

    private void showAudioRecordDialog() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag("dialog_record");

            if (prev != null) {
                fragmentTransaction.remove(prev);
            }
            fragmentTransaction.addToBackStack(null);
            RecordAudioDialog recordAudioDialog = new RecordAudioDialog();
            recordAudioDialog.setCallback(filePath -> {
                String mime = getMimeType(filePath);
                newPointPresenter.audioChosen(filePath.getPath());
            });
            recordAudioDialog.show(fragmentTransaction, "dialog_record");
        } else {
            PermissionUtils.requestPermission(this, AUDIO_RECORD_PERMISSION_REQUEST_CODE,
                    Manifest.permission.RECORD_AUDIO, false);
        }
    }

    public String getMimeType(Uri uri) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == AUDIO_RECORD_PERMISSION_REQUEST_CODE && PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.RECORD_AUDIO)) {
            // Enable the my location layer if the permission has been granted.
            showAudioRecordDialog();
        } else if (requestCode == PHOTO_STORAGE_PERMISSION_REQUEST_CODE && PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showGallery();
        } else if (requestCode == CAMERA_PERMISSION_REQUEST_CODE && PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.CAMERA)) {
            showCamera();
        } else if (requestCode == LOCATION_PERMISSION_REQUEST_CODE && PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            updateLocation();
        } else if (requestCode == AUDIO_STORAGE_PERMISSION_REQUEST_CODE && PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            chooseAudioFromDisk();
        }
    }

    @BindView(R.id.point_title_wrapper)
    TextInputLayout titleWrapper;

    @BindView(R.id.point_short_description_wrapper)
    TextInputLayout descriptionWrapper;

    @BindView(R.id.point_text_wrapper)
    TextInputLayout textWrapper;


    @OnClick(R.id.create_button)
    void createButtonClicked() {
        boolean isTextFieldsFilled = true;
        if (TextUtils.isEmpty(getPointTitle())) {
            showEditTextError(titleWrapper, getString(R.string.fiil_point_title));
            isTextFieldsFilled = false;
        }
        if (TextUtils.isEmpty(getPointDescription())) {
            showEditTextError(descriptionWrapper, getString(R.string.fill_point_description));
            isTextFieldsFilled = false;
        }
        if (TextUtils.isEmpty(getPointText())) {
            showEditTextError(textWrapper, getString(R.string.fill_point_text));
            isTextFieldsFilled = false;
        }
        if (isTextFieldsFilled) {
            newPointPresenter.createButtonClicked();
        }
    }

    private void showEditTextError(TextInputLayout editText, String errorText) {
        editText.setError(errorText);
    }

    @BindView(R.id.audio_progress)
    ProgressBar audioProgressBar;

    @Override
    public void showAudioProgress() {
        audioProgressBar.setVisibility(View.VISIBLE);
        addAudioButton.setEnabled(false);
        addAudioButton.setText(R.string.audio_uloading);
    }

    @Override
    public void hideAudioProgress() {
        audioProgressBar.setVisibility(View.GONE);
        addAudioButton.setEnabled(true);
        addAudioButton.setText(R.string.add_audio);
    }

    @Override
    public void showAudioUploadSuccess() {
        audioProgressBar.setVisibility(View.GONE);
        addAudioButton.setEnabled(true);
        addAudioButton.setText(R.string.change_audio);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {
        new AlertDialogWrapper.Builder(this)
                .setTitle(R.string.failure)
                .setMessage(message)
                .show();
    }

    @Override
    public void showUploadAudioAlert() {
        showError(getString(R.string.please_upload_audio));
    }

    @Override
    public void showUploadCoverPhotoAlert() {
        showError(getString(R.string.please_upload_cover_photo));
    }

    @Override
    public void showUploadPhotoAlert() {
        showError(getString(R.string.please_upload_photo));
    }

    @Override
    public void showWaitUntilUpload() {
        showError(getString(R.string.wait_until_files_uploaded));
    }

    @Override
    public void showNoConnectionView() {

    }

    @Override
    public void hideNoConnectionView() {

    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showText(String text) {
        pointText.setText(text);
    }

    @Override
    public void showTitle(String title) {
        pointTitle.setText(title);
    }

    @Override
    public void showDescription(String description) {
        shortDescription.setText(description);
    }

    @Override
    public void returnToNewTour(TourPoint point) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_TOUR_POINT, point);
        setResult(RESULT_OK, intent);
        finish();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_GALLERY && intent != null) {
                Uri selectedImage = intent.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                String picturePath = FileUtil.getPathFromPhotoUri(this, selectedImage, filePathColumn);
                if (pendingChoosePhotoDestination == PHOTO_CHOOSE_FOR_PHOTOS) {
                    newPointPresenter.photoChosen(picturePath);
                } else if (pendingChoosePhotoDestination == PHOTO_CHOOSE_FOR_COVER) {
                    newPointPresenter.coverPhotoChosen(picturePath);
                }
                pendingChoosePhotoDestination = 0;
            } else if (requestCode == REQUEST_CAMERA) {
                if (pendingChoosePhotoDestination == PHOTO_CHOOSE_FOR_PHOTOS) {
                    newPointPresenter.photoChosen(lastCapturedPhotoUri.getPath());
                } else if (pendingChoosePhotoDestination == PHOTO_CHOOSE_FOR_COVER) {
                    newPointPresenter.coverPhotoChosen(lastCapturedPhotoUri.getPath());
                }
                pendingChoosePhotoDestination = 0;
            } else if (requestCode == REQUEST_AUDIO && intent != null && intent.getData() != null) {
                Uri uri = intent.getData();
                FileUtil.saveFileToCache(this, uri).subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe(audioPath -> {
                    newPointPresenter.audioChosen(audioPath);
                });
            } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
                Place place = PlaceAutocomplete.getPlace(this, intent);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 16));
            }
        }
    }

    private void showPhotoChooseDialog() {
        new MaterialDialog.Builder(this)
                .title(R.string.choose_photo)
                .items(getString(R.string.camera), getString(R.string.gallery))
                .itemsCallback((dialog, itemView, which, text) -> {
                    switch (which) {
                        case 0:
                            showCamera();
                            break;
                        case 1:
                            showGallery();
                            break;
                    }
                }).show();
    }

    private void showGallery() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, REQUEST_GALLERY);
        } else {
            PermissionUtils.requestPermission(this, PHOTO_STORAGE_PERMISSION_REQUEST_CODE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, false);
        }
    }

    private void showCamera() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            initPhotoUri();
            takePicture.putExtra(MediaStore.EXTRA_OUTPUT, lastCapturedPhotoUri);
            startActivityForResult(takePicture, REQUEST_CAMERA);
        } else {
            PermissionUtils.requestPermission(this, CAMERA_PERMISSION_REQUEST_CODE,
                    Manifest.permission.CAMERA, false);
        }
    }

    private void showAudioChooseDialog() {
        new MaterialDialog.Builder(this)
                .title(R.string.choose_audio)
                .items(getString(R.string.record_audio), getString(R.string.from_file_system))
                .itemsCallback((dialog, itemView, which, text) -> {
                    switch (which) {
                        case 0:
                            showAudioRecordDialog();
                            break;
                        case 1:
                            chooseAudioFromDisk();
                            break;
                    }
                }).show();
    }

    private void chooseAudioFromDisk() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Intent intent;
            intent = new Intent();
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setType("audio/*");
            startActivityForResult(Intent.createChooser(intent, getString(R.string.choose_audio)), REQUEST_AUDIO);
        } else {
            PermissionUtils.requestPermission(this, AUDIO_STORAGE_PERMISSION_REQUEST_CODE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, false);
        }
    }

    private Uri lastCapturedPhotoUri;

    private void initPhotoUri() {
        lastCapturedPhotoUri = FileUtil.createPhotoUri();
    }

    @BindView(R.id.add_photo_button)
    Button addCoverPhotoButton;

    @BindView(R.id.cover_photo)
    ImageView coverPhoto;

    @OnClick(R.id.add_photo_button)
    void addPhotoClicked() {
        pendingChoosePhotoDestination = PHOTO_CHOOSE_FOR_COVER;
        showPhotoChooseDialog();
    }

    @OnClick(R.id.cover_photo)
    void photoCoverClicked() {
        pendingChoosePhotoDestination = PHOTO_CHOOSE_FOR_COVER;
        showPhotoChooseDialog();
    }

    @Override
    public void showPhotoDownloading(Photo photo) {
        adapter.addPhotoDownloading(photo);
    }

    @BindView(R.id.point_title)
    EditText pointTitle;

    @BindView(R.id.point_short_description)
    EditText shortDescription;

    @BindView(R.id.point_text)
    EditText pointText;

    @OnTextChanged(R.id.point_title)
    void pointTitleTextChanged() {
        pointTitle.setError(null);
    }

    @Override
    public String getPointTitle() {
        return pointTitle.getText().toString();
    }

    @Override
    public String getPointDescription() {
        return shortDescription.getText().toString();
    }

    @Override
    public String getPointText() {
        return pointText.getText().toString();
    }

    @Override
    public void showPhotoDownloaded(Photo photo) {
        adapter.notifyPhotoDownloaded(photo);
    }

    @Override
    public void showPhotosDownloaded(List<Photo> photos) {
        adapter.notifyPhotosDownloaded(photos);
    }

    @Override
    public LatLng getLocation() {
        return googleMap.getCameraPosition().target;
    }

    @BindView(R.id.progress)
    ProgressBar coverPhotoProgress;

    @Override
    public void showCoverPhotoDownloading(Photo photo) {
        addCoverPhotoButton.setVisibility(View.GONE);
        coverPhoto.setVisibility(View.VISIBLE);
        coverPhotoProgress.setVisibility(View.VISIBLE);
        Picasso.with(this).load(new File(photo.getScreenDependentUrl(this))).fit().centerCrop().into(coverPhoto);
    }

    @Override
    public void showCoverPhotoDownloaded(Photo photo) {
        coverPhotoProgress.setVisibility(View.GONE);
        Picasso.with(this).load(photo.getScreenDependentUrl(this)).fit().centerCrop().into(coverPhoto);
        addCoverPhotoButton.setVisibility(View.GONE);
    }

    @Override
    public void removePhoto(Photo photo) {
        adapter.removePhoto(photo);
    }

    @Override
    public void removeCoverPhoto() {
        coverPhoto.setVisibility(View.GONE);
        addCoverPhotoButton.setVisibility(View.VISIBLE);
    }

    private class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {
        private List<Photo> pointPhotos = new ArrayList<>();
        private List<Boolean> downloadedPhotos = new ArrayList<>();

        public void addPhotoDownloading(Photo photo) {
            downloadedPhotos.add(false);
            pointPhotos.add(photo);
            notifyItemInserted(getItemCount() - 2); //pre last item inserted
        }

        public void notifyPhotoDownloaded(Photo photo) {
            int index = pointPhotos.indexOf(photo);
            downloadedPhotos.set(index, true);
            notifyItemChanged(index);
        }

        public void notifyPhotosDownloaded(List<Photo> photos) {
            pointPhotos = new ArrayList<>(photos);
            for (Photo photo : photos) {
                downloadedPhotos.add(true);
            }
            notifyDataSetChanged();
        }

        public void removePhoto(Photo photo) {
            int index = pointPhotos.indexOf(photo);
            downloadedPhotos.remove(index);
            pointPhotos.remove(index);
            notifyItemRemoved(index);
        }

        private static final int ITEM_PHOTO = 0;
        private static final int ITEM_ADD_PHOTO = 1;

        class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView imageView;
            private View progress;
            private ImageButton deleteButton;

            public ViewHolder(View itemView) {
                super(itemView);
                this.imageView = (ImageView) itemView.findViewById(R.id.photo);
                this.progress = itemView.findViewById(R.id.progress);
                this.deleteButton = (ImageButton) itemView.findViewById(R.id.delete_button);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case ITEM_PHOTO:
                    View itemView = LayoutInflater.from(NewPointActivity.this).inflate(R.layout.grid_item_new_photo, null, false);
                    return new ViewHolder(itemView);
                case ITEM_ADD_PHOTO:
                default:
                    View addPhotoView = View.inflate(NewPointActivity.this, R.layout.grid_item_add_photo, null);
                    addPhotoView.setOnClickListener(view -> {
                        pendingChoosePhotoDestination = PHOTO_CHOOSE_FOR_PHOTOS;
                        showPhotoChooseDialog();
                    });
                    return new ViewHolder(addPhotoView);
            }
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (getItemViewType(position) == ITEM_PHOTO) {
                ImageView image = holder.imageView;
                Photo photo = pointPhotos.get(position);
                String photoPath = photo.getUrl();
                boolean isLoaded = downloadedPhotos.get(position);

                if (isLoaded) {
                    Picasso.with(NewPointActivity.this).load(photo.getScreenDependentUrl(NewPointActivity.this)).fit().centerCrop().into(image);
                } else {
                    Picasso.with(NewPointActivity.this).load(new File(photoPath)).fit().centerCrop().into(image);
                }

                holder.progress.setVisibility(isLoaded ? View.GONE : View.VISIBLE);

                holder.deleteButton.setOnClickListener(view -> newPointPresenter.deletePhotoClicked(photo));
            }
        }

        @Override
        public int getItemCount() {
            return pointPhotos.size() + 1;
        }

        @Override
        public int getItemViewType(int position) {
            return position == getItemCount() - 1 ? ITEM_ADD_PHOTO : ITEM_PHOTO;
        }
    }


}
