package one.citytour.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import one.citytour.R;
import one.citytour.domain.repository.impl.Injector;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by vanya on 6/4/2016.
 */
public class SettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.settings));
        }
        if (!Injector.provideAuthLocalRepository().hasAccessToken()) {
            loginLogoutButton.setText(R.string.login);
        }
    }

    @BindView(R.id.logout_login_button)
    TextView loginLogoutButton;

    @OnClick(R.id.setting_logout_login)
    void logoutLogin() {
        if (Injector.provideAuthLocalRepository().hasAccessToken()) {
            logout();
        } else {
            navigateToAuth();
        }
    }

    private void logout() {
        Injector.provideAuthLocalRepository().clearSession();
        Injector.provideLocalProfileRepository().clearCurrentProfile().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(o -> {
            finish();
        });
    }

    private void navigateToAuth() {
        Intent intent = new Intent(this, AuthActivity.class);
        startActivity(intent);
        finish();
    }
}
