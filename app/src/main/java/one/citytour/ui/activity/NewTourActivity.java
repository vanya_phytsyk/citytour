package one.citytour.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import one.citytour.R;
import one.citytour.domain.model.Photo;
import one.citytour.domain.model.TourPoint;
import one.citytour.presenter.NewTourPresenter;
import one.citytour.presenter.PresenterHolder;
import one.citytour.ui.util.FileUtil;
import one.citytour.util.PermissionUtils;
import one.citytour.view.NewTourView;

/**
 * Created by vanya on 7/16/2016.
 */
public class NewTourActivity extends AppCompatActivity implements NewTourView {

    private static final int PHOTO_STORAGE_PERMISSION_REQUEST_CODE = 35;
    private final static int REQUEST_CAMERA = 11;
    private static final int REQUEST_GALLERY = 22;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 37;

    private static final int REQUEST_NEW_POINT = 235;
    private static final int REQUEST_UPDATE_POINT = 236;
    public static final String EXTRA_TOUR = "extra_tour";
    public static final String EXTRA_TOUR_ID = "extra_tour_ID";

    private NewTourPresenter newTourPresenter;
    private PointAdapter adapter;

    @BindView(R.id.point_list)
    RecyclerView pointList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newTourPresenter = PresenterHolder.getNewTourPresenter(this);
        if (savedInstanceState == null) {
            initTourFromIntent();
        }
        setContentView(R.layout.activity_new_tour);
        ButterKnife.bind(this);
        adapter = new PointAdapter();
        pointList.setLayoutManager(new GridLayoutManager(this, getResources().getInteger(R.integer.new_tour_point_grid_column_count)));
        pointList.setAdapter(adapter);
    }

    private void initTourFromIntent() {
        if (getIntent() != null) {
            Intent intent = getIntent();
            if (intent.hasExtra(EXTRA_TOUR_ID)) {
                long tourId = intent.getLongExtra(EXTRA_TOUR_ID, -1);
                newTourPresenter.initTourId(tourId);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        newTourPresenter.resume();
    }

    @Override
    public void showTitle(String title) {
        tourTitle.setText(title);
    }

    @Override
    public void showDescription(String description) {
        tourSubtitle.setText(description);
    }

    @Override
    public void showPoints(List<TourPoint> points) {
        adapter.setPoints(points);
    }

    @OnClick(R.id.add_photo_button)
    void addPhotoButtonClicked() {
        showPhotoChooseDialog();
    }

    private void showPhotoChooseDialog() {
        new MaterialDialog.Builder(this)
                .title(R.string.choose_photo)
                .items(getString(R.string.camera), getString(R.string.gallery))
                .itemsCallback((dialog, itemView, which, text) -> {
                    switch (which) {
                        case 0:
                            showCamera();
                            break;
                        case 1:
                            showGallery();
                            break;
                    }
                }).show();
    }

    private void showGallery() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, REQUEST_GALLERY);
        } else {
            PermissionUtils.requestPermission(this, PHOTO_STORAGE_PERMISSION_REQUEST_CODE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, false);
        }
    }

    private void showCamera() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            initPhotoPath();
            takePicture.putExtra(MediaStore.EXTRA_OUTPUT, lastCapturedPhotoUri);
            startActivityForResult(takePicture, REQUEST_CAMERA);
        } else {
            PermissionUtils.requestPermission(this, CAMERA_PERMISSION_REQUEST_CODE,
                    Manifest.permission.CAMERA, false);
        }
    }

    private Uri lastCapturedPhotoUri;

    public void initPhotoPath() {
        lastCapturedPhotoUri = FileUtil.createPhotoUri();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PHOTO_STORAGE_PERMISSION_REQUEST_CODE && PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showGallery();
        } else if (requestCode == CAMERA_PERMISSION_REQUEST_CODE && PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.CAMERA)) {
            showCamera();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_GALLERY && intent != null) {
                Uri selectedImage = intent.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                String picturePath = FileUtil.getPathFromPhotoUri(this, selectedImage, filePathColumn);
                newTourPresenter.coverPhotoChosen(picturePath);
            } else if (requestCode == REQUEST_CAMERA) {
                newTourPresenter.coverPhotoChosen(lastCapturedPhotoUri.getPath());
            } else if (requestCode == REQUEST_NEW_POINT) {
                TourPoint point = intent.getParcelableExtra(NewPointActivity.EXTRA_TOUR_POINT);
                newTourPresenter.pointCreated(point);
            } else if (requestCode == REQUEST_UPDATE_POINT) {
                TourPoint point = intent.getParcelableExtra(NewPointActivity.EXTRA_TOUR_POINT);
                newTourPresenter.pointUpdated(point);
            }
        }
    }

    @BindView(R.id.progress)
    ProgressBar coverPhotoProgressBar;

    @BindView(R.id.cover_photo)
    ImageView coverPhoto;

    @BindView(R.id.no_photo_text)
    TextView noCoverPhotoText;

    @Override
    public void showCoverPhotoDownloading(Photo photo) {
        coverPhotoProgressBar.setVisibility(View.VISIBLE);
        noCoverPhotoText.setVisibility(View.GONE);
        Picasso.with(this).load(new File(photo.getUrl())).fit().centerCrop().into(coverPhoto);
    }

    @Override
    public void showCoverPhotoDownloaded(Photo photo) {
        coverPhotoProgressBar.setVisibility(View.GONE);
        Picasso.with(this).load(photo.getScreenDependentUrl(this)).fit().centerCrop().into(coverPhoto);
    }

    @OnClick(R.id.new_point_button)
    void newPointButtonClicked() {
        newTourPresenter.newPointButtonClicked();
    }

    @Override
    public void navigateToPointCreate() {
        Intent intent = new Intent(this, NewPointActivity.class);
        startActivityForResult(intent, REQUEST_NEW_POINT);
    }

    @Override
    public void navigateToPointUpdate(TourPoint point) {
        Intent intent = new Intent(this, NewPointActivity.class);
        intent.putExtra(NewPointActivity.EXTRA_TOUR_POINT, point);
        startActivityForResult(intent, REQUEST_UPDATE_POINT);
    }

    @Override
    public void addPoint(TourPoint tourPoint) {
        adapter.addPoint(tourPoint);
    }

    @Override
    public void updatePoint(TourPoint point, int pointWaitingForUpdatinIndex) {
        adapter.updatePoint(point, pointWaitingForUpdatinIndex);
    }

    @Override
    public void removePoint(int pointIndex) {
        adapter.removePoint(pointIndex);
    }

    @OnClick(R.id.create_button)
    void createButtonClicked() {
        newTourPresenter.createButtonClicked();
    }

    @BindView(R.id.tour_title)
    EditText tourTitle;

    @BindView(R.id.tour_description)
    EditText tourSubtitle;

    @Override
    public String getTourTitle() {
        return tourTitle.getText().toString();
    }

    @Override
    public String getTourSubtitle() {
        return tourSubtitle.getText().toString();
    }

    @BindView(R.id.public_private_switcher)
    Switch publicPrivateSwitcher;

    @Override
    public boolean isPrivate() {
        return publicPrivateSwitcher.isChecked();
    }

    @Override
    public void showNoPhotoAlert() {
        showError(getString(R.string.select_cover_photo_for_tour));
    }

    @BindView(R.id.tour_title_wrapper)
    TextInputLayout tourTitleWrapper;

    @BindView(R.id.tour_description_wrapper)
    TextInputLayout tourSubtitleWrapper;

    @OnTextChanged(R.id.tour_title)
    void pointTitleTextChanged() {
        tourTitleWrapper.setError(null);
    }

    @OnTextChanged(R.id.tour_description)
    void pointSubTitleTextChanged() {
        tourSubtitleWrapper.setError(null);
    }

    @Override
    public void showNoTitleAlert() {
        tourTitleWrapper.setError(getString(R.string.add_tour_title));
    }

    @Override
    public void showNoDescriptionAlert() {
        tourSubtitleWrapper.setError(getString(R.string.fill_tour_subtitle));
    }

    @Override
    public void showNoPointsAlert() {
        showError(getString(R.string.create_one_point));
    }

    @Override
    public void exit() {
        finish();
    }

    @Override
    public void removeCoverPhoto() {

    }

    @Override
    public void hideEmptyView() {

    }

    @BindView(R.id.progress_overlay)
    View progressOverlay;

    @BindView(R.id.root)
    View rootView;

    @Override
    public void showProgress() {
        progressOverlay.setVisibility(View.VISIBLE);
        rootView.setClickable(false);
    }

    @Override
    public void hideProgress() {
        progressOverlay.setVisibility(View.GONE);
        rootView.setClickable(true);
    }

    @Override
    public void showError(String message) {
        new AlertDialogWrapper.Builder(this)
                .setTitle(R.string.failure)
                .setMessage(message)
                .show();
    }

    @Override
    public void showNoConnectionView() {

    }

    @Override
    public void hideNoConnectionView() {

    }

    @Override
    public void showEmptyView() {

    }

    private class PointViewHolder extends RecyclerView.ViewHolder {
        private final ImageView deleteButton;
        private TextView pointTitle;
        private TextView pointSubtitle;
        private ImageView pointCoverPhoto;

        public PointViewHolder(View itemView) {
            super(itemView);
            pointTitle = (TextView) itemView.findViewById(R.id.point_title);
            pointSubtitle = (TextView) itemView.findViewById(R.id.point_short_description);
            pointCoverPhoto = (ImageView) itemView.findViewById(R.id.cover_photo);
            deleteButton = (ImageView) itemView.findViewById(R.id.delete_button);
        }
    }

    private class PointAdapter extends RecyclerView.Adapter<PointViewHolder> {

        private List<TourPoint> points = new ArrayList<>();

        private void addPoint(TourPoint tourPoint) {
            points.add(tourPoint);
            notifyItemInserted(getItemCount() - 1);
        }

        private void setPoints(List<TourPoint> points) {
            this.points = new ArrayList<>(points);
            notifyDataSetChanged();
        }

        public void updatePoint(TourPoint point, int pointWaitingForUpdatinIndex) {
            points.set(pointWaitingForUpdatinIndex, point);
            notifyItemChanged(pointWaitingForUpdatinIndex);
        }

        private void removePoint(int pointIndex) {
            points.remove(pointIndex);
            notifyItemRemoved(pointIndex);
            notifyItemRangeChanged(0, getItemCount());
        }

        @Override
        public PointViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View pointView = LayoutInflater.from(NewTourActivity.this).inflate(R.layout.grid_item_point, parent, false);
            return new PointViewHolder(pointView);
        }

        @Override
        public void onBindViewHolder(PointViewHolder holder, int position) {
            TourPoint point = points.get(position);
            holder.pointTitle.setText(point.getTitle());
            holder.pointSubtitle.setText(point.getSubTitle());
            Picasso.with(NewTourActivity.this).load(point.getPreviewPhoto().getScreenDependentUrl(NewTourActivity.this)).fit().centerCrop().into(holder.pointCoverPhoto);
            holder.itemView.setOnClickListener(view -> {
                newTourPresenter.pointClicked(point, position);
            });
            holder.deleteButton.setOnClickListener(view -> {
                newTourPresenter.removePointClicked(position);
            });
        }

        @Override
        public int getItemCount() {
            return points.size();
        }
    }
}
