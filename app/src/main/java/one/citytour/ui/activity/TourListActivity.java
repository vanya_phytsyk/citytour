package one.citytour.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import one.citytour.R;
import one.citytour.domain.model.Tour;
import one.citytour.presenter.PresenterHolder;
import one.citytour.presenter.TourListPresenter;
import one.citytour.sharing.VKSharer;
import one.citytour.ui.adapter.TourListAdapter;
import one.citytour.view.TourListView;
import rx.subjects.PublishSubject;
import timber.log.Timber;

/**
 * Created by vanya on 5/9/2016.
 */
public class TourListActivity extends Activity implements TourListView {
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 342654532;
    private TourListPresenter tourListPresenter;

    @BindView(R.id.location)
    TextView locationName;

    @BindView(R.id.find_location)
    ImageView findLocation;

    @BindView(R.id.list)
    RecyclerView recyclerView;

    @Nullable
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.navigation_view)
    NavigationView navigationView;

    @BindView(R.id.newTourButton)
    FloatingActionButton newTourButton;

    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    ImageView sideMenuUserAvatar;

    TextView sideMenuUserName;

    private TourListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_list);
        ButterKnife.bind(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new TourListAdapter(this);
        adapter.setOnItemClickListener(tour -> tourListPresenter.tourClick(tour));

        adapter.setOnMenuButtonClickListener(tour -> {
            tourListPresenter.tourMenuButtonClicked(tour);
        });

        recyclerView.setAdapter(adapter);
        tourListPresenter = PresenterHolder.getTourListPresenter(this);
        initNavigationMenu();

        swipeRefreshLayout.setOnRefreshListener(() -> {
            tourListPresenter.refreshTourListClicked();
        });
    }

    @Override
    public void showTourMenu(Tour tour, List<TourMenuItem> tourMenuItems) {
        List<String> itemStrings = new ArrayList<>(tourMenuItems.size());
        for (TourMenuItem item : tourMenuItems) {
            itemStrings.add(getString(item.getStringResId()));
        }
        new MaterialDialog.Builder(this)
                .items(itemStrings)
                .itemsColor(getResources().getColor(R.color.textColorPrimary))
                .itemsCallback((dialog, itemView, which, text) -> {
                    switch (tourMenuItems.get(which)) {
                        case SHARE:
                            tourListPresenter.tourShareButtonClick(tour);
                            break;
                        case SAVE:
                            tourListPresenter.tourSaveButtonClick(tour);
                            break;
                        case REMOVE:
                            tourListPresenter.removeTourButtonClick(tour);
                            break;
                        case DELETE_FROM_SAVED:
                            tourListPresenter.removeTourFromSavedButtonClick(tour);
                            break;
                        case UPDATE:
                            tourListPresenter.updateTourButtonClick(tour);
                            break;
                        default:
                            dialog.dismiss();
                            break;
                    }
                }).show();
    }

    @Override
    public void showShareDialog(Tour tour) {
        int[] resIds = {R.string.vk, R.string.facebook};
        new MaterialDialog.Builder(this)
                .items(getString(resIds[0]), getString(resIds[1]))
                .itemsColor(getResources().getColor(R.color.textColorPrimary))
                .itemsCallback((dialog, itemView, which, text) -> {
                    if (resIds[which] == R.string.vk) {
                        tourListPresenter.vkShareClicked(tour);
                    } else if (resIds[which] == R.string.facebook) {
                        tourListPresenter.fbShareClicked(tour);
                    }
                }).show();
    }

    @Override
    public void showVKShareDialog(Tour tour, PublishSubject<Object> resultSubject) {
        VKSharer sharer = new VKSharer(this);
        sharer.setShareResultSubject(resultSubject);
        sharer.shareToWall(tour);
    }

    @Override
    public void navigateToTourUpdate(long tourId) {
        Intent intent = new Intent(this, NewTourActivity.class);
        intent.putExtra(NewTourActivity.EXTRA_TOUR_ID, tourId);
        startActivity(intent);
    }

    private void initNavigationMenu() {

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                super.onDrawerSlide(drawerView, 0);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, 0);
            }
        };

        if (drawerLayout != null) {
            drawerLayout.addDrawerListener(actionBarDrawerToggle);
        }

        navigationView.setNavigationItemSelectedListener(menuItem -> {

            menuItem.setChecked(menuItem.isChecked());
            if (drawerLayout != null) {
                drawerLayout.closeDrawers();
            }

            switch (menuItem.getItemId()) {
                case R.id.search:
                    tourListPresenter.searchMenuItemClick();
                    return true;
                case R.id.saved:
                    tourListPresenter.savedToursMenuItemClick();
                    return true;
                case R.id.own:
                    tourListPresenter.ownToursMenuItemClick();
                    return true;
                case R.id.settings:
                    tourListPresenter.settingsMenuItemClick();
                    return true;
            }

            return false;
        });

        navigationView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                view.removeOnLayoutChangeListener(this);
                View.OnClickListener onClickListener = view1 -> tourListPresenter.userCredentialsClick();
                sideMenuUserAvatar = (ImageView) navigationView.findViewById(R.id.user_avatar);
                sideMenuUserAvatar.setOnClickListener(onClickListener);
                sideMenuUserName = (TextView) navigationView.findViewById(R.id.user_name);
                sideMenuUserName.setOnClickListener(onClickListener);
            }
        });

        if (drawerLayout != null) {
            actionBarDrawerToggle.syncState();
        }
    }

    @Override
    public void navigateToSettings() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public void showTourSavedSuccess() {
        Toast.makeText(this, "Tour saved", Toast.LENGTH_LONG).show();
    }

    @Override
    public void hideTourItem(Tour tour) {
        adapter.animateTourHiding(tour);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tourListPresenter.resume();
    }

    @OnClick(R.id.location)
    void locationTextClick() {
        tourListPresenter.placeSelectionClick();
    }

    @OnClick(R.id.find_location)
    void locationIconClick() {
        tourListPresenter.placeSelectionClick();
    }

    @Override
    public void updatePlaceName(String name) {
        locationName.setText(name);
    }

    @Override
    public void updateProfileAvatar(String avatarUrl) {
        navigationView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                view.removeOnLayoutChangeListener(this);
                Picasso.with(TourListActivity.this).load(avatarUrl).fit().centerCrop().into(sideMenuUserAvatar);
            }
        });
    }

    @Override
    public void updateProfileName(String userName) {
        navigationView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                view.removeOnLayoutChangeListener(this);
                sideMenuUserName.setText(userName);
            }
        });
    }

    @Override
    public void clearProfileInfo() {
        if (sideMenuUserAvatar != null && sideMenuUserName != null) {
            sideMenuUserName.setText(R.string.drawer__sign_in);
            sideMenuUserAvatar.setImageResource(R.mipmap.ic_profile);
        }
    }

    @Override
    public void showNewTourButton() {
        newTourButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateTour(Tour tour) {
        adapter.updateTour(tour);
    }

    @Override
    public void hideNewTourButton() {
        newTourButton.setVisibility(View.GONE);
    }

    @OnClick(R.id.newTourButton)
    void newTourButtonClicked() {
        tourListPresenter.newTourButtonClicked();
    }

    @Override
    public void navigateToNewTour() {
        Intent intent = new Intent(this, NewTourActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawers();
        } else {
            tourListPresenter.backPressed();
        }
    }

    @Override
    public void selectMenuItem(MenuItem item) {
        if (navigationView != null) {
            switch (item) {
                case CACHED:
                default:
                    navigationView.getMenu().getItem(0).setChecked(true);
                    break;

            }
        }
    }

    @Override
    public void exit() {
        super.onBackPressed();
    }

    @Override
    public void showPlaceName() {
        locationName.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePlaceName() {
        locationName.setVisibility(View.GONE);
    }

    @Override
    public void showPlaceSelectView() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            Toast.makeText(this, R.string.you_have_no_google_play, Toast.LENGTH_LONG).show();
            Timber.e(e, "", "");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                tourListPresenter.placeSelected(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.e(TourListActivity.class.getSimpleName(), status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void showTourList(List<Tour> tours) {
        hideEmptyView();
        adapter.setTours(tours);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        if (recyclerView != null) {
            recyclerView.setItemAnimator(null);
            recyclerView.setAdapter(null);
            recyclerView = null;
        }
        adapter = null;
        super.onDestroy();
    }

    @Override
    public void navigateToTourView(Tour tour) {
        Intent intent = new Intent(this, TourActivity.class);
        intent.putExtra(TourActivity.EXTRA_TOUR_ID, tour.getId());
        startActivity(intent);
    }

    @Override
    public void navigateToAuthorize() {
        Intent intent = new Intent(this, AuthActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToVKAuthorize() {
        Intent intent = new Intent(this, AuthActivity.class);
        intent.putExtra(AuthActivity.EXTRA_VK_AUTH, true);
        startActivity(intent);
    }

    @Override
    public void showProgress() {
        if (swipeRefreshLayout != null && !swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.post(() -> {
                swipeRefreshLayout.setRefreshing(true);
            });
        }
    }

    @Override
    public void hideProgress() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void showError(String message) {
        new AlertDialogWrapper.Builder(this)
                .setTitle(R.string.failure)
                .setMessage(message)
                .setNegativeButton(R.string.OK, (dialog, which) -> {
                    dialog.dismiss();
                }).show();
    }

    @BindView(R.id.empty_view)
    View emptyView;

    @Override
    public void showEmptyView() {
        if (emptyView != null && recyclerView != null) {
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void hideEmptyView() {
        if (recyclerView != null) {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void showNoConnectionView() {
        Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideNoConnectionView() {

    }
}
