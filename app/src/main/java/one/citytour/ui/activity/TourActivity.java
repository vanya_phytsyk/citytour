package one.citytour.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import one.citytour.R;
import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;
import one.citytour.presenter.PresenterHolder;
import one.citytour.presenter.TourPresenter;
import one.citytour.ui.fragment.TourPointDetailsFragment;
import one.citytour.ui.picasso.CircleTransform;
import one.citytour.ui.picasso.PicassoMarkerIconDownloader;
import one.citytour.ui.widget.RouteTileProvider;
import one.citytour.util.PermissionUtils;
import one.citytour.view.TourView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindColor;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TourActivity extends AppCompatActivity implements OnMapReadyCallback, TourView,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    public static final String EXTRA_TOUR_ID = "extra_tour";
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    List<PicassoMarkerIconDownloader> markerIconDownloaders = new ArrayList<>();
    Map<Marker, TourPoint> markerTourPointMap = new HashMap<>();

    private GoogleMap map;
    private TourPresenter tourPresenter;

    private TourPointDetailsFragment pointDetailsFragment;

    @BindDimen(R.dimen.map_marker_icon_size)
    int markerSize;

    @BindColor(R.color.map_route_stroke)
    int routeColor;

    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingUpPanelLayout;
    private boolean mPermissionDenied;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour);
        ButterKnife.bind(this);
        pointDetailsFragment = (TourPointDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.pointDetails);
        tourPresenter = PresenterHolder.getTourPresenter(getTourFromIntent(), this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private long getTourFromIntent() {
        Intent intent = getIntent();
        return intent.getLongExtra(EXTRA_TOUR_ID, -1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tourPresenter.resume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        markerIconDownloaders.clear();
        markerTourPointMap.clear();
        if (map != null) {
            map.clear();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        map = googleMap;
        map.setOnMarkerClickListener(this);
        map.setOnMapClickListener(this);
        map.setPadding(0, getResources().getDimensionPixelOffset(R.dimen.map_padding), 0, 0);
        tourPresenter.mapReady();

        enableMyLocation();
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, false);
        } else if (map != null) {
            // Access to the location has been granted to the app.
            map.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
    }

    @Override
    public void onBackPressed() {
        if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void showTour(Tour tour) {
        showTourMarkers(tour);
    }

    private void showTourMarkers(Tour tour) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (TourPoint point : tour.getPoints()) {
            LatLng latLng = new LatLng(point.getLatitude(), point.getLongitude());
            Marker marker = map.addMarker(new MarkerOptions().position(latLng).title(point.getTitle()));
            markerTourPointMap.put(marker, point);
            PicassoMarkerIconDownloader downloader = new PicassoMarkerIconDownloader(marker);
            markerIconDownloaders.add(downloader);
            Picasso.with(TourActivity.this).load(point.getPreviewPhoto().getResizedtUrl(markerSize)).resize(markerSize, markerSize)
                    .transform(new CircleTransform()).into(downloader);
            builder.include(latLng);
        }
        final LatLngBounds bounds = builder.build();
        int padding = 0; // offset from edges of the map in pixels
        final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                map.animateCamera(cameraUpdate);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        for (PicassoMarkerIconDownloader downloader: markerIconDownloaders) {
            downloader.setDisabled(true);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        TourPoint point = markerTourPointMap.get(marker);
        tourPresenter.tourPointClick(point);
        return false;
    }

    @Override
    public void showTourRoute(List<LatLng> latLngs) {
        PolylineOptions line = new PolylineOptions();
        int lineWidth = getResources().getDimensionPixelOffset(R.dimen.map_route_width);
        line.width(lineWidth).color(routeColor);
        LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
        for (int i = 0; i < latLngs.size(); i++) {
            line.add(latLngs.get(i));
            latLngBuilder.include(latLngs.get(i));
        }

      //  map.addPolyline(line);
        map.addTileOverlay(new TileOverlayOptions().tileProvider(new RouteTileProvider(this, latLngs)));
    }

    @Override
    public void showTourPointDetails(Tour tour, TourPoint tourPoint) {
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        pointDetailsFragment.updatePointDetails(tourPoint);
        pointDetailsFragment.setTour(tour);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(false).show(getSupportFragmentManager(), "dialog");
    }


}


