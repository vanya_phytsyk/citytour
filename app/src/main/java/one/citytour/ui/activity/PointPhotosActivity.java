package one.citytour.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import one.citytour.R;
import one.citytour.domain.model.TourPoint;
import one.citytour.domain.repository.impl.Injector;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by ivan on 24.04.16.
 */
public class PointPhotosActivity extends Activity {

    public static final String EXTRA_POINT_ID = "extra_point_id";

    private long pointId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_pager);

        if (savedInstanceState != null && savedInstanceState.containsKey(EXTRA_POINT_ID)) {
            pointId = savedInstanceState.getLong(EXTRA_POINT_ID);
        } else if (getIntent() != null) {
            pointId = getIntent().getLongExtra(EXTRA_POINT_ID, -1);
        } else {
            finish();
            return;
        }
        ViewPager photosPager = (ViewPager) findViewById(R.id.photo_pager);
        PhotoAdapter adapter = new PhotoAdapter();
        photosPager.setAdapter(adapter);

        Injector.provideLocalTourRepository().getCachedPoint(pointId).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread()).subscribe(point -> {
            adapter.setTourPoint(point);
            adapter.notifyDataSetChanged();
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (pointId != 0) {
            outState.putLong(EXTRA_POINT_ID, pointId);
        }
        super.onSaveInstanceState(outState);
    }

    private class PhotoAdapter extends PagerAdapter {

        private TourPoint tourPoint;

        public void setTourPoint(TourPoint tourPoint) {
            this.tourPoint = tourPoint;
        }

        @Override
        public int getCount() {
            return tourPoint != null && tourPoint.getPhotos() != null ? tourPoint.getPhotos().size() : 0;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            PhotoView photoView = new PhotoView(container.getContext());

            final PhotoViewAttacher attacher = new PhotoViewAttacher(photoView);
            Picasso.with(PointPhotosActivity.this)
                    .load(tourPoint.getPhotos().get(position).getScreenDependentUrl(PointPhotosActivity.this))
                    .into(photoView, new Callback() {
                        @Override
                        public void onSuccess() {
                            attacher.update();
                        }

                        @Override
                        public void onError() {
                        }
                    });
            container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            return photoView;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
