package one.citytour.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import one.citytour.R;
import one.citytour.domain.model.Photo;
import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;
import one.citytour.presenter.PointDetailsPresenter;
import one.citytour.presenter.PresenterHolder;
import one.citytour.ui.activity.PointPhotosActivity;
import one.citytour.ui.picasso.CircleTransform;
import one.citytour.ui.widget.playbutton.AudioPlayerWidget;
import one.citytour.view.PointDetailsView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subjects.PublishSubject;

/**
 * Created by ivan on 21.04.16.
 */
public class TourPointDetailsFragment extends Fragment implements PointDetailsView {

    @BindView(R.id.point_photo_full_size)
    public ImageView photoFullSize;

    @BindView(R.id.point_preview)
    public ImageView preview;

    @BindView(R.id.point_title)
    public TextView pointTitle;

    @BindView(R.id.point_short_description)
    public TextView pointSubtitle;

    @BindView(R.id.point_text)
    public TextView pointText;

    @BindView(R.id.rating)
    public RatingBar ratingBar;

    private PointDetailsPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = PresenterHolder.getPointDetailsPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_point_details, container);
    }

    @BindView(R.id.ratingContainer)
    public View ratingContainer;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        ratingContainer.setOnClickListener(view1 -> {
            presenter.ratingClick();
        });
        setRatingBarColor(ratingBar);

        audioPlayerWidget.setPlayButtonClickListener(() -> presenter.playAudioClick());
    }

    private void setRatingBarColor(RatingBar ratingBar) {
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public void showUpdateRatingDialog(float rating) {
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.layout_dialog_rate_point, null, false);
        RatingBar ratingBar = (RatingBar) dialogView.findViewById(R.id.rating);
        setRatingBarColor(ratingBar);
        ratingBar.setRating(rating);
        new MaterialDialog.Builder(getActivity())
                .customView(dialogView, false)
                .positiveText(R.string.rate)
                .title(R.string.rate_this_point)
                .onPositive((dialog, which) -> {
                    float newRating = ratingBar.getRating();
                    presenter.ratingChosen((int) newRating);
                    dialog.dismiss();
                })
                .show();
    }

    @Override
    public void updateRatingView(int pointRating) {
        showRating(pointRating);
    }

    @BindView(R.id.audio_player)
    AudioPlayerWidget audioPlayerWidget;

    @Override
    public void updateCurrentPlayingPosition(int position, int maxDuration) {
        if (!audioPlayerWidget.isTouching()) {
            audioPlayerWidget.updateDuration(position, maxDuration);
        }
    }

    @Override
    public void setAudioChangeProgressSubject(PublishSubject<Integer> audioProgressChangeSubject) {
        audioPlayerWidget.setChangeAudioPositionSubject(audioProgressChangeSubject);
    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.resume();
    }

    @OnClick(R.id.point_photo_full_size)
    public void photoClick() {
        presenter.photoClick();
    }

    @Override
    public void showPlaying(boolean isPlaying) {
        if (isPlaying) {
            audioPlayerWidget.showPlaying();
        } else {
            audioPlayerWidget.showPausing();
        }
    }

    @Override
    public void hidePlayProgress() {
        audioPlayerWidget.hideProgress();
    }

    @Override
    public void showPlayProgress() {
        audioPlayerWidget.showProgress();
    }

    @Override
    public void showPointDetails(TourPoint tourPoint) {
        showPhotoPreview(tourPoint.getPreviewPhoto());
        showTitle(tourPoint.getTitle());
        showSubTitle(tourPoint.getSubTitle());
        showFullSizePhoto(tourPoint.getPhotos().get(0));
        showText(tourPoint.getDescription());
        showRating(tourPoint.getRating());
    }

    private void showRating(float rating) {
        ratingBar.setRating(rating);
    }

    @Override
    public void navigateToPhotos(long pointId) {
        Intent intent = new Intent(getContext(), PointPhotosActivity.class);
        intent.putExtra(PointPhotosActivity.EXTRA_POINT_ID, pointId);
        startActivity(intent);
    }

    private void showText(String text) {
        pointText.setText(text);
    }

    private void showFullSizePhoto(Photo photo) {
        Picasso.with(getContext()).load(photo.getScreenDependentUrl(getContext())).into(photoFullSize);
    }

    private void showSubTitle(String subTitle) {
        pointSubtitle.setText(subTitle);
    }

    private void showTitle(String title) {
        pointTitle.setText(title);
    }

    private void showPhotoPreview(Photo previewPHoto) {
        int imageSize = getResources().getDimensionPixelSize(R.dimen.preview_image_size);
        Picasso.with(getContext())
                .load(previewPHoto.getResizedtUrl(imageSize))
                .transform(new CircleTransform())
                .resize(imageSize, imageSize).centerCrop()
                .into(preview);
    }

    public void updatePointDetails(TourPoint tourPoint) {
        presenter.update(tourPoint);
    }

    public void setTour(Tour tour) {
        presenter.setTour(tour);
    }

}
