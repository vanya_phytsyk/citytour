package one.citytour.ui.widget.playbutton;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import one.citytour.R;
import rx.subjects.PublishSubject;

/**
 * Created by vanya on 8/9/2016.
 */

public class AudioPlayerWidget extends FrameLayout {
    private TextView playDurationView;
    private ProgressBar progressBar;
    private SeekBar seekBar;
    private ImageButton playButton;
    private int duration;
    private int currentSecond;
    private PlayButtonClickListener playButtonClickListener;

    private boolean isPlaying;
    private PublishSubject<Integer> changeAudioPositionSubject;

    private boolean isTouching;

    public AudioPlayerWidget(Context context) {
        super(context);
        init(context);
    }

    public AudioPlayerWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AudioPlayerWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.layout_audio_player, this);

        playDurationView = (TextView) findViewById(R.id.time);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        seekBar = (SeekBar) findViewById(R.id.seek_bar);
        playButton = (ImageButton) findViewById(R.id.play_button);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (changeAudioPositionSubject != null) {
                    changeAudioPositionSubject.onNext(seekBar.getProgress());
                }
                isTouching = false;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isTouching = true;
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    updateDuration(progress, seekBar.getMax());
                }
            }
        });
        playButton.setOnClickListener(view -> {
            if (playButtonClickListener != null) {
                playButtonClickListener.playButtonClicked();
            }
        });
    }

    public void showProgress() {
        progressBar.setVisibility(VISIBLE);
        seekBar.setVisibility(GONE);
        playButton.setClickable(false);
    }

    public void hideProgress() {
        progressBar.setVisibility(GONE);
        seekBar.setVisibility(VISIBLE);
        playButton.setClickable(true);
    }

    public void updateDuration(int currentSecond, int maxDuration) {
        this.currentSecond = currentSecond;
        if (seekBar.getMax() != maxDuration) {
            seekBar.setMax(maxDuration);
        }
        seekBar.setProgress(currentSecond);
        String currentPositionText = getFormattedTimeText(currentSecond);
        String durationText = getFormattedTimeText(maxDuration);
        playDurationView.setText(String.format("%s/%s", currentPositionText, durationText));
    }

    private String getFormattedTimeText(int seconds) {
        return String.format("%02d:%02d", seconds / 60, seconds % 60);
    }

    public void showPlaying() {
        playButton.setImageResource(R.mipmap.ic_pause_white_24dp);
        isPlaying = true;
    }

    public void showPausing() {
        playButton.setImageResource(R.mipmap.ic_play_arrow_white_24dp);
        isPlaying = false;
    }

    public interface PlayButtonClickListener {
        public void playButtonClicked();
    }

    public void setPlayButtonClickListener(PlayButtonClickListener playButtonClickListener) {
        this.playButtonClickListener = playButtonClickListener;
    }

    public void setChangeAudioPositionSubject(PublishSubject<Integer> changeAudioPositionSubject) {
        this.changeAudioPositionSubject = changeAudioPositionSubject;
    }

    public boolean isTouching() {
        return isTouching;
    }
}
