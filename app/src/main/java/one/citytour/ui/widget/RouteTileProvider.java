package one.citytour.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.Log;

import one.citytour.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileProvider;

import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 * Created by vanya on 5/31/2016.
 */
public class RouteTileProvider implements TileProvider {
    private int tileWidth;
    private BitMapThreadLocal bitmapLocal;
    private List<LatLng> route;
    private Context context;

    private int pathWidth;
    private Paint paint;
    private Path path;
    private TileProjection.DoublePoint doublePoint = new TileProjection.DoublePoint(0, 0);

    private enum LineStyle {
        SIMPLE_SHADOW, LIKE_GOOGLE
    }

    private LineStyle lineStyle = LineStyle.SIMPLE_SHADOW;

    public RouteTileProvider(Context context, List<LatLng> route) {
        tileWidth = context.getResources().getDisplayMetrics().widthPixels;
        bitmapLocal = new BitMapThreadLocal();
        this.route = route;
        this.context = context;
        init();
    }

    private void init() {
        pathWidth = context.getResources().getDimensionPixelOffset(R.dimen.map_route_width);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(pathWidth);
        paint.setColor(context.getResources().getColor(R.color.map_route_stroke));
        paint.setPathEffect(new CornerPathEffect(pathWidth));
        switch (lineStyle) {
            case SIMPLE_SHADOW:
                paint.setShadowLayer(1.0f, 0.0f, 2.0f, 0xFF000000);
                break;
        }

        path = new Path();
        path.setFillType(Path.FillType.WINDING);
    }

    @Override
    public synchronized Tile getTile(int x, int y, int zoom) {
        Log.d("Route", x + " " + y + " " + zoom);

        TileProjection projection = new TileProjection(tileWidth,
                x, y, zoom);
        path.rewind();
        byte[] data;
        Bitmap bitmap = getNewBitmap();
        Canvas canvas = new Canvas(bitmap);
        onDraw(canvas, projection);
        data = bitmapToByteArray(bitmap);
        Tile tile = new Tile(tileWidth, tileWidth, data);
        return tile;
    }

    private void onDraw(Canvas canvas, TileProjection projection) {
        LatLng firstPoint = route.get(0);
        projection.latLngToPoint(firstPoint, doublePoint);
        float firstX = (float) doublePoint.x;
        float firstY = (float) doublePoint.y;
        path.moveTo(firstX, firstY);
        float[] points = new float[route.size() * 2];
        points[0] = firstX;
        points[1] = firstY;
        for (int i = 1; i < route.size(); ++i) {
            LatLng latLng = route.get(i);
            projection.latLngToPoint(latLng, doublePoint);
            path.lineTo((float) doublePoint.x, (float) doublePoint.y);

        }
        if (lineStyle == LineStyle.LIKE_GOOGLE) {
            canvas.drawPath(path, paint);
            paint.setStrokeWidth((float) (pathWidth * 0.1));
            paint.setColor(context.getResources().getColor(R.color.map_route_stroke_dark));
            path.offset(2, 2);
            canvas.drawPath(path, paint);
        } else {
            canvas.drawPath(path, paint);
        }
    }

    /**
     * Get an empty bitmap, which may however be reused from a previous call in
     * the same thread.
     *
     * @return
     */
    private Bitmap getNewBitmap() {
        Bitmap bitmap = bitmapLocal.get();
        // Clear the previous bitmap
        bitmap.eraseColor(Color.TRANSPARENT);
        return bitmap;
    }

    private static byte[] bitmapToByteArray(Bitmap bm) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] data = bos.toByteArray();
        return data;
    }

    class BitMapThreadLocal extends ThreadLocal<Bitmap> {
        @Override
        protected Bitmap initialValue() {
            Bitmap image = Bitmap.createBitmap(tileWidth, tileWidth,
                    Bitmap.Config.ARGB_8888);
            return image;
        }
    }
}
