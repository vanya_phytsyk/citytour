package one.citytour.ui.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import one.citytour.presenter.helper.StorageHelper;
import rx.Observable;
import rx.Subscriber;
import timber.log.Timber;

/**
 * Created by vanya on 7/12/2016.
 */
public class FileUtil {
    public static Observable<String> saveFileToCache(Context context, Uri uri) {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    InputStream input = context.getContentResolver().openInputStream(uri);
                    File file = new File(createAudioFilePath());
                    try {
                        OutputStream output = new FileOutputStream(file);
                        try {
                            try {
                                byte[] buffer = new byte[4 * 1024]; // or other buffer size
                                int read;

                                if (input != null) {
                                    while ((read = input.read(buffer)) != -1) {
                                        output.write(buffer, 0, read);
                                    }
                                }
                                output.flush();
                            } finally {
                                output.close();
                                String audiopath = file.getAbsolutePath();
                                subscriber.onNext(audiopath);
                                subscriber.onCompleted();
                            }
                        } catch (Exception e) {
                            Timber.e(e, "");
                        }

                    } finally {
                        try {
                            if (input != null) {
                                input.close();
                            }
                        } catch (IOException e) {
                            Timber.e(e, "");
                        }
                    }
                } catch (FileNotFoundException e) {
                    Timber.e(e, "");
                }
            }
        });

    }

    private static String createAudioFilePath() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "AUDIO_" + timeStamp + "_";
        return StorageHelper.getInstance().getCachedFileUri(new Date().toString()).getPath() + ".mp3";
    }


    public static String getPathFromPhotoUri(Context context, Uri uri, String[] column) {
        Cursor cursor = context.getContentResolver().query(uri,
                column, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(column[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            return picturePath;
        }
        return null;
    }


    public static Uri createPhotoUri() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        return StorageHelper.getInstance().getCachedFileUri(imageFileName);
    }



}