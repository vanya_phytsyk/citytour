package one.citytour.ui.dialog;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.DialogFragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import one.citytour.R;
import one.citytour.audio.AudioPlayer;
import one.citytour.audio.AudioRecorder;
import one.citytour.presenter.helper.StorageHelper;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by vanya on 7/9/2016.
 */
public class RecordAudioDialog extends DialogFragment {

    @BindView(R.id.button_record)
    Button recordButton;

    @BindView(R.id.record_time)
    TextView recordTime;

    @BindView(R.id.play_pause_button)
    Button playPauseButton;

    private boolean isRecording;
    private boolean isRecorded;
    private RecordCompleteCallback callback;
    private boolean isPlaying;

    public void setCallback(RecordCompleteCallback callback) {
        this.callback = callback;
    }

    private String fileName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        fileName = "temp";
        if (savedInstanceState != null) {
            dismiss();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.dialog_audio_record, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    private AudioRecorder audioRecorder;

    Subscription amplitudeSubscription;

    @OnClick(R.id.button_record)
    void recordButtonClicked() {
        if (isRecording) {
            stopRecording();
            isRecording = false;
            isRecorded = true;
            recordButton.setText(R.string.record);
            playPauseButton.setVisibility(View.VISIBLE);
        } else {
            startRecording();
            isRecording = true;
            recordButton.setText(R.string.stop);
            playPauseButton.setVisibility(View.GONE);
            AudioPlayer.getInstance().stopAudio();
        }
    }

    @OnClick(R.id.play_pause_button)
    void playPauseClicked() {
        if (isPlaying) {
            AudioPlayer.getInstance().pauseAudio();
        } else {
            AudioPlayer.getInstance().playAudio("temp");
        }
    }

    private void startRecording() {
        audioRecorder = new AudioRecorder(fileName);
        audioRecorder.startRecording();
        amplitudeSubscription = audioRecorder.getAmplitudeValue().observeOn(AndroidSchedulers.mainThread()).subscribe(longIntegerPair -> {
            animateRecordButton(longIntegerPair.second);
        });
    }

    private void animateRecordButton(float amplitude) {
        float ratio = Short.MAX_VALUE / amplitude / 4;
        ratio = ratio > 1.5f ? 1.5f : ratio < 1 ? 1 : ratio;

        ObjectAnimator scaleX = ObjectAnimator.ofFloat(recordButton, "scaleX", ratio);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(recordButton, "scaleY", ratio);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(scaleX, scaleY);
        animatorSet.setDuration(100);
        animatorSet.setInterpolator(new OvershootInterpolator());
        animatorSet.start();
    }

    private void stopRecording() {
        if (amplitudeSubscription != null) {
            amplitudeSubscription.unsubscribe();
        }
        if (audioRecorder != null) {
            audioRecorder.stopRecording();
        }
        isRecording = false;
        isRecorded = false;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();
        stopRecording();
        AudioPlayer.getInstance().stopAudio();
    }

    @OnClick(R.id.save_button)
    void saveClicked() {
        if (isRecorded) {
            if (callback != null) {
                callback.onComplete(audioRecorder.getFileUri());
            }
            dismiss();
        } else {
            Toast.makeText(getActivity(), "Yout have not recorded audio", Toast.LENGTH_SHORT).show();
        }
    }

    public interface RecordCompleteCallback {
        void onComplete(Uri fileUri);
    }
}
