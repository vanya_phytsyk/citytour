package one.citytour.ui.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import one.citytour.R;
import one.citytour.domain.model.Tour;

/**
 * Created by vanya on 5/10/2016.
 */
public class TourListAdapter extends RecyclerView.Adapter<TourListAdapter.ViewHolder> {

    private static final int ANIM_DURATION = 200;
    private final Context context;
    private List<Tour> tours = new ArrayList<>();
    private OnItemClickListener onItemClickListener;
    private OnMenuButtonClickListener onMenuButtonClickListener;
    private int actionButtonWidth;

    private final NumberFormat numberFormat;
    private final RelativeSizeSpan sizeSpan;

    public TourListAdapter(Context context) {
        this.context = context;
        actionButtonWidth = context.getResources().getDimensionPixelOffset(R.dimen.tour_list_item_menu_width);

        this.numberFormat = new DecimalFormat("0.0");
        this.sizeSpan = new RelativeSizeSpan(.5f);
    }

    public void setTours(List<Tour> tours) {
        this.tours = tours;
    }

    public void updateTour(Tour tour) {
        int index = tours.indexOf(tour);
        notifyItemChanged(index);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnMenuButtonClickListener(OnMenuButtonClickListener onMenuButtonClickListener) {
        this.onMenuButtonClickListener = onMenuButtonClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_tour, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Tour tour = tours.get(position);
        Picasso.with(context).load(tour.getCoverPhoto().getScreenDependentUrl(context)).fit()
                .centerCrop().placeholder(R.drawable.photo_placeholder).into(holder.photo);
        holder.title.setText(tour.getTitle());
        holder.description.setText(tour.getDescription());
        View.OnClickListener onClickListener = v -> {
            onItemClickListener.onItemClicked(tour);
        };
        holder.itemView.setOnClickListener(onClickListener);
        holder.photo.setOnClickListener(onClickListener);
        holder.menuButton.setOnClickListener(view -> {
            if (onMenuButtonClickListener != null) {
                onMenuButtonClickListener.onMenuButtonClicked(tour);
            }
        });

        Spannable spannable = new SpannableString(numberFormat.format(tour.getRating()));
        spannable.setSpan(sizeSpan, 1, 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        holder.rating.setText(spannable);
    }

    public void animateTourHiding(Tour tour) {
        int index = tours.indexOf(tour);
        tours.remove(tour);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return tours.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView photo;
        private TextView title;
        private TextView description;
        private ImageView menuButton;
        private TextView rating;

        public ViewHolder(View itemView) {
            super(itemView);
            photo = (ImageView) itemView.findViewById(R.id.photo);
            title = (TextView) itemView.findViewById(R.id.title);

            description = (TextView) itemView.findViewById(R.id.description);
            menuButton = (ImageView) itemView.findViewById(R.id.menu_button);
            menuButton.getDrawable().setColorFilter(getMenuButtonColor(), PorterDuff.Mode.MULTIPLY);
            rating = (TextView) itemView.findViewById(R.id.rating);
        }

        private int getMenuButtonColor() {
            return context.getResources().getColor(R.color.textColorSecondary);
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(Tour tour);
    }

    public interface OnMenuButtonClickListener {
        void onMenuButtonClicked(Tour tour);
    }

}
