package one.citytour.presenter.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import one.citytour.CityTourApplication;

/**
 * Created by vanya on 7/20/2016.
 */
public class ConnectivityHelper {
    public static boolean isNetworkAvailable() {
        ConnectivityManager cm;
        cm = (ConnectivityManager) CityTourApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}

