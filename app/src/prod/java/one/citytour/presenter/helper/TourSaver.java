package one.citytour.presenter.helper;

import android.graphics.Bitmap;

import one.citytour.CityTourApplication;
import one.citytour.domain.model.Photo;
import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;
import one.citytour.domain.repository.impl.Injector;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by vanya on 6/7/2016.
 */
public class TourSaver {
    public Observable<Photo> saveTour(Tour tourForSave) {
        return Injector.provideRemoteTourRepository()
                .getTour(tourForSave.getId())
                .flatMap(tour -> Injector.provideRemoteRouteRepository().getRoute(tour)
                        .flatMap(latLngs -> latLngs.isEmpty() ? Observable.just(tour)
                                : Injector.provideLocalRouteRepository().saveRoute(tour, latLngs)))
                .flatMap(tour -> Injector.provideLocalTourRepository().save(tour))
                .flatMap(tour -> savePhoto(tour, tour.getCoverPhoto()))
                .flatMap(tour -> Observable.from(tour.getPoints()))
                .flatMap(this::saveAudio)
                .subscribeOn(Schedulers.newThread())
                .flatMap(tourPoint -> savePhoto(tourPoint, tourPoint.getPreviewPhoto()))
                .flatMap(tourPoint -> Observable.from(tourPoint.getPhotos()))
                .flatMap(photo -> savePhoto(photo, photo))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<TourPoint> saveAudio(TourPoint tourPoint) {
        return Injector.provideRemoteFileRepository().getFile(tourPoint.getAudio().getUrl())
                .flatMap(responseBody -> Observable.create(new Observable.OnSubscribe<TourPoint>() {
                    @Override
                    public void call(Subscriber<? super TourPoint> subscriber) {
                        try {
                            byte[] bytes = responseBody.bytes();
                            StorageHelper.getInstance().cacheFile(tourPoint.getAudio().getUrl(), bytes, uri -> {
                                        Timber.d("cached file id: %d", tourPoint.getAudio().getId());
                                        subscriber.onNext(tourPoint);
                                        subscriber.onCompleted();
                                    }
                                    , throwable -> {
                                        Timber.d("cached file id: %d failure", tourPoint.getAudio().getId());
                                        subscriber.onError(new Exception("Something happen"));
                                    }
                            );
                        } catch (IOException e) {
                            subscriber.onError(e);
                        }
                    }
                })).subscribeOn(Schedulers.newThread());
    }

    private <T> Observable<T> savePhoto(T photoholder, Photo photo) {
        return Observable.create(new Observable.OnSubscribe<Photo>() {
            @Override
            public void call(Subscriber<? super Photo> subscriber) {
                ByteArrayOutputStream stream = null;
                try {
                    Timber.d("save photo id: %d", photo.getId());
                    Bitmap bitmap = Picasso.with(CityTourApplication.getContext()).
                            load(photo.getScreenDependentUrl(CityTourApplication.getContext())).get();
                    stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();

                    StorageHelper.getInstance().storeFile(photo.getUrl(), byteArray, uri -> {
                        photo.setUrl(uri.toString());
                        bitmap.recycle();
                        subscriber.onNext(photo);
                        subscriber.onCompleted();
                    }, subscriber::onError);
                } catch (IOException e) {
                    subscriber.onError(e);
                    closeStream(stream);
                } finally {
                    closeStream(stream);
                }
            }
        }).flatMap(photo1 -> Injector.provideLocalPhotoRepository().updatePhoto(photo)).flatMap(photo1 -> Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {
                subscriber.onNext(photoholder);
                subscriber.onCompleted();
            }
        })).subscribeOn(Schedulers.newThread());

    }

    private void closeStream(OutputStream stream) {
        if (stream != null) {
            try {
                stream.close();
                stream.flush();
            } catch (IOException e) {
                Timber.e(e, "");
            }
        }
    }

    public Observable<Tour> deleteTour(Tour tour) {
        return Observable.create((Observable.OnSubscribe<Tour>) subscriber -> {
            Injector.provideLocalTourRepository().deleteFromSaved(tour);
            Injector.provideLocalRouteRepository().removeRoute(tour.getId());
            StorageHelper.getInstance().removeFromStorage(tour.getCoverPhoto().getUrl());
            subscriber.onNext(tour);
            subscriber.onCompleted();
        }).flatMap(tour1 -> Observable.from(tour.getPoints()))
                .flatMap(tourPoint -> {
                    StorageHelper.getInstance().removeFromStorage(tourPoint.getPreviewPhoto().getUrl());
                    StorageHelper.getInstance().removeFromStorage(tourPoint.getAudio().getUrl());
                    return Observable.just(tourPoint);
                })
                .flatMap(tourPoint -> Observable.from(tourPoint.getPhotos())
                        .map((Func1<Photo, Photo>) photo -> {
                            StorageHelper.getInstance().removeFromStorage(tourPoint.getPreviewPhoto().getUrl());
                            return photo;
                        })).flatMap(photo -> Observable.just(tour))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
