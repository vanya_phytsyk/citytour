package one.citytour.domain.repository.impl;

import one.citytour.domain.model.TourListResponse;
import one.citytour.domain.repository.impl.retrofit.TourApi;
import one.citytour.domain.model.Tour;
import one.citytour.domain.repository.RemoteTourRepository;

import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by vanya on 5/21/2016.
 */
public class RemoteTourRepositoryImpl implements RemoteTourRepository {

    TourApi tourApi;

    protected RemoteTourRepositoryImpl() {
        tourApi = RetrofitHelper.getInstance().getRetrofit().create(TourApi.class);
    }

    @Override
    public Observable<TourListResponse> getTours(double latitude, double longitude) {
        return tourApi.getTourList(latitude, longitude)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Tour> getTour(long id) {
        return tourApi.getTour(id);
    }

    @Override
    public Observable<Tour> createTour(Tour tour) {
        return tourApi.createTour(tour)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<TourListResponse> getOwnTours() {
        return tourApi.getOwnTourList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Tour> updateTour(Tour tour) {
        return tourApi.updateTour(tour.getId(), tour)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<Void>> deleteTour(long id) {
        return tourApi.deleteTour(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
