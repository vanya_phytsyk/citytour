package one.citytour.domain.repository.impl;

import com.google.gson.Gson;

import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.ResponseBody;
import one.citytour.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import one.citytour.domain.model.AuthResponse;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vanya on 5/6/2016.
 */
public class RetrofitHelper {
    private static final String MOCK_URL = "http://demo6200350.mockable.io/";
    private static final String LIVE_URL = "http://52.32.182.23/";
    private static final String LIVE_DOMAIN = "http://citytour.one/";

    private static final String SERVER_URL = LIVE_DOMAIN;
    private Retrofit retrofit;

    private static class LazyHolder {
        private static final RetrofitHelper INSTANCE = new RetrofitHelper();
    }

    public static RetrofitHelper getInstance() {
        return LazyHolder.INSTANCE;
    }

    private RetrofitHelper() {
        Retrofit.Builder builder = new Retrofit.Builder();
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClientBuilder.addInterceptor(logging);
        }

        httpClientBuilder.addInterceptor(chain -> {
            Request original = chain.request();
            if (AuthLocalRepositoryImpl.getInstance().hasAccessToken()) {
                Request.Builder requestBuilder = original.newBuilder();
                requestBuilder.header("Authorization", "Bearer " + AuthLocalRepositoryImpl.getInstance().getAccessToken());
                Request request = requestBuilder.build();
                return chain.proceed(request);
            } else {
                return chain.proceed(original);
            }
        });

        httpClientBuilder.authenticator((route, response) -> {
            if (AuthLocalRepositoryImpl.getInstance().hasRefreshToken()) {
                HttpUrl.Builder urlBuilder = HttpUrl.parse(SERVER_URL + "_/token/refresh").newBuilder();//fuuuuu
                urlBuilder.addQueryParameter("refresh_token", AuthLocalRepositoryImpl.getInstance().getRefreshToken());//fuuuuu
                String url = urlBuilder.build().toString();//fuuuuu
                Request refreshTokenRequest = new Request.Builder().url(url).build();//fuuuuu
                ResponseBody responseBody = new OkHttpClient.Builder().build().newCall(refreshTokenRequest).execute().body(); //fuuuuu
                AuthResponse authResponse = new Gson().fromJson(responseBody.string(), AuthResponse.class);
                AuthLocalRepositoryImpl.getInstance().storeAuthCredentials(authResponse);
                if (AuthLocalRepositoryImpl.getInstance().hasAccessToken()) {
                    return response.request().newBuilder()
                            .header("Authorization", "Bearer " + AuthLocalRepositoryImpl.getInstance().getAccessToken())
                            .build();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        });

        builder.client(httpClientBuilder.build());
        retrofit = builder
                .baseUrl(SERVER_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
