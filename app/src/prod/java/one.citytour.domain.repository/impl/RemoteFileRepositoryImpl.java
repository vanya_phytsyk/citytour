package one.citytour.domain.repository.impl;

import one.citytour.domain.repository.impl.retrofit.FileApi;
import one.citytour.domain.repository.RemoteFileRepository;

import okhttp3.ResponseBody;
import rx.Observable;

/**
 * Created by vanya on 6/9/2016.
 */
public class RemoteFileRepositoryImpl implements RemoteFileRepository {
    private FileApi fileApi;

    protected RemoteFileRepositoryImpl() {
        fileApi = RetrofitHelper.getInstance().getRetrofit().create(FileApi.class);
    }

    @Override
    public Observable<ResponseBody> getFile(String url) {
        return fileApi.getFile(url);
    }
}
