package one.citytour.domain.repository.impl;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import one.citytour.domain.repository.impl.retrofit.UploadFileApi;
import one.citytour.domain.model.Audio;
import one.citytour.domain.model.Photo;
import one.citytour.domain.repository.FileUploadRepository;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by vanya on 7/8/2016.
 */
public class FileUploadRepositoryImpl implements FileUploadRepository {

    protected FileUploadRepositoryImpl() {
    }

    @Override
    public Observable<Photo> uploadPhoto(String filePath) {
        File file = new File(filePath);
        RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), file);
        UploadFileApi fileApi = RetrofitHelper.getInstance().getRetrofit().create(UploadFileApi.class);
        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), "1"); //1 - photo file type for server side

        return fileApi.postPhoto(fileBody, type)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Audio> uploadAudio(String audioPath) {
        File file = new File(audioPath);
        RequestBody fileBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        UploadFileApi fileApi = RetrofitHelper.getInstance().getRetrofit().create(UploadFileApi.class);
        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), "2");

        return fileApi.postAudio(fileBody, type);
    }
}
