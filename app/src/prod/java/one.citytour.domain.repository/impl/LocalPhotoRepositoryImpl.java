package one.citytour.domain.repository.impl;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import one.citytour.CityTourApplication;
import one.citytour.domain.model.Photo;
import one.citytour.domain.repository.LocalPhotoRepository;
import one.citytour.domain.repository.impl.realrmentity.RealmPhoto;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by vanya on 7/2/2016.
 */
public class LocalPhotoRepositoryImpl implements LocalPhotoRepository {

    Realm realmDefaultInstance;

    protected LocalPhotoRepositoryImpl() {
        RealmConfiguration realmConfig = RealmConfigHolder.getConfiguration();
        Realm.setDefaultConfiguration(realmConfig);
        realmDefaultInstance = Realm.getDefaultInstance();
    }

    @Override
    public Observable<Photo> updatePhoto(Photo photo) {
        return Observable.create(new Observable.OnSubscribe<Photo>() {
            @Override
            public void call(Subscriber<? super Photo> subscriber) {
                realmDefaultInstance.beginTransaction();
                realmDefaultInstance.copyToRealmOrUpdate(new RealmPhoto(photo));
                realmDefaultInstance.commitTransaction();
                subscriber.onNext(photo);
                subscriber.onCompleted();
            }
        });
    }
}
