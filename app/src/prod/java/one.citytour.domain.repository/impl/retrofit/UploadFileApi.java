package one.citytour.domain.repository.impl.retrofit;

import okhttp3.RequestBody;
import one.citytour.domain.model.Audio;
import one.citytour.domain.model.Photo;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

/**
 * Created by vanya on 7/8/2016.
 */
public interface UploadFileApi {
    @Multipart
    @POST("_/file")
    Observable<Photo> postPhoto(@Part("file\"; filename=\"pp.png\" ") RequestBody file, @Part("type") RequestBody type);

    @Multipart
    @POST("_/file")
    Observable<Audio> postAudio(@Part("file\"; filename=\"pp\" ") RequestBody file, @Part("type") RequestBody type);
}
