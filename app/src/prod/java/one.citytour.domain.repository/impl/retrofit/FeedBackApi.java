package one.citytour.domain.repository.impl.retrofit;

import one.citytour.domain.model.FeedBack;
import retrofit2.http.Body;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by vanya on 7/31/2016.
 */
public interface FeedBackApi {
@PUT("_/tour/{tourId}/point/{pointId}/rating")
    public Observable<FeedBack> putRating(@Path("tourId") long tourId, @Path("pointId") long pointId, @Body FeedBack rating);
}
