package one.citytour.domain.repository.impl.retrofit;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by vanya on 5/7/2016.
 */
public interface FileApi {
    @GET
    Observable<ResponseBody> getFile(@Url String uri);
}
