package one.citytour.domain.repository.impl.retrofit;

import one.citytour.domain.model.AuthResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by vanya on 7/3/2016.
 */
public interface AuthApi {
    @FormUrlEncoded
    @POST("_/auth/authorize")
    Observable<AuthResponse> auth(@Field("socialNetworkType") String socialNetworkType, @Field("accessToken") String accessToken);

    @GET("_/token/refresh")
    Observable<AuthResponse> refresh(@Query("refresh_token") String refreshToken);

    @GET("_/token/refresh")
    Call<AuthResponse> refreshSync(@Query("refresh_token") String refreshToken);
}