package one.citytour.domain.repository.impl.retrofit;

import one.citytour.domain.model.Profile;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by vanya on 7/3/2016.
 */
public interface ProfileApi {
    @GET("_/user/info")
    Observable<Profile> getProfile();
}
