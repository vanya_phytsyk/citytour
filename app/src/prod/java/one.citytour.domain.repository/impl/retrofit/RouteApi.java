package one.citytour.domain.repository.impl.retrofit;

import one.citytour.domain.model.RouteResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by vanya on 18.04.16.
 */
public interface RouteApi {
    @GET("/maps/api/directions/json")
    Observable<RouteResponse> getRoute(
            @Query(value = "origin") String position,
            @Query(value = "destination") String destination,
            @Query(value = "waypoints") String waypoints,
            @Query("sensor") boolean sensor,
            @Query("mode") String mode,
            @Query("language") String language,
            @Query("key") String apiKey);
}
