package one.citytour.domain.repository.impl.retrofit;

import one.citytour.domain.model.Tour;

import java.util.List;

import one.citytour.domain.model.TourListResponse;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by vanya on 5/6/2016.
 */
public interface TourApi {
    @GET("_/tour/{id}")
    Observable<Tour> getTour(@Path("id") long tourId);

    @GET("_/tour")
    Observable<TourListResponse> getTourList(@Query("lat") double latitude, @Query("lng") double longitude);

    @GET("_/user/me/tours")
    Observable<TourListResponse> getOwnTourList();

    @POST("_/tour")
    Observable<Tour> createTour(@Body Tour tour);

    @PUT("_/tour/{tourID}")
    Observable<Tour> updateTour(@Path("tourID") long tourId, @Body Tour tour);

    @DELETE("_/tour/{id}")
    Observable<Response<Void>> deleteTour(@Path("id") long tourId);
}
