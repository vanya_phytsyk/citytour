package one.citytour.domain.repository.impl;

import one.citytour.domain.model.FeedBack;
import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;
import one.citytour.domain.repository.RemoteRatingRepository;
import one.citytour.domain.repository.impl.retrofit.FeedBackApi;
import rx.Observable;

/**
 * Created by vanya on 8/1/2016.
 */
public class RemoteRatingRepositoryImpl implements RemoteRatingRepository {

    private FeedBackApi feedBackApi;

    public RemoteRatingRepositoryImpl() {
        feedBackApi = RetrofitHelper.getInstance().getRetrofit().create(FeedBackApi.class);
    }

    @Override
    public Observable<FeedBack> updateRating(Tour tour, TourPoint point, FeedBack newRating) {
        return feedBackApi.putRating(tour.getId(), point.getId(), newRating);
    }
}
