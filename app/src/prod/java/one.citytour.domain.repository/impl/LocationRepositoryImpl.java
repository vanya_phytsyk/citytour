package one.citytour.domain.repository.impl;

import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceFilter;
import com.google.android.gms.maps.model.LatLng;

import one.citytour.CityTourApplication;
import one.citytour.domain.repository.LocalProfileRepository;
import one.citytour.domain.repository.LocationRepository;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by vanya on 7/7/2016.
 */
public class LocationRepositoryImpl implements LocationRepository {

    private Observable<LatLng> observable;
    private Subscriber<LatLng> subscriber;

    protected LocationRepositoryImpl() {

    }

    @Override
    public Observable<Location> getCurrentLocation() {
        LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10000); //10sec
        return new ReactiveLocationProvider(CityTourApplication.getContext()).getUpdatedLocation(request);
    }
}
