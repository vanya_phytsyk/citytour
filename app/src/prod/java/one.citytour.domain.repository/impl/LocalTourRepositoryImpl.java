package one.citytour.domain.repository.impl;

import one.citytour.CityTourApplication;
import one.citytour.domain.model.Profile;
import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;
import one.citytour.domain.repository.LocalTourRepository;
import one.citytour.domain.repository.impl.realrmentity.RealmPoint;
import one.citytour.domain.repository.impl.realrmentity.RealmProfile;
import one.citytour.domain.repository.impl.realrmentity.RealmTour;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by vanya on 5/25/2016.
 */
public class LocalTourRepositoryImpl implements LocalTourRepository {

    Realm realmDefaultInstance;
    RealmConfiguration realmConfig;

    protected LocalTourRepositoryImpl() {
        realmConfig = RealmConfigHolder.getConfiguration();
        Realm.setDefaultConfiguration(realmConfig);

        realmDefaultInstance = Realm.getDefaultInstance();
    }

    @Override
    public Observable<List<Tour>> getSavedTours() {
        return Observable.create((Observable.OnSubscribe<List<Tour>>) subscriber -> {
            Realm realmInstance = Realm.getInstance(realmConfig);
            RealmResults<RealmTour> results = realmInstance.where(RealmTour.class).equalTo("isSaved", true).findAll();
            List<Tour> tours = new ArrayList<>(results.size());
            for (RealmTour realmTour : results) {
                tours.add(realmTour.convert());
            }
            subscriber.onNext(tours);
            subscriber.onCompleted();
            realmInstance.close();
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Tour> save(Tour tour) {
        return Observable.create(new Observable.OnSubscribe<Tour>() {
            @Override
            public void call(Subscriber<? super Tour> subscriber) {
                tour.setSaved(true);
                RealmTour realmTour = new RealmTour(tour);
                realmDefaultInstance.beginTransaction();
                RealmProfile ownProfile = realmDefaultInstance.where(RealmProfile.class).equalTo("isSelf", true).findFirst();
                if (tour.hasCreator() && ownProfile.getId() == tour.getCreator().getId()) {
                    realmTour.setCreator(ownProfile);
                }
                realmDefaultInstance.copyToRealmOrUpdate(realmTour);
                realmDefaultInstance.commitTransaction();
                subscriber.onNext(tour);
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public void cache(List<Tour> tours) {
        realmDefaultInstance.executeTransactionAsync(realm -> {
            RealmList<RealmTour> realmList = new RealmList<>();
            for (Tour tour : tours) {
                if (realm.where(RealmTour.class).equalTo("id", tour.getId()).equalTo("isSaved", true).findFirst() == null) {
                    RealmProfile ownProfile = realm.where(RealmProfile.class).equalTo("isSelf", true).findFirst();
                    RealmTour realmTour = new RealmTour(tour);
                    if (ownProfile != null && tour.hasCreator() && ownProfile.getId() == tour.getCreator().getId()) {
                        realmTour.setCreator(ownProfile);
                    }
                    realmList.add(realmTour);
                }
            }
            realm.copyToRealmOrUpdate(realmList);
        });
    }

    @Override
    public Observable<Tour> cache(Tour tour) {
        return Observable.create(new Observable.OnSubscribe<Tour>() {
            @Override
            public void call(Subscriber<? super Tour> subscriber) {
                RealmTour realmTour = new RealmTour(tour);
                realmDefaultInstance.beginTransaction();
                if (realmDefaultInstance.where(RealmTour.class).equalTo("id", tour.getId()).equalTo("isSaved", true).findFirst() == null) {
                    RealmProfile ownProfile = realmDefaultInstance.where(RealmProfile.class).equalTo("isSelf", true).findFirst();
                    if (ownProfile != null && tour.hasCreator() && ownProfile.getId() == tour.getCreator().getId()) {
                        realmTour.setCreator(ownProfile);
                    }
                    realmDefaultInstance.copyToRealmOrUpdate(realmTour);
                }
                realmDefaultInstance.commitTransaction();
                subscriber.onNext(tour);
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public Observable<List<Tour>> getCached() {
        return Observable.create((Observable.OnSubscribe<List<Tour>>) subscriber -> {
            Realm realm = Realm.getInstance(realmConfig);
            RealmResults<RealmTour> results = realm.where(RealmTour.class).findAll();
            List<Tour> tours = new ArrayList<>(results.size());
            for (RealmTour realmTour : results) {
                tours.add(realmTour.convert());
            }
            subscriber.onNext(tours);
            subscriber.onCompleted();
            realm.close();
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Tour> getSingleCached(long tourId) {
        return Observable.create((Observable.OnSubscribe<Tour>) subscriber -> {
            Realm realm = Realm.getInstance(realmConfig);
            RealmTour result = realm.where(RealmTour.class).equalTo("id", tourId).findFirst();
            subscriber.onNext(result.convert());
            subscriber.onCompleted();
            realm.close();
        });
    }

    @Override
    public Observable<Tour> getSingleSaved(long tourId) {
        return Observable.create((Observable.OnSubscribe<Tour>) subscriber -> {
            Realm realm = Realm.getInstance(realmConfig);
            RealmTour result = realm.where(RealmTour.class).equalTo("id", tourId).equalTo("isSaved", true).findFirst();
            if (result != null) {
                subscriber.onNext(result.convert());
            } else {
                subscriber.onNext(null);
            }
            subscriber.onCompleted();
            realm.close();
        });
    }

    @Override
    public void clearCache() {
        realmDefaultInstance.beginTransaction();
        realmDefaultInstance.where(RealmTour.class).equalTo("isSaved", false).findAll().deleteAllFromRealm();
        realmDefaultInstance.commitTransaction();
    }

    @Override
    public void deleteFromSaved(Tour tour) {
        Realm realmInstance = Realm.getInstance(realmConfig);
        realmInstance.beginTransaction();
        RealmTour realmTour = realmInstance.where(RealmTour.class).equalTo("id", tour.getId()).findFirst();
        realmTour.setSaved(false);
        realmInstance.copyToRealmOrUpdate(realmTour);
        realmInstance.commitTransaction();
    }

    @Override
    public Observable<TourPoint> getCachedPoint(long pointId) {
        return Observable.create((Observable.OnSubscribe<TourPoint>) subscriber -> {
            Realm realm = Realm.getInstance(realmConfig);
            RealmPoint result = realm.where(RealmPoint.class).equalTo("id", pointId).findFirst();
            if (result != null) {
                subscriber.onNext(result.convert());
            }
            subscriber.onCompleted();
            realm.close();
        });
    }
}
