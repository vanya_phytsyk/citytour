package one.citytour.domain.repository.impl;

import one.citytour.domain.repository.impl.retrofit.AuthApi;
import one.citytour.domain.model.Profile;
import one.citytour.domain.repository.Authorizer;
import rx.Observable;

/**
 * Created by vanya on 7/3/2016.
 */
public class AuthorizerImpl implements Authorizer {
    private AuthApi authApi;
    private AuthType authType;

    protected AuthorizerImpl(AuthType authType) {
        authApi = RetrofitHelper.getInstance().getRetrofit().create(AuthApi.class);
        this.authType = authType;
    }

    @Override
    public Observable<Profile> auth(String token) {
        return authApi.auth(authType.getType(), token)
                .flatMap(authResponse -> {
                    AuthLocalRepositoryImpl.getInstance().storeAuthCredentials(authResponse);
                    return Observable.just(authResponse);
                })
                .flatMap(authResponse -> Injector.provideRemoteProfileRepository().getProfile())
                .flatMap(profile -> {
                    profile.setSelf(true);
                    return Injector.provideLocalProfileRepository().storeProfile(profile);
                });
    }
}
