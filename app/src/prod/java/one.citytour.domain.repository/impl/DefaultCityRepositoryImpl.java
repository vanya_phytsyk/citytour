package one.citytour.domain.repository.impl;

import android.content.res.Resources;

import com.google.android.gms.maps.model.LatLng;

import one.citytour.CityTourApplication;
import one.citytour.R;
import one.citytour.domain.repository.DefaultCityRepository;

/**
 * Created by vanya on 7/21/2016.
 */
public class DefaultCityRepositoryImpl implements DefaultCityRepository {
    @Override
    public String getDefaultCityName() {
        Resources resources = CityTourApplication.getContext().getResources();
        String cityName = resources.getString(R.string.default_city_name);
        return cityName;
    }

    @Override
    public LatLng getDefaultCityLatLng() {
        Resources resources = CityTourApplication.getContext().getResources();
        double lattitude = Double.parseDouble(resources.getString(R.string.default_city_lattitude));
        double longitude = Double.parseDouble(resources.getString(R.string.default_city_longitude));
        return new LatLng(lattitude, longitude);
    }
}
