package one.citytour.domain.repository.impl;

import one.citytour.domain.repository.impl.retrofit.ProfileApi;
import one.citytour.domain.model.Profile;
import one.citytour.domain.repository.RemoteProfileRepository;
import rx.Observable;

/**
 * Created by vanya on 5/21/2016.
 */
public class RemoteProfileRepositoryImpl implements RemoteProfileRepository {

    private ProfileApi profileApi;

    protected RemoteProfileRepositoryImpl() {
        profileApi = RetrofitHelper.getInstance().getRetrofit().create(ProfileApi.class);
    }

    @Override
    public Observable<Profile> getProfile() {
        return profileApi.getProfile();
    }
}
