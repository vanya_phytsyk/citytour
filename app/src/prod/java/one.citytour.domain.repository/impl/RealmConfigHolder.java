package one.citytour.domain.repository.impl;

import io.realm.DynamicRealm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import one.citytour.CityTourApplication;

/**
 * Created by vanya on 7/31/2016.
 */
public class RealmConfigHolder {
    private static final int DATABASE_VERSION = 2; //Change if schema changed
    private RealmConfiguration realmConfiguration;
    private static RealmConfigHolder instance = new RealmConfigHolder();

    private RealmConfigHolder() {
        realmConfiguration = new RealmConfiguration.Builder(CityTourApplication.getContext())
                .schemaVersion(DATABASE_VERSION)
                .deleteRealmIfMigrationNeeded()
                .migration(migration)
                .build();
    }

    public static RealmConfiguration getConfiguration() {
        return instance.realmConfiguration;
    }

    RealmMigration migration = (realm, oldVersion, newVersion) -> {
        // probably it will be needed in future but now we just set deleteRealmIfMigrationNeeded
    };


}
