package one.citytour.domain.repository.impl;

import one.citytour.domain.repository.AuthLocalRepository;
import one.citytour.domain.repository.Authorizer;
import one.citytour.domain.repository.DefaultCityRepository;
import one.citytour.domain.repository.FileUploadRepository;
import one.citytour.domain.repository.LocalPhotoRepository;
import one.citytour.domain.repository.LocalProfileRepository;
import one.citytour.domain.repository.LocalRouteRepository;
import one.citytour.domain.repository.LocalTourRepository;
import one.citytour.domain.repository.LocationRepository;
import one.citytour.domain.repository.RemoteFileRepository;
import one.citytour.domain.repository.RemoteProfileRepository;
import one.citytour.domain.repository.RemoteRatingRepository;
import one.citytour.domain.repository.RemoteRouteRepository;
import one.citytour.domain.repository.RemoteTourRepository;

/**
 * Created by vanya on 7/19/2016.
 */
public class Injector {
    public static AuthLocalRepository provideAuthLocalRepository() {
        return AuthLocalRepositoryImpl.getInstance();
    }

    public static Authorizer provideAuthorizer(Authorizer.AuthType authType) {
        return new AuthorizerImpl(authType);
    }

    public static FileUploadRepository provideFileUploadRepository() {
        return new FileUploadRepositoryImpl();
    }

    public static LocalPhotoRepository provideLocalPhotoRepository() {
        return new LocalPhotoRepositoryImpl();
    }

    public static LocalProfileRepository provideLocalProfileRepository() {
        return new LocalProfileRepositoryImpl();
    }

    public static LocalRouteRepository provideLocalRouteRepository() {
        return new LocalRouteRepositoryImpl();
    }

    public static LocalTourRepository provideLocalTourRepository() {
        return new LocalTourRepositoryImpl();
    }

    public static LocationRepository provideLocationRepository() {
        return new LocationRepositoryImpl();
    }

    public static RemoteFileRepository provideRemoteFileRepository() {
        return new RemoteFileRepositoryImpl();
    }

    public static RemoteProfileRepository provideRemoteProfileRepository() {
        return new RemoteProfileRepositoryImpl();
    }

    public static RemoteRouteRepository provideRemoteRouteRepository() {
        return new RemoteRouteRepositoryImpl();
    }

    public static RemoteTourRepository provideRemoteTourRepository() {
        return new RemoteTourRepositoryImpl();
    }

    public static DefaultCityRepository provideDefaultCityRepository() {
        return new DefaultCityRepositoryImpl();
    }

    public static RemoteRatingRepository provideRemoteRatingRepository() {
        return new RemoteRatingRepositoryImpl();
    }
}
