package one.citytour.domain.repository.impl;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import one.citytour.CityTourApplication;
import one.citytour.domain.model.Profile;
import one.citytour.domain.repository.LocalProfileRepository;
import one.citytour.domain.repository.impl.realrmentity.RealmProfile;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by vanya on 7/3/2016.
 */
public class LocalProfileRepositoryImpl implements LocalProfileRepository {

    private final RealmConfiguration realmConfig;

    protected LocalProfileRepositoryImpl() {
        realmConfig = RealmConfigHolder.getConfiguration();
        Realm.setDefaultConfiguration(realmConfig);
    }

    @Override
    public Observable<Profile> getProfile() {
        return Observable.create(new Observable.OnSubscribe<Profile>() {
            @Override
            public void call(Subscriber<? super Profile> subscriber) {
                Realm realm = Realm.getInstance(realmConfig);
                RealmProfile realmProfile = realm.where(RealmProfile.class).equalTo("isSelf", true).findFirst();
                if (realmProfile != null) {
                    subscriber.onNext(realmProfile.convert());
                } else subscriber.onNext(null);
                subscriber.onCompleted();
                realm.close();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Profile> storeProfile(Profile profile) {
        return Observable.create(new Observable.OnSubscribe<Profile>() {
            @Override
            public void call(Subscriber<? super Profile> subscriber) {
                Realm realm = Realm.getInstance(realmConfig);
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(new RealmProfile(profile));
                realm.commitTransaction();
                subscriber.onNext(profile);
                subscriber.onCompleted();
                realm.close();
            }
        });
    }

    @Override
    public Observable<Object> clearCurrentProfile() {
        return Observable.create(o -> {
            Realm realm = Realm.getInstance(realmConfig);
            realm.beginTransaction();
            realm.where(RealmProfile.class).equalTo("isSelf", true).findAll().deleteAllFromRealm();
            realm.commitTransaction();
            o.onNext(null);
            o.onCompleted();
        });
    }
}
