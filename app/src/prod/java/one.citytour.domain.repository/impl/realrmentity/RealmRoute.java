package one.citytour.domain.repository.impl.realrmentity;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by vanya on 6/12/2016.
 */
public class RealmRoute extends RealmObject {
    @PrimaryKey
    private long tourId;

    private RealmList<RealmLatLng> latLngs = new RealmList<>();;

    public RealmRoute(){}

    public RealmRoute(long tourId, List<LatLng> latLngs) {
        this.tourId = tourId;
        for (LatLng latLng: latLngs) {
            this.latLngs.add(new RealmLatLng(latLng));
        }
    }

    public long getTourId() {
        return tourId;
    }

    public void setTourId(long tourId) {
        this.tourId = tourId;
    }

    public List<RealmLatLng> getLatLngs() {
        return latLngs;
    }

    public List<LatLng> convertToLatLngs() {
        List<LatLng> result = new ArrayList<>();
        for (RealmLatLng realmLatLng: latLngs) {
            result.add(new LatLng(realmLatLng.getLatitude(), realmLatLng.getLongitude()));
        }
        return result;
    }

}
