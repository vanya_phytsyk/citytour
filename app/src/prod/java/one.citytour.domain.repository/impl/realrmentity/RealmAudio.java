package one.citytour.domain.repository.impl.realrmentity;

import io.realm.RealmObject;
import one.citytour.domain.model.Audio;

/**
 * Created by vanya on 6/7/2016.
 */
public class RealmAudio extends RealmObject {
    private long id;
    private String url;

    public RealmAudio(){}

    public RealmAudio(Audio audio) {
        this.id = audio.getId();
        this.url = audio.getUrl();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public java.lang.String getUrl() {
        return url;
    }

    public void setUrl(java.lang.String url) {
        this.url = url;
    }

    public Audio convert() {
        return new Audio(id, url);
    }
}
