package one.citytour.domain.repository.impl.realrmentity;

import one.citytour.domain.model.Audio;
import one.citytour.domain.model.Photo;
import one.citytour.domain.model.TourPoint;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by vanya on 5/25/2016.
 */
public class RealmPoint extends RealmObject {
    private long id;

    private double longitude;
    private double latitude;
    private RealmPhoto previewPhoto;
    private RealmList<RealmPhoto> photos;
    private java.lang.String title;
    private java.lang.String subTitle;
    private java.lang.String description;
    private RealmAudio audio;
    private float rating;

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public RealmAudio getAudio() {
        return audio;
    }

    public void setAudio(RealmAudio audio) {
        this.audio = audio;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public RealmPhoto getPreviewPhoto() {
        return previewPhoto;
    }

    public void setPreviewPhoto(RealmPhoto previewPhoto) {
        this.previewPhoto = previewPhoto;
    }

    public List<RealmPhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(RealmList<RealmPhoto> photos) {
        this.photos = photos;
    }

    public java.lang.String getTitle() {
        return title;
    }

    public void setTitle(java.lang.String title) {
        this.title = title;
    }

    public java.lang.String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(java.lang.String subTitle) {
        this.subTitle = subTitle;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public RealmPoint() {
    }

    public TourPoint convert() {
        TourPoint tourPoint = new TourPoint();
        tourPoint.setId(id);
        tourPoint.setTitle(title);
        tourPoint.setAudio(new Audio(audio.getId(), audio.getUrl()));
        tourPoint.setLatitude(latitude);
        tourPoint.setLongitude(longitude);
        tourPoint.setSubTitle(subTitle);

        List<Photo> photos = new ArrayList<>();
        for (RealmPhoto realmPhoto : this.photos) {
            photos.add(new Photo(realmPhoto.getId(), realmPhoto.getUrl()));
        }

        tourPoint.setPhotos(photos);
        tourPoint.setPreviewPhoto(new Photo(previewPhoto.getId(), previewPhoto.getUrl()));
        tourPoint.setDescription(description);
        tourPoint.setRating(rating);

        return tourPoint;
    }

    public RealmPoint(TourPoint tourPoint) {
        this.id = tourPoint.getId();
        this.description = tourPoint.getDescription();
        this.previewPhoto = new RealmPhoto(tourPoint.getPreviewPhoto());
        this.subTitle = tourPoint.getSubTitle();

        RealmList<RealmPhoto> photos = new RealmList<>();
        for (Photo photo : tourPoint.getPhotos()) {
            photos.add(new RealmPhoto(photo));
        }

        this.photos = photos;
        this.latitude = tourPoint.getLatitude();
        this.longitude = tourPoint.getLongitude();
        this.audio = new RealmAudio(tourPoint.getAudio());
        this.title = tourPoint.getTitle();
        this.rating = tourPoint.getRating();
    }
}

