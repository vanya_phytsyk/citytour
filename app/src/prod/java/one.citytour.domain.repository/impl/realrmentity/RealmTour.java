package one.citytour.domain.repository.impl.realrmentity;

import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by vanya on 5/25/2016.
 */
public class RealmTour extends RealmObject {

    @PrimaryKey
    private long id;

    private String title;
    private RealmPhoto coverPhoto;
    private String description;
    private RealmProfile creator;
    private float rating;

    private boolean isSaved;

    private boolean isPrivate;

    private RealmList<RealmPoint> points;

    public RealmList<RealmPoint> getPoints() {
        return points;
    }

    public void setPoints(RealmList<RealmPoint> points) {
        this.points = points;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RealmPhoto getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(RealmPhoto coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    public RealmProfile getCreator() {
        return creator;
    }

    public void setCreator(RealmProfile creator) {
        this.creator = creator;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public RealmTour() {
    }

    public Tour convert() {
        Tour tour = new Tour();
        tour.setId(id);
        tour.setCoverPhoto(coverPhoto.convert());
        tour.setDescription(description);
        tour.setTitle(title);
        tour.setSaved(isSaved);

        List<TourPoint> points = new ArrayList<>();
        for (RealmPoint realmPoint : this.points) {
            points.add(realmPoint.convert());
        }
        tour.setPoints(points);
        tour.setRating(rating);
        tour.setCreator(creator != null ? creator.convert() : null);
        tour.setPrivate(isPrivate);

        return tour;
    }

    public RealmTour(Tour tour) {
        this.setId(tour.getId());
        this.setTitle(tour.getTitle());
        this.setCoverPhoto(new RealmPhoto(tour.getCoverPhoto()));
        this.setDescription(tour.getDescription());
        this.setSaved(tour.isSaved());

        RealmList<RealmPoint> points = new RealmList<RealmPoint>();
        for (TourPoint tourPoint : tour.getPoints()) {
            points.add(new RealmPoint(tourPoint));
        }

        this.setPoints(points);
        if (tour.hasCreator()) {
            this.setCreator(new RealmProfile(tour.getCreator()));
        }
        this.setRating((float) tour.getRating());
        this.setPrivate(tour.isPrivate());
    }

}
