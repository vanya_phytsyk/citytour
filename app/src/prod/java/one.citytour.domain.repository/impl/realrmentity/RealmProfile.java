package one.citytour.domain.repository.impl.realrmentity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import one.citytour.domain.model.Profile;

/**
 * Created by vanya on 7/3/2016.
 */
public class RealmProfile extends RealmObject {
    @PrimaryKey
    private long id;
    private String firstName;
    private String lastName;
    private RealmPhoto photo;
    private boolean isSelf;

    public RealmProfile(){}

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public RealmPhoto getPhoto() {
        return photo;
    }

    public RealmProfile(Profile profile) {
        this.id = profile.getId();
        this.firstName = profile.getFirstName();
        this.lastName = profile.getLastName();
        this.photo = new RealmPhoto(profile.getPhoto());
        this.isSelf = profile.isSelf();
    }

    public Profile convert() {
        Profile profile = new Profile();
        profile.setId(id);
        profile.setFirstName(firstName);
        profile.setLastName(lastName);
        profile.setPhoto(photo.convert());
        profile.setSelf(isSelf);
        return profile;
    }
}
