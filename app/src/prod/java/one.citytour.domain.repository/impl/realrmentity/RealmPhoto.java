package one.citytour.domain.repository.impl.realrmentity;

import io.realm.annotations.PrimaryKey;
import one.citytour.domain.model.Photo;

import io.realm.RealmObject;

/**
 * Created by vanya on 6/7/2016.
 */
public class RealmPhoto extends RealmObject {
    @PrimaryKey
    private long id;
    private String url;

    public RealmPhoto(){}

    public RealmPhoto(Photo photo) {
        this.id = photo.getId();
        this.url = photo.getUrl();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Photo convert() {
        return new Photo(id, url);
    }
}
