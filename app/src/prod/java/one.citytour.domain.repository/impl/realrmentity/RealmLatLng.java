package one.citytour.domain.repository.impl.realrmentity;

import com.google.android.gms.maps.model.LatLng;

import io.realm.RealmObject;

/**
 * Created by vanya on 6/14/2016.
 */
public class RealmLatLng extends RealmObject {
    private double latitude;
    private double longitude;

    public RealmLatLng(){}

    public RealmLatLng(LatLng latLng) {
        this.latitude = latLng.latitude;
        this.longitude = latLng.longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
