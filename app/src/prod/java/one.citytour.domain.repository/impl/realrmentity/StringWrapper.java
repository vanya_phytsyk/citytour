package one.citytour.domain.repository.impl.realrmentity;

import io.realm.RealmObject;

/**
 * Created by vanya on 5/27/2016.
 */
public class StringWrapper extends RealmObject {
    private String string;

    public StringWrapper(){}

    public StringWrapper(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }
}
