package one.citytour.domain.repository.impl;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import one.citytour.BuildConfig;
import one.citytour.domain.repository.RemoteRouteRepository;
import one.citytour.domain.repository.impl.retrofit.RouteApi;
import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;

import java.util.Collections;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by vanya on 5/6/2016.
 */
public class RemoteRouteRepositoryImpl implements RemoteRouteRepository {

    private static final String API_KEY = "AIzaSyDJlBpStYNntV0MNoiDjDY3Fe5lU4eC2uI";

    protected RemoteRouteRepositoryImpl() {

    }

    @Override
    public Observable<List<LatLng>> getRoute(Tour tour) {
        return requestTourRoute(tour);
    }

    private Observable<List<LatLng>> requestTourRoute(Tour tour) {
        List<TourPoint> points = tour.getPoints();
        Retrofit.Builder builder = new Retrofit.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

            httpClient.addInterceptor(logging);
            builder.client(httpClient.build());
        }

        Retrofit retrofit = builder
                .baseUrl("https://maps.googleapis.com")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RouteApi routeApi = retrofit.create(RouteApi.class);

        TourPoint startPoint = points.get(0);
        String startPointCoords = startPoint.getStringCoordinates();
        TourPoint finishPoint = points.get(points.size() - 1);
        String finishPointCoords = finishPoint.getStringCoordinates();

        String throughCoords = "";
        if (points.size() >= 3) {
            throughCoords += "optimize:true|";
            for (int i = 1; i < points.size() - 1; i++) {
                throughCoords += points.get(i).getStringCoordinates();
                if (i != points.size() - 2) {
                    throughCoords += "|";
                }
            }
        }

        return routeApi.
                getRoute(startPointCoords, finishPointCoords, throughCoords, true, "walking", "en", API_KEY)
                .flatMap(routeResponse -> Observable.create(new Observable.OnSubscribe<List<LatLng>>() {
                    @Override
                    public void call(Subscriber<? super List<LatLng>> subscriber) {
                        List<LatLng> latLngs = routeResponse.hasPoints() ?
                                PolyUtil.decode(routeResponse.getPoints()) : Collections.emptyList();
                        subscriber.onNext(latLngs);
                        subscriber.onCompleted();
                    }
                }));
    }
}
