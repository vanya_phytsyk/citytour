package one.citytour.domain.repository.impl;

import android.content.Context;
import android.content.SharedPreferences;

import com.vk.sdk.VKAccessToken;

import one.citytour.CityTourApplication;
import one.citytour.domain.model.AuthResponse;
import one.citytour.domain.repository.AuthLocalRepository;

/**
 * Created by vanya on 7/3/2016.
 */
public class AuthLocalRepositoryImpl implements AuthLocalRepository {

    private static final String PREFERENCES_NAME = "auth_credentials";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String REFRESH_TOKEN = "refresh_token";
    private static final String EXPIRED_PERIOD = "expired_period";
    private static final String LAST_REFRESHING_TIME = "last_refreshing_time";

    private String accessToken;
    private String refreshToken;
    private long lastRefreshingTime;

    private SharedPreferences preferences;

    private AuthLocalRepositoryImpl() {
        Context context = CityTourApplication.getContext();
        preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);

        accessToken = preferences.getString(ACCESS_TOKEN, null);
        refreshToken = preferences.getString(REFRESH_TOKEN, null);
        lastRefreshingTime = preferences.getLong(LAST_REFRESHING_TIME, -1);
    }

    private static final class InstanceHolder {
        private static final AuthLocalRepositoryImpl INSTANCE = new AuthLocalRepositoryImpl();
    }

    protected static AuthLocalRepositoryImpl getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public String getAccessToken() {
        return accessToken;
    }

    @Override
    public String getRefreshToken() {
        return refreshToken;
    }

    @Override
    public void clearSession() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        this.accessToken = null;
        this.refreshToken = null;
    }

    @Override
    public void storeAuthCredentials(String accessToken, String refreshToken, long expiredPeriod) {
        clearSession();
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.lastRefreshingTime = System.currentTimeMillis() / 1000;

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ACCESS_TOKEN, accessToken);
        editor.putString(REFRESH_TOKEN, refreshToken);
        editor.putLong(EXPIRED_PERIOD, expiredPeriod);
        editor.putLong(LAST_REFRESHING_TIME, lastRefreshingTime);
        editor.commit();
    }

    @Override
    public void storeAuthCredentials(AuthResponse authResponse) {
        storeAuthCredentials(authResponse.getAccessToken(), authResponse.getRefreshToken(), authResponse.getExpiredPeriod());
    }

    @Override
    public boolean hasAccessToken() {
        return accessToken != null;
    }

    @Override
    public boolean hasRefreshToken() {
        return refreshToken != null;
    }

    @Override
    public boolean isAuthByVK() {
        return VKAccessToken.currentToken() != null;
    }
}
