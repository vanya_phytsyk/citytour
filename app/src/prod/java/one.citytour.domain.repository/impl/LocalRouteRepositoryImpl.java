package one.citytour.domain.repository.impl;

import one.citytour.CityTourApplication;
import one.citytour.domain.model.Tour;
import one.citytour.domain.repository.LocalRouteRepository;
import one.citytour.domain.repository.impl.realrmentity.RealmRoute;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import rx.Observable;

/**
 * Created by vanya on 6/13/2016.
 */
public class LocalRouteRepositoryImpl implements LocalRouteRepository {

    private final RealmConfiguration realmConfig;

    protected LocalRouteRepositoryImpl() {
        realmConfig = RealmConfigHolder.getConfiguration();
    }

    @Override
    public Observable<List<LatLng>> getRouteForTour(long tourId) {
        return Observable.create((Observable.OnSubscribe<List<LatLng>>) subscriber -> {
            Realm realm = Realm.getInstance(realmConfig);
            RealmRoute realmRoute = realm.where(RealmRoute.class).equalTo("tourId", tourId).findFirst();
            if (realmRoute != null) {
                subscriber.onNext(realmRoute.convertToLatLngs());
            } else {
                subscriber.onNext(null);
            }
            subscriber.onCompleted();
            realm.close();
        });
    }

    @Override
    public Observable<Tour> saveRoute(Tour tour, List<LatLng> latLngs) {
        return Observable.create(subscriber -> {
            Realm realm = Realm.getInstance(realmConfig);
            realm.beginTransaction();
            realm.copyToRealm(new RealmRoute(tour.getId(), latLngs));
            realm.commitTransaction();
            subscriber.onNext(tour);
            subscriber.onCompleted();
            realm.close();
        });
    }

    @Override
    public void removeRoute(long tourId) {
        Realm realm = Realm.getInstance(realmConfig);
        realm.beginTransaction();
        RealmRoute realmRoute = realm.where(RealmRoute.class).equalTo("tourId", tourId).findFirst();
        if (realmRoute != null) {
            realmRoute.deleteFromRealm();
        }
        realm.commitTransaction();
        realm.close();
    }
}
