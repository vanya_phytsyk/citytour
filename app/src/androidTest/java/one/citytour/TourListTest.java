package one.citytour;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import one.citytour.ui.activity.TourListActivity;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import  static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.action.ViewActions.typeTextIntoFocusedView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by vanya on 7/18/2016.
 */
@RunWith(AndroidJUnit4.class)
public class TourListTest {

    @Rule
    public ActivityTestRule<TourListActivity> tourListActivityActivityTestRule =
            new ActivityTestRule<TourListActivity>(TourListActivity.class);

    @Test
    public void clickAddTourButton_oppensNewTourActivity() throws Exception {
        onView(withId(R.id.newTourButton)).perform(click());
        onView(withId(R.id.create_button)).check(matches(isDisplayed()));

        closeSoftKeyboard();
        onView(withId(R.id.tour_title)).perform(typeText("Title for test"));
       // onView(withId(R.id.tour_description)).perform(requ("Description for test"));
        onView(withId(R.id.tour_description)).perform(typeTextIntoFocusedView("Description for test"));

        onView(withId(R.id.create_button)).perform(click());
    }
}
