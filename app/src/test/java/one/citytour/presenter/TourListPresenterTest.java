package one.citytour.presenter;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import one.citytour.domain.model.Tour;
import one.citytour.presenter.helper.ConnectivityHelper;
import one.citytour.view.TourListView;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by vanya on 7/20/2016.
 */
public class TourListPresenterTest {

    private TourListPresenter presenter;

    @Mock
    private TourListView tourListView;

    @Mock
    private List<Tour> tours;

    @Mock
    private Tour tour;

    @Mock
    private Place place;

    @Captor
    private ArgumentCaptor<List<Tour>> tourListArgumentCaptor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        presenter = new TourListPresenter();
        presenter.setView(tourListView);
    }

    @Test
    public void testPlaceSelectionClick() throws Exception {
        when(place.getName()).thenReturn("SomePlace");
        when(place.getLatLng()).thenReturn(new LatLng(0, 0));
        presenter.placeSelectionClick();
        verify(tourListView).showPlaceSelectView();
    }

    @Test
    public void testPlaceSelected() throws Exception {
        when(place.getName()).thenReturn("SomePlace");
        when(place.getLatLng()).thenReturn(new LatLng(0, 0));
        presenter.placeSelected(place);
        verify(tourListView).updatePlaceName("SomePlace");
    }

    @Test
    public void testUserCredentialsClick() throws Exception {
        presenter.userCredentialsClick();
        verify(tourListView).navigateToAuthorize();
    }

    @Test
    public void testTourClick_tourSaved() throws Exception {
        when(tour.isSaved()).thenReturn(true);
//        ConnectivityHelper.setNextTimeNetworkUnAvailable();
        presenter.tourClick(tour);
        verify(tourListView).navigateToTourView(tour);
    }

    @Test
    public void testTourClick_tourNotSaved_noInternet() throws Exception {
        when(tour.isSaved()).thenReturn(false);
//        ConnectivityHelper.setNextTimeNetworkUnAvailable();
        presenter.tourClick(tour);
        verify(tourListView).showNoConnectionView();
    }

    @Test
    public void testTourClick_tourNotSaved() throws Exception {
        when(tour.isSaved()).thenReturn(false);
        presenter.tourClick(tour);
        verify(tourListView).navigateToTourView(tour);
    }

    @Test
    public void testTourSaveButtonClick() throws Exception {
        presenter.tourSaveButtonClick(tour);
        verify(tourListView).showTourSavedSuccess();
        verify(tourListView).updateTour(tour);
    }

    @Test
    public void testTourDeleteButtonClick() throws Exception {
        presenter.removeTourFromSavedButtonClick(tour);
        tourListView.hideTourItem(tour);
    }

    @Test
    public void testSearchMenuItemClick() throws Exception {
        presenter.searchMenuItemClick();
        verify(tourListView).showPlaceName();
    }

    @Test
    public void testSavedToursMenuItemClick() throws Exception {
        presenter.savedToursMenuItemClick();
        verify(tourListView).showTourList(tourListArgumentCaptor.capture());
        verify(tourListView).hidePlaceName();
    }

    @Test
    public void testSettingsMenuItemClick() throws Exception {
        presenter.settingsMenuItemClick();
        verify(tourListView).navigateToSettings();
    }

    @Test
    public void testResume() throws Exception {
        presenter.resume();
    }

    @Test
    public void testNewTourButtonClicked() throws Exception {
        presenter.newTourButtonClicked();
        verify(tourListView).navigateToNewTour();
    }

    @Test
    public void testPause() throws Exception {

    }

    @Test
    public void testStart() throws Exception {

    }

    @Test
    public void testStop() throws Exception {

    }
}