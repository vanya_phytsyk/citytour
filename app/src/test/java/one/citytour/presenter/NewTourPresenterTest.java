package one.citytour.presenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import one.citytour.domain.model.Photo;
import one.citytour.domain.model.Tour;
import one.citytour.domain.model.TourPoint;
//import one.citytour.domain.repository.impl.FakeFileUploadRepository;
import one.citytour.view.NewTourView;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by vanya on 7/17/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewTourPresenterTest {

    @Mock
    private NewTourPresenter presenter;

    @Mock
    private NewTourView newTourView;

    @Mock
    private Tour tour;

    @Before
    public void setup() throws Exception {
        presenter = new NewTourPresenter();
        presenter.setNewTourView(newTourView);
        presenter.setTour(tour);
    }

    //region validation tests
    @Test
    public void testValidateCoverPhoto() throws Exception {
        when(tour.getCoverPhoto()).thenReturn(null);
        presenter.createButtonClicked();

        verify(newTourView).showNoPhotoAlert();
    }

    @Test
    public void testValidateTitle() throws Exception {
        when(tour.getCoverPhoto()).thenReturn(new Photo());
        when(tour.getTitle()).thenReturn("");

        presenter.createButtonClicked();
        verify(newTourView).showNoTitleAlert();
    }

    @Test
    public void testValidateDescription() throws Exception {
        when(tour.getCoverPhoto()).thenReturn(new Photo());
        when(tour.getTitle()).thenReturn("Some title");
        when(tour.getDescription()).thenReturn("");

        presenter.createButtonClicked();

        verify(newTourView).showNoDescriptionAlert();
    }

    @Test
    public void testValidatePoints() throws Exception {
        when(tour.getCoverPhoto()).thenReturn(new Photo());
        when(tour.getTitle()).thenReturn("Some title");
        when(tour.getDescription()).thenReturn("Some description");
        when(tour.hasPoints()).thenReturn(false);

        presenter.createButtonClicked();

        verify(newTourView).showNoPointsAlert();
    }

    //endregion

    //region photoChosen
    @Test
    public void testCoverPhotoChosen_afterUpload_Photo_url_changed() throws Exception {
        String photoPath = "some/photo/path";
        ArgumentCaptor<Photo> argument = ArgumentCaptor.forClass(Photo.class);
        presenter.coverPhotoChosen(photoPath);
        verify(newTourView).showCoverPhotoDownloading(argument.capture());
        assertNotEquals(photoPath,argument.getValue().getUrl());
    }

    @Test
    public void testCoverPhotoChosen_afterUpload_showUploadedPHotoOnView() throws Exception {
        String photoPath = "some/photo/path";
        presenter.coverPhotoChosen(photoPath);
//        verify(newTourView).showCoverPhotoDownloaded(new Photo(0, FakeFileUploadRepository.CORRECT_URL));
    }

    @Test
    public void testCoverPhotoChosen_afterUploadFail_showFailOnView() throws Exception {
//        String photoPath = FakeFileUploadRepository.FAILURE_FILE_PATH;
//        presenter.coverPhotoChosen(photoPath);
//        verify(newTourView).showError("Wrong file path!");
//        verify(tour).setCoverPhoto(null);
//        verify(newTourView).removeCoverPhoto();
    }

    //endregion

    @Test
    public void testCreateButtonClicked_viewExit() throws Exception {
        when(tour.getCoverPhoto()).thenReturn(new Photo());
        when(tour.getTitle()).thenReturn("Some title");
        when(tour.getDescription()).thenReturn("Some description");
        when(tour.hasPoints()).thenReturn(true);

        presenter.createButtonClicked();

        verify(newTourView).exit();
    }

    @Test
    public void testNewPointClicked_addPointToTour_viewShowAddedPoint() throws Exception {
        TourPoint tourPoint = mock(TourPoint.class);
        presenter.pointCreated(tourPoint);

        verify(newTourView).addPoint(tourPoint);
        verify(tour).addPoint(tourPoint);
    }
}